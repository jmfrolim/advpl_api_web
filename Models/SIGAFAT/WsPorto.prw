#Include 'totvs.ch'
#Include 'restful.ch'
#Include 'topconn.ch'

Class WsPorto
    Data Url
    Data User
    Data Password
    Data Itens
    Data Viagem
    Data Produto

    Method New(cPlaca,dPerio) Constructor
    Method GetConn()
    Method SetConn()
    Method GetItens()
    Method UltViagem()
    Method IsCarrega()
EndClass

Method New(cPlaca,dPerio) Class WsPorto
    Self:Itens  := {}
    Self:Viagem := {}
    Self:Url    := GetMv("FT_URLPORT")
    Self:GetItens(cPlaca)
    Self:UltViagem(cPlaca)

Return Self


Method SetConn() Class WsPorto
Return

Method GetItens(cPlaca) Class WsPorto
    /*Local oRestClient   := FwRest():New(Self:Url)
    oRestClient:setPath("/"+cPlaca) */
    Aadd(Self:Itens,{2019-05-25,"0640002        ","FI0004", "DVS2824", 2019-05-25, "10:54", 2019-05-25, "11:07", 30.84, 47.82, 0 ,"869950    "})
    Aadd(Self:Itens,{2019-05-25,"0640002        ","FI0004", "DVS2824", 2019-05-25, "06:47", 2019-05-25, "07:03", 33.09, 50.12, 17.03,"769885    "})

Return Self

Method UltViagem(cPlaca) Class WsPorto
    
    BeginSql Alias "cAliasZC4"
        SELECT TOP 1
         ZC4_PLACA,
         ZC4_DTFIM,
         ZC4_HRFIM,
         ZC4_DOCPOR,
         ZC4_CODPRO,
         ZC4_DTABER,
         ZC4_HRABER
        FROM %Table:ZC4% ZC4
        WHERE ZC4_PLACA =%Exp:cPlaca%
        AND ZC4.%NotDel%
        ORDER BY ZC4.R_E_C_N_O_ DESC
    EndSql
    cAliasZC4->(DbSelectArea("cAliasZC4"))
    cAliasZC4->(DbGoTop())
    If ! cAliasZC4->(Eof())
        Aadd(Self:Viagem,{cAliasZC4->ZC4_PLACA} )
        Aadd(Self:Viagem,{cAliasZC4->ZC4_DTFIM} )
        Aadd(Self:Viagem,{cAliasZC4->ZC4_HRFIM} )
        Aadd(Self:Viagem,{cAliasZC4->ZC4_DOCPOR})
        Aadd(Self:Viagem,{cAliasZC4->ZC4_CODPRO})
        Aadd(Self:Viagem,{cAliasZC4->ZC4_DTABER})
        Aadd(Self:Viagem,{cAliasZC4->ZC4_HRABER})
    EndIf

    cAliasZC4->(DbSelectArea("cAliasZC4"))
    cAliasZC4->(DbCloseArea())    

Return Self

Method IsCarrega() Class WsPorto
    Local lReturn  := .T. 
    Local nX       := 1
    
    If !Empty(Self:Itens) .And. !Empty(Self:Viagem)
        For nX:= 1 to Len(Self:Itens)
            If Self:Itens[nX][12] == Self[4][2]
               lReturn      :=  .F.
               Self:Produto :=  Self:Itens[nX][2] 
            Else
               lReturn      :=  .T.
               Self:Produto :=  Self:Itens[nX][2] 
            EndIf             
        Next nX++          
    EndIf
    
Return lReturn