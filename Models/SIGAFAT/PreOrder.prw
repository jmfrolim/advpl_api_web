#Include 'Totvs.ch'
#Include 'TopConn.ch'

/*/{Protheus.doc} PreOrder
  (long_description)
  @type  Class
  @author Joao Manoel 
  @since 29/01/19
  @version 12.1.17
  @example
  (examples)
  @see (links_or_references)
/*/
Class PreOrder
 Data ID
 Data Filial
 Data Codigo
 Data Contrato
 Data Pedido
 Data Item
 Data Produto
 Data Cliente
 Data Emissao
 Data Ordem
 Data Quantidade
 Data Veiculo
 Data Motorista
 Data Transportadora
 Data UsrInclusao
 Data UsrAlter 

 Method New() Constructor
 Method GetCod()
 Method Insert()
 Method Alter()
 Method Delete()
 Method Find()
 Method FindAll()
 Method GetClient()
 Method GetMotorista()
 Method GetProduct()
 Method IsBlock()

EndClass

Method New() Class PreOrder
  ::Contrato        := Contract():New()
  ::Cliente         := Customer():New(/*Codigo*/,/*Loja*/,/*Cgc*/)
  ::Transportadora  := Transporter():New()
  ::Produto         := Product():New()
  ::Motorista       := Driver():New()
  ::Veiculo         := Vehicle():New()
  ::Item            := {}
Return Self