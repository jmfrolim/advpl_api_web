#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Define  PulaLinha   Chr(13)+Chr(10)


Class Driver
  Data Codigo
  Data CPF
  Data CNH
  Data Nome
  Data Status
  Data Filial
  Data Tipo
  Data UF
  Data Fone

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter()
  Method Delete(cEmpFil,cCodigo)
  Method Find(cFili,cCodigo)
  Method FindAll(aPars)
  Method SetBlock(cEmpFil,cCodigo)
  Method SetFree(cEmpFil,cCodigo)
  Method GetStatus(cEmpFil,cCodigo)
  Method Clone(Object)
  Method GetTipo(cTipo)
  Method SetLog(nOpt)

EndClass

Method New() Class Driver
  
Return Self

Method GetCod() Class Driver 
Return Self:Codigo  := U_ZUF05116()
//-------------------------------------------------------------------
/*/{Protheus.doc} function
description Method Insert
@author  Joao Manoel Freitas Rolim
@since   23/03/2018
@version 1.0
@build 12.1.17
@param aDados
  [1] Codigo
  [2] CPF
  [3] Nome
  [4] Status -> 1 Sim(Bloqueado); 2 N?o (N?o Bloqueado)
  [5] Filial
  [6] Tipo -> 1 Proprio; 2 Terceiro; 3 Agregado
  [7] Estado -> ex SP; RJ; MA; PI
  [8] Fone
  [9] CNH
@return lReturn -> Logico podendo ser .T.ou .F. cValToChar 
/*/
//-------------------------------------------------------------------
Method Insert() Class Driver
  Local lReturn
  Local lExist    := Self:Find(FwxFilial('DA4'),Self:Codigo)
  Local cNomeR    := Upper(CvalToChar(Self:Nome))

  If lExist
     lReturn    := .F.
     
  Else
     RecLock('DA4',.T.)
      DA4->DA4_FILIAL      := FwxFilial('DA4')
      DA4->DA4_COD         := Self:Codigo
      DA4->DA4_NOME        := Upper(Self:Nome)
      DA4->DA4_TIPMOT      := Self:Tipo
      DA4->DA4_NREDUZ      := cNomeR
      DA4->DA4_EST         := Self:UF
      DA4->DA4_CGC         := Self:CPF
      DA4->DA4_TEL         := Self:Fone
      DA4->DA4_NUMCNH      := Self:CNH 
      DA4->DA4_BLQMOT      := Self:Status
     DA4->(MsUnLock())

     If  Self:Find(FwxFilial('DA4'),Self:Codigo)
         lReturn := .T.

     Else
         lReturn := .F.
     EndIf
  
  EndIf
  
Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Method
description Method Alter
@author  Joao Manoel Freitas Rolim
@since   23/03/2018
@version 1.0
@build 12.1.17
@param aDados
  [1] Codigo
  [2] CPF
  [3] Nome
  [4] Status -> 1 Sim(Bloqueado); 2 N?o (N?o Bloqueado)
  [5] Filial
  [6] Tipo -> 1 Proprio; 2 Terceiro; 3 Agregado
  [7] Estado -> ex SP; RJ; MA; PI
  [8] Fone
@return lReturn -> Logico podendo ser .T.ou .F.
/*/
//-------------------------------------------------------------------
Method Alter() Class Driver
  Local lReturn 
  Local lExist    := Self:Find(FwxFilial('DA4'),Self:Codigo)

  If  lExist      
      Begin Transaction
       RecLock('DA4',.F.)
        DA4->DA4_NOME        := Self:Nome
        DA4->DA4_TIPMOT      := Self:Tipo
        DA4->DA4_NREDUZ      := SubStr(Self:Nome,1,20)
        DA4->DA4_EST         := Self:UF
        DA4->DA4_CGC         := Self:CPF
        DA4->DA4_TEL         := Self:Fone 
       DA4->(MsUnLock())
      End Transaction
      lReturn   := .T.     
  Else
      lReturn    := .F.    
  EndIf

Return lReturn

Method Delete(cEmpFil,cCodigo) Class Driver
  Local lReturn
  Local cIndex    := cEmpFil+cCodigo

  DbSelectArea('DA4')
  DA4->(DbSetOrder(1))
  DA4->(DbSeek(cIndex))
  If  Found()
      Begin Transaction
       RecLock('DA4',.F.)
        DA4->(DbDelete())
       DA4->(MsUnLock())
      
       If  Self:Find(cEmpFil,cCodigo)
           DisarmTransaction()
       Else
           lReturn  := .T.        
       EndIf
      End Transaction
      
  Else    
      lReturn := .F.    
  EndIf
  DA4->(DbCloseArea())  
Return lReturn

Method Find(cEmpFil,cCodigo) Class Driver
  Local lReturn
  Local cIndex  := cEmpFil+cCodigo 

  DbSelectArea('DA4')
  DA4->(DbSetOrder(1))//DA4_FILIAL+DA4_COD
  DA4->(DbSeek(cIndex))
  If  Found()
      Self:Codigo  := DA4->DA4_COD
      Self:CPF     := DA4->DA4_CGC
      Self:Nome    := DA4->DA4_NOME
      Self:Status  := Iif(DA4->DA4_BLQMOT =='1','1-BLOQUEADO','2-ATIVO')
      Self:Filial  := DA4->DA4_FILIAL
      Self:Tipo    := Self:GetTipo(AllTrim(DA4->DA4_TIPMOT))
      Self:UF      := DA4->DA4_EST
      Self:Fone    := DA4->DA4_TEL
      Self:CNH     := DA4->DA4_NUMCNH
      lReturn  := .T.
  Else
      lReturn  := .F.    
  EndIf
  DA4->(DbCloseArea())
  
Return lReturn
/*/{Protheus.doc} Clone
    (long_description)
    @type  Method
    @author user
    @since date
    @version version
    @param param, param_type, param_descr
    @return return, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
/*/
Method Clone(Object) Class Driver
  Do While !Empty(Object)
     Self:CPF   := Object:CPF
     Self:Nome  := Object:Nome
     Self:Status:= Object:Status
     Self:Filial:= Object:Filial
     Self:Tipo  := Object:Tipo
     Self:UF    := Object:UF
     Self:Fone  := Object:Fone
     Self:CNH   := Object:CNH
     Object     := ""
  EndDo
Return Self
/*/{Protheus.doc} FindAll
    (long_description)
    @type  Method
    @author user
    @since date
    @version version
    @param param, param_type, param_descr
    @return return, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
/*/
Method FindAll(aPars) Class Driver
 Local cSQL   := ""
 Local cLog   := ""
 Local dDate  := Date()
 Local cHora  := Time()
 
 cSQL+=" SELECT                             "+PulaLinha+""
 cSQL+=" DA4_FILIAL FILIAL,DA4_COD CODIGO,  "+PulaLinha+""
 cSQL+=" DA4_NOME NOME,                     "+PulaLinha+""
 cSQL+=" CASE DA4_TIPMOT                    "+PulaLinha+""
 cSQL+="   WHEN 1 THEN '1-PROPRIO'          "+PulaLinha+""
 cSQL+="   WHEN 2 THEN '2-TERCEIRO'         "+PulaLinha+""
 cSQL+="   WHEN 3 THEN '3-AGREGADO'         "+PulaLinha+""
 cSQL+=" END TIPO,                          "+PulaLinha+""
 cSQL+=" DA4_EST UF,                        "+PulaLinha+""
 cSQL+=" DA4_CGC CPF,                       "+PulaLinha+""
 cSQL+=" DA4_TEL FONE,                      "+PulaLinha+""
 cSQL+=" DA4_NUMCNH CNH,                    "+PulaLinha+""
 cSQL+=" CASE DA4_BLQMOT                    "+PulaLinha+""
 cSQL+="  WHEN 1 THEN '1-BLOQUEADO'         "+PulaLinha+""
 cSQL+="  WHEN 2 THEN '2-ATIVO'             "+PulaLinha+""
 cSQL+=" END STATUS                         "+PulaLinha+""
 cSQL+=" FROM "+RetSqlName('DA4')+ "  DA4   "+PulaLinha+""
 cSQL+=" WHERE DA4.D_E_L_E_T_=''            "+PulaLinha+""
 
 MemoWrite('\temp\driver_findall.txt',cSQL)
 cSQL+= ChangeQuery(cSQL)

  If  Select('cAlias') > 0
      DbSelectArea('cAlias')
      cAlias->(DbCloseArea())
  EndIf
  
  TcQuery cSQL New Alias 'cAlias'
  DbSelectArea('cAlias')
  cAlias->(DbGoTop())
  If  Empty(cAlias->CODIGO)
      cLog:='Consulta do Metodo FindAll Class Driver Retornou vazia dia '+AllTrim(Str(Day(dDate)))+' mes '+AllTrim(Str(Month(dDate)))+' hora '+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,7,2))+' .'
      ConOut('')
      ConOut(cLog)
      ConOut('')
      MemoWrite('\temp\result_driver_findall_'+AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,7,2))+'.txt',cLog)
  Else
      cLog:='Consulta do Metodo FindAll Class Driver realizada com sucesso dia '+AllTrim(Str(Day(dDate)))+' mes '+AllTrim(Str(Month(dDate)))+' hora '+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,7,2))+' .'
      ConOut('')
      ConOut(cLog)
      ConOut('')    
      MemoWrite('\temp\result_driver_findall_'+AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,7,2))+'.txt',cLog)
  EndIf  
Return 'cAlias'

/*/{Protheus.doc} GetTipo
  (long_description)
  @type  Method
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
/*/
Method GetTipo(cTipo) Class Driver
  Do Case
     Case cTipo == '1'
         cTipo  := '1-PROPRIO'

     Case cTipo == '2'
         cTipo  := '2-TERCEIRO'

     Case cTipo == '3'
         cTipo  := '3-AGREGADO'
  EndCase
Return cTipo

Method SetLog(nOpt) Class Driver
 Do Case
    Case nOpt == 1
         ConOut("Iniciando Insert ")
    
    Case nOpt == 2
         ConOut("Iniciando Alter ")
    Case nOpt == 3
         ConOut('Registo '+Self:Codigo+' encontrado com sucesso.')
    Case nOpt == 6
         ConOut('Insert: '+Self:Codigo+' realizado com sucesso.')
  EndCase
         
Return