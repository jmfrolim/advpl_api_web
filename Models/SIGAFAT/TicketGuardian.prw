#Include 'totvs.ch'
#Include 'topconn.ch'
#Include 'tbiConn.ch'
#Define  CRFL   Chr(13)+Chr(10)

Class TicketGuardian 
    Data placa        
    Data placa2       
    Data placa3       
    Data tag          
    Data periferico   
    Data pesoinici   
    Data pesofinal   
    Data produto      
    Data bruto       
    Data tara        
    Data liquido     
    Data operacao     
    Data numero       
    Data status
    Data navio  
    Data csqlTag  
    Data regra
    Data rota   

    Method New(cPlaca,cTag,cOpera,cPereif,nPesoIni,nPesoFin,nLiquido,cNumero) Constructor
    Method GetStatus()
    Method GetTag()
    Method GetRegra()
    Method Getplaca()
    Method GetPeriferico()
    Method GetPesoinici()
    Method GetPesofinal()
    Method GetProduto()
    Method GetBruto()
    Method GetTara()
    Method GetLiquido()
    Method GetOperacao()
    Method GetNumero()
    Method SetPeriferico()
    Method SetPesoinici()
    Method SetPesofinal()
    Method SetProduto()
    Method SetTara()
    Method SetLiquido()
    Method SetOperacao()
    Method SetNumero()
    Method SetSqlTAG()
    Method IsOk()
EndClass


Method New(cPlaca,cTag,cOpera,cPereif,nPesoIni,nPesoFin,cNumero) Class TicketGuardian
    Private OldTag      := cTag
    Private OldPlaca    := cPlaca
    Private OldOpera    := cOpera
    Private OldPereif   := cPereif
    Private OldPesoI    := Iif(ValType(nPesoIni) == 'N',nPesoIni,Val(nPesoIni))
    Private OldPesoF    := Iif(ValType(nPesoFin) == 'N',nPesoFin,Val(nPesoFin))
    Private ProdutReg   := ""
    Private OldNumero   := cNumero

    Self:SetOperacao()
    Self:SetPeriferico()
    Self:SetPesoinici()
    Self:SetPesofinal()
    Self:SetNumero()

Return Self

Method GetRegra(cRegra,cRota) Class TicketGuardian
    Local aArea     := GetArea()
    Local lReturn   := .T.
    Local cLogDia   := AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))

    If Empty(cRegra) .And. Empty(cRota)
       lReturn  := .F. 
    
    ElseIf Empty(cRegra) .Or. Empty(cRota)
       lReturn  := .F. 
    Else
        BeginSql Alias "QZC3"
            SELECT 
              ZC3_FILIAL,
              ZC3_ATIVO,
              ZC3_REGOPE,
              ZC3_ROTAMZ,
              ZC3_DESROP,
              ZC3_FORN,
              ZC3_LJFORN,
              ZC3_PRODUT,
              ZC3_NAVIO
            FROM %Table: ZC3%    ZC3
            WHERE 
            ZC3_REGOPE=%Exp: cRegra% 
            AND 
            ZC3_ROTAMZ=%Exp: cRota%
            AND
            ZC3.%NotDel%
        EndSQL
        QZC3->(DbSelectArea("QZC3"))
        QZC3->(DbGoTop())
        MemoWrite('\temp\QZC3'+cLogDia+'.txt',QZC3)
        If ! QZC3->(Eof())
            If  !Empty(QZC3->ZC3_PRODUT)
                Self:SetProduto(QZC3->ZC3_PRODUT)
                Self:navio  := QZC3->ZC3_NAVIO
                lReturn := .T.
            Else    
                lReturn := .F.
            EndIf
        Else
            lReturn := .F.
        EndIf
        If  Select("QZC3") > 0
            QZC3->(DbCloseArea())
        EndIf        
    EndIf

    RestArea(aArea)
Return lReturn

Method SetProduto(cProduto) Class TicketGuardian
    If !Empty(cProduto)
        Self:produto    := cProduto
    EndIf
Return Self

Method SetSqlTAG() Class TicketGuardian
    Local cSQL  := ""

    If Select('cAlias') > 0
        cAlias->(DbSelectArea())
        cAlias->(DbCloseArea())
    EndIf
    
    cSQL    +=" SELECT      " + CRFL
    cSQL    +=" ZC5_FILIAL, " + CRFL
    cSQL    +=" ZC5_CODIGO, " + CRFL
    cSQL    +=" ZC5_REGRA,  " + CRFL
    cSQL    +=" ZC5_ROTA,   " + CRFL
    cSQL    +=" ZC5_TAG,    " + CRFL
    cSQL    +=" ZC5_PLACA1, " + CRFL
    cSQL    +=" ZC5_ATIVO   " + CRFL
    cSQL    +=" FROM        " + CRFL
    cSQL    += RetSqlName('ZC5')+"  ZC5 " + CRFL

    TcQuery cSQL New Alias  'cAlias'

Return  'cAlias'


Method GetTag(cTag,cPlaca) Class TicketGuardian
    Local cSQL      := ""
    Local cWhere    := ""
    Local cLogDia   := ""

    If  !Empty(cTag) .And. !Empty(cPlaca)
        cWhere  += " WHERE  ZC5_TAG='"+cTag+"'  AND ZC5_PLACA1='" +cPlaca+" ' " +CRFL
        cWhere  += " ZC5.D_E_L_E_T_='' " + CRFL

        cSQL    :=  Self:SetSqlTAG()
        cSQL    +=  cWhere
        MemoWrite('\temp\TicketGuardian_gettag_sql.sql',cSQL)
        cLogDia := "Query montada com sucesso."
        MemoWrite('\temp\TicketGuardian_gettag_sql.txt',cLogDia)
        cSQL    := ChangeQuery(cSQL)

        If  Select('cAlias') > 0
            DbSelectArea('cAlias')
            cAlias->(DbCloseArea())
        EndIf

        TcQuery cSQL New Alias "cAlias"
        If  !cAlias->(Eof())
            Self:tag        := cAlias->ZC5_TAG
            Self:placa      := cAlias->ZC5_PLACA1
            Self:regra      := cAlias->ZC5_REGRA
            Self:rota       := cAlias->ZC5_ROTA
            Self:status     := cAlias->ZC5_ATIVO
        EndIf    
    Else
        cWhere  += " WHERE  ZC5_TAG='"+cTag+"'  OR ZC5_PLACA1='" +cPlaca+" ' " +CRFL
        cWhere  += " ZC5.D_E_L_E_T_='' " + CRFL

        cSQL    :=  Self:SetSqlTAG()
        cSQL    +=  cWhere
        MemoWrite('\temp\TicketGuardian_gettag_sql.sql',cSQL)
        cLogDia := "Query montada com sucesso."
        MemoWrite('\temp\TicketGuardian_gettag_sql.txt',cLogDia)
        cSQL    := ChangeQuery(cSQL)

        If  Select('cAlias') > 0
            DbSelectArea('cAlias')
            cAlias->(DbCloseArea())
        EndIf
        TcQuery cSQL New Alias "cAlias"
        If  !cAlias->(Eof())
            Self:tag        := cAlias->ZC5_TAG
            Self:placa      := cAlias->ZC5_PLACA1
            Self:regra      := cAlias->ZC5_REGRA
            Self:rota       := cAlias->ZC5_ROTA
            Self:status     := cAlias->ZC5_ATIVO
        EndIf        
    EndIf    
Return Self

Method SetOperacao() Class TicketGuardian
    Self:operacao   := OldOpera
Return Self

Method SetPeriferico() Class TicketGuardian     
Return Self:periferico := OldPereif

Method SetPesoinici() Class TicketGuardian      
Return Self:pesoinici  := OldPesoI

Method SetPesofinal() Class TicketGuardian  
Return Self:pesofinal   := OldPesoF

Method SetNumero() Class TicketGuardian 
Return Self:numero  := OldNumero