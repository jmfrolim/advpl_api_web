#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Define  PulaLinha   Chr(13)+Chr(10)


Class Customer
  Data Filial
  Data Codigo
  Data Loja
  Data TipoCivil // F�sica ou Juridica
  Data TipoVenda // Consumidor Final| ou Revendedor | etc
  Data CGC
  Data Nome
  Data NomeRed
  Data Cidade
  Data EmailNfe
  Data EmailContato
  Data Pais
  Data Status
  Data CGCExiste
  Data MultLoja
  Data ContaContabil
  Data Natureza
  Data UF
  Data Endereco


  Method New(cCodigo,cLoja,cCgc) Constructor
  Method GetCod()
  Method Insert()
  Method Alter(EmpFil,Codigo)
  Method Delete(cEmpFil,cCodigo)
  Method Find(cFili,cCodigo)
  Method GetCgc(cCgc)
  Method GTpJurid(cCgc)
  Method StSubCgc(cCgc)
  Method GetLoja(cCgc,cTipo)
  Method FindeCpf(cCgc,cSql)
  Method IsMultiFazenda(cTipoVenda,cTipoJ)
  Method Clone(Object)


EndClass

Method New(cCodigo,cLoja,cCgc) Class Customer 
  Private cLojaE   := ""
  Private cTopSql  := ""
  Private cOldCgc	:= cCgc  
  Private cTipoJuri:=""
  Private nCGC     := 0

  If !Empty(cCgc)
      cTipoJuri:= Self:GTpJurid(cCgc)
      Self:GetCgc(cCgc)      
  Else
      Self:Find(FwxFilial('SA1'),cCodigo+cLoja)
  EndIf

Return Self


//-------------------------------------------------------------------
/*/{Protheus.doc} Method Of Class
  @description GetCgc
  @author  Joao Manoel
  @since   25/09/18
  @version 1.1
  @param cTipo, character, (Descri��o do par�metro)
  @return Self, Object
  @example
  
/*/
//-------------------------------------------------------------------
Method GetCgc(cCgc) Class Customer
  Local cSQLSA1 := ""  
  
    cSQLSA1:=" SELECT"+cTopSql+" A1_COD,A1_LOJA,"  +PulaLinha
    cSQLSA1+=" A1_NOME,A1_NREDUZ,A1_MSBLQL,      " +PulaLinha
    cSQLSA1+=" A1_PESSOA,A1_TIPO,A1_NATUREZ,"      +PulaLinha
    cSQLSA1+=" A1_PAIS,A1_B2B,A1_CONTA,A1_MULOJA, "+PulaLinha
    cSQLSA1+=" A1_GRPVEN,A1_RISCO,A1_CLASSE,"      +PulaLinha
    cSQLSA1+=" A1_MOEDALC,A1_LC, A1_CGC"           +PulaLinha 
    cSQLSA1+=" FROM "+ RetSqlName('SA1')+"  SA1 "  +PulaLinha
    cSQLSA1+=" WHERE SA1.A1_CGC LIKE '%"+cCgc+"%' "+PulaLinha  
    cSQLSA1+=" AND SA1.D_E_L_E_T_ ='' "            +PulaLinha
    cSQLSA1+=" ORDER BY SA1.R_E_C_N_O_ DESC"       +PulaLinha

    MemoWrite('C:\temp\customerGetGcg'+Str(nCGC)+'.txt',cSQLSA1)
    
    If Select("cAliasSQL") <> 0
       cAliasSQL->(DbCloseArea())
    EndIf
    
    If Select('cSQLSA') <> 0
    	 cSQLSA->(DbCloseArea())
    EndIf
    
    TcQuery cSQLSA1 New Alias cSQLSA  

      If nCGC >= 2
         Self:Codigo  := Self:GetCod()
         Self:Loja    := Iif(cTipoJuri == 'J',Self:GetLoja(cOldCgc,cTipoJuri),'0001')
         Self:CGC     := cOldCgc
         Return Self 

      ElseIf Empty(cSQLSA->A1_CGC)

       DbSelectArea('cSQLSA')
         cSQLSA->(DbCloseArea())
         
         If cTipoJuri == 'J'
            cCgc  := Self:StSubCgc(cCgc)
            nCGC  := nCGC + 1
            Self:GetCgc(cCgc) 

         ElseIf cTipoJuri == 'F'
              Self:Codigo := Self:GetCod()
              Self:Loja   := '0001'  
              Self:CGC		:= AllTrim(cOldCgc)          
              Return Self 

         EndIf
         
      Else         
       
        TcQuery cSQLSA1 New Alias cAliasSQL
        DbSelectArea('cAliasSQL')
        cLojaE  := cAliasSQL->A1_LOJA 
        
        If Len(Alltrim(cOldCgc)) > 8 .And. Len(AllTrim(cOldCgc)) < 12

           If cOldCgc == cAliasSQL->A1_CGC
              Self:CGCExiste  := cAliasSQL->A1_CGC
           Else
              Self:CGCExiste  := ""             
           EndIf

        EndIf          
        
        If Len(AllTrim(cOldCgc)) > 12
        
        	 If nCGC < 2
        	    If cOldCgc == cAliasSQL->A1_CGC
              	 Self:CGCExiste := cAliasSQL->A1_CGC
	           	Else
              	 Self:CGCExiste := ""
              	 Self:MultLoja   := '1'
		         	EndIf
		         	
        	 EndIf           
        	 
        EndIf
        
        cAliasSQL->(DbGoTop())
          Self:Codigo         := Alltrim(cAliasSQL->A1_COD)
          Self:Nome           := AllTrim(cAliasSQL->A1_NOME)
          Self:Loja           := AllTrim(Self:GetLoja(cOldCgc,cTipoJuri))
          Self:CGC						:= AllTrim(cOldCgc)
          Self:NomeRed        := AllTrim(cAliasSQL->A1_NREDUZ)
          Self:TipoVenda      := AllTrim(cAliasSQL->A1_TIPO)
          Self:TipoCivil      := AllTrim(cAliasSQL->A1_PESSOA)         
          Self:Pais           := AllTrim(cAliasSQL->A1_PAIS)          
          Self:Natureza       := AllTrim(cAliasSQL->A1_NATUREZ)
          Self:Status         := AllTrim(cAliasSQL->A1_MSBLQL)
          Self:ContaContabil  := AllTrim(cAliasSQL->A1_CONTA)
        cAliasSQL->(DbCloseArea())

      EndIf
  
Return Self

Method GetCod() Class Customer 
 Local  cCodigo  := ""
 Local  cSQL     := ""
 Local  cSQL1    := ""

    cSQL  :=" SELECT MAX(R_E_C_N_O_) AS CODRECNO FROM "+ RetSqlName('SA1') +" SA1 "
    cSQL  +=" WHERE SA1.D_E_L_E_T_='' AND (SA1.A1_MULOJA ='2' OR SA1.A1_MULOJA='') "

    MemoWrite('C:\temp\customerGetCodRecno.txt',cSQL)

    If Select('cAliasSQL') > 0
      cAliasSQL->(DbCloseArea())     
    EndIf

    TcQuery cSQL New Alias cAliasSQL
    DbSelectArea('cAliasSQL')
    cAliasSQL->(DbGoTop())
    cSQL1 := " SELECT A1_COD AS COD FROM "+ RetSqlName('SA1') +" SA1 "
    cSQL1 += " WHERE  SA1.R_E_C_N_O_='"+ Str(cAliasSQL->CODRECNO) +"' "
    MemoWrite('C:\temp\customerGetCod.txt',cSQL1)
    
    If  Select('cAliasSQL1') > 0
        cAliasSQL1->(DbCloseArea())
    EndIf 
    
    TcQuery cSQL1 New Alias cAliasSQL1
    DbSelectArea('cAliasSQL1')
    cAliasSQL->(DbGoTop())
    
    cCodigo 	:= Right(StrZero(Val(Right(cAliasSQL1->COD,4))+1,6),4)
    cCodigo 	:= 'CC' + cCodigo
    Self:CGC	:= AllTrim(cOldCgc)

Return cCodigo

Method Insert() Class Customer
 Local    lReturn
 Local    aVetor      := {}
 Private  lMsErroAuto := .F.

  aVetor:={{"A1_COD"       ,Self:Codigo  	            ,Nil},; // Codigo				 
           {"A1_LOJA"      ,Self:Loja  	              ,Nil},; // Loja
           {"A1_NOME"      ,Self:Nome  	              ,Nil},; // Nome
           {"A1_NREDUZ"    ,Self:NomeRed              ,Nil},; // Nome reduz.
           {"A1_TIPO"      ,Self:TipoVenda            ,Nil},; // Tipo
           {"A1_END"       ,Self:Endereco	            ,Nil},; // Endereco
           {"A1_MUN"       ,SubStr(Self:Cidade,6,20)  ,Nil},;// Cidade
           {"A1_EST"       ,Self:UF                   ,Nil},;// Estado
           {"A1_CGC"       ,Self:CGC                  ,Nil},;
           {"A1_PESSOA"    ,Self:TipoCivil            ,Nil},;
           {"A1_NATUREZ"   ,Self:Natureza             ,Nil},;
		       {"A1_PAIS"      ,Self:Pais                 ,Nil},;
		       {"A1_CONTA"     ,Self:ContaContabil        ,Nil}}
           /*{"A1_COD_MUN"   ,SubStr(Self:Cidade,1,5)   ,Nil},;//Cod Cidade
           {"A1_MSBLQL"    ,Self:Status               ,Nil},;
           {"A1_PESSOA"    ,Self:TipoCivil            ,Nil},;
		       {"A1_NATUREZ"   ,Self:Natureza             ,Nil},;
		       {"A1_PAIS"   ,Self:Pais                 ,Nil},;
		       {"A1_CONTA"     ,Self:ContaContabil        ,Nil}}*/
         
         MsExecAuto({|x,y| Mata030(x,y)},aVetor,3) //3- Inclusao, 4-  Altera�ao, 5- Exclusao 
         //MsExecAuto({|x,y| Mata030(x,y)},aVetor,3) //3- Inclus?o, 4-  Altera�?o, 5- Exclus?o 
         
         If  lMsErroAuto
             ConOut("Erro")
             MostraErro() 
             lReturn   := .F.
         Else	
             ConOut("Ok")
             lReturn   := .T.
         EndIf

Return lReturn

Method Alter(EmpFil,Codigo) Class Customer 
Return

Method Delete(cEmpFil,cCodigo) Class Customer 
  Local lReturn
  Local lIsExit   := Self:Find(cEmpFil,cCodigo)
  
  If  lIsExit
      aVetor:={{"A1_COD"       ,Self:Codigo  	            ,Nil},; // Codigo				 
               {"A1_LOJA"      ,Self:Loja  	              ,Nil},; // Loja
               {"A1_NOME"      ,Self:Nome  	              ,Nil},; // Nome
               {"A1_NREDUZ"    ,Self:NomeRed              ,Nil},; // Nome reduz.
               {"A1_TIPO"      ,Self:TipoVenda            ,Nil},; // Tipo
               {"A1_END"       ,Self:Endereco	            ,Nil},; // Endereco
               {"A1_MUN"       ,Self:Cidade	              ,Nil},; // Cidade
               {"A1_EST"       ,Self:UF                   ,Nil},;  // Estado
               {"A1_PESSOA"    ,Self:TipoCivil            ,Nil},;
		           {"A1_NATUREZ"   ,Self:Natureza             ,Nil},;
		           {"A1_PAIS"   ,Self:Pais                 ,Nil},;
		           {"A1_CGC"       ,Self:CGC                  ,Nil},;
		           {"A1_CONTA"     ,Self:ContaContabil        ,Nil}}
         
         MsExecAuto({|x,y| Mata030(x,y)},aVetor,5) //3- Inclusao, 4-  Altera�ao, 5- Exclusao 
         
         If  lMsErroAuto
             ConOut("Erro")
             MostraErro()
             lReturn   := .F.
         Else	
             ConOut("Ok")
             lReturn   := .T.
         EndIf

  Else
      lReturn := .F.
    
  EndIf

Return lReturn

Method Find(cEmpFil,cCodigo) Class Customer
  Local lReturn
  Local nIndex

  If  10 == Len(cCodigo) 
      nIndex := 1
  Else
      nIndex := 3    
  EndIf
  
  DbSelectArea('SA1')
  SA1->(DbSetOrder(nIndex))// 1 A1_FILIAL+A1_COD+A1_LOJA Ou 3 A1_FILIAL+A1_CGC
  SA1->(DbSeek(cEmpFil+cCodigo))
  If  Found()      
      Self:Codigo  	       := SA1->A1_COD
      Self:Loja  	         := SA1->A1_LOJA
      Self:Nome  	         := SA1->A1_NOME
      Self:NomeRed         := SA1->A1_NREDUZ
      Self:TipoVenda       := SA1->A1_TIPO
      Self:Endereco	       := SA1->A1_END
      Self:Cidade	         := SA1->A1_MUN
      Self:UF              := SA1->A1_EST
      Self:Status          := SA1->A1_MSBLQL
      Self:TipoCivil       := SA1->A1_PESSOA
      Self:Natureza        := SA1->A1_NATUREZ
      Self:Pais            := SA1->A1_PAIS
      Self:CGC             := SA1->A1_CGC
      Self:ContaContabil   := SA1->A1_CONTA
      lReturn := .T.

  Else
      lReturn := .F.
  EndIf

Return lReturn

Method GTpJurid(cCgc) Class Customer
  Local cRetorno  := ""
  
  If Len(AllTrim(cCgc)) > 11
     cRetorno := 'J'
  Else
     cRetorno := 'F'
  EndIf

Return cRetorno

Method GetLoja(cCgc,cTipo) Class Customer
  Local cReturn  := ""
    If cTipo == 'J'
       cReturn  := StrZero(Val(SubStr(cCgc,11,2)),4)
    Else
       cReturn  := StrZero(Val(cLojaE)+1,4)
    EndIf
Return cReturn


Method StSubCgc(cCgc) Class Customer
  Local cRetorno    := ""
  If Len(AllTrim(cCgc)) > 11
     cTopSql  := " TOP 1 "     
     cRetorno := Left(cCgc,8)
  Else
     cRetorno := Alltrim(Left(cCgc,11))
  EndIf
Return cRetorno

/*/{Protheus.doc} Clone
  (long_description)
  @type  Method
  @author user
  @since date
  @version version
  @param Object, Objeto Cliente, recerbe um cliente
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
/*/
Method Clone(Object) Class Customer
  Do While !Empty(Object)
     Self:Filial        := Object:Filial
     Self:Codigo        := Object:Codigo
     Self:Loja          := Object:Loja
     Self:TipoCivil     := Object:TipoCivil
     Self:TipoVenda     := Object:TipoVenda
     Self:CGC           := Object:CGC
     Self:Nome          := Object:Nome
     Self:NomeRed       := Object:NomeRed
     Self:Cidade        := Object:Cidade
     Self:EmailNfe      := Object:EmailNfe
     Self:EmailContato  := Object:EmailContato
     Self:Pais          := Object:Pais
     Self:Status        := Object:Status
     Self:CGCExiste     := Object:CGCExiste
     Self:MultLoja      := Object:MultLoja
     Self:ContaContabil := Object:ContaContabil
     Self:Natureza      := Object:Natureza
     Self:UF            := Object:UF
     Self:Endereco      := Object:Endereco
     Object             := ""
  EndDo
  
Return Self