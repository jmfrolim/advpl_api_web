#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Define  PulaLinha   Chr(13)+Chr(10)
//-------------------------------------------------------------------
/*/{Protheus.doc} Class Rating
@description
@author  Joao Manoel
@since   09/2018
@version 12.1.17
@Obs   Class Model das Tabelas Z7I-> Cabecalho da Cotacao e Z7J-> Para Detalhes
/*/
//-------------------------------------------------------------------

Class Rating
  Data Filial
  Data Contrato
  Data Pedido
  Data Status
  Data UserImp
  Data Codigo
  Data Emissao
  Data Validade
  Data Volume
  Data aItens

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter(cEmpFil,cCodigo)
  Method Delete(cEmpFil,cCodigo)
  Method Find(cFili,cCodigo)
  Method FindAll(aPar,nTipo)
  Method GQtdCon(Contrato)//get quantidade do contrato
  Method SetItem(Object,aTransp)
  Method GetItem(Object,cItem)
  Method GetStatus()
  Method SetStatus()
EndClass

Method New() Class Rating 
  Self:Filial   := FwxFilial()
  Self:aItens   := {}
Return Self

Method GetCod() Class Rating 
 Local cSQL   := " "
 Local cData  := Transform(CvalToChar(Date()),"!")
 
  cSQL := " SELECT MAX(Z7I_CODIGO) CODIGO  FROM "+ RetSqlName('Z7I')+" Z7I "
  cSQL += " WHERE Z7I_FILIAL='"+FwxFilial()+"' "
  
  MemoWrite("\temp\rating_getcod"+cData+".txt",cSQL)
 
  If Select('cAlias') > 0
     DbSelectArea('cAlias')
     cAlias->(DbCloseArea )
  EndIf

  TcQuery cSQL New Alias 'cAlias'
  DbSelectArea('cAlias')
  Self:Codigo := StrZero(Val(cAlias->CODIGO)+1,6)
  cAlias->(DbCloseArea())

Return Self:Codigo

Method Insert() Class Rating 
  Local lReturn := .F.
  Local lExist  := Self:Find(Self:Filial,Self:Codigo) // implementar esse metodo
  Local nX      := 0
  If  lExist
      lReturn  := .F.
  Else
      Begin Transaction
       RecLock('Z7I',.T.)
        Z7I->Z7I_FILIAL := Self:Filial
        Z7I->Z7I_CODIGO := Self:Codigo
        Z7I->Z7I_EMISSA := Self:Emissao
        Z7I->Z7I_VENCIM := Self:Validade
        Z7I->Z7I_CONTRA := Self:Contrato
        Z7I->Z7I_PEDIDO := Self:Pedido
        Z7I->Z7I_VOLUME := Self:Volume
        Z7I->Z7I_USR    := Self:UserImp
        Z7I->Z7I_STATUS := Self:Status
       Z7I->(MsUnLock())
       For nX:= 1 to Len(Self:aItens)
           RecLock('Z7J',.T.)
            Z7J->Z7J_FILIAL  := Self:aItens[nX][1]
            Z7J->Z7J_ITEM    := Self:aItens[nX][2]
            Z7J->Z7J_CODIGO  := Self:aItens[nX][3]
            Z7J->Z7J_CONTRA  := Self:aItens[nX][4]
            Z7J->Z7J_TRANS   := Self:aItens[nX][5]
            Z7J->Z7J_LOJATR  := Self:aItens[nX][6]
            Z7J->Z7J_VLRF    := Self:aItens[nX][7]
            Z7J->Z7J_APROV   := Self:aItens[nX][8]
            Z7J->Z7J_USRI    := ""//Self:aItens[nX][9]
            Z7J->Z7J_USRAL   := ""//Self:aItens[nX][10]
            Z7J->Z7J_URSAP   := ""//Self:aItens[nX][11]
            Z7J->Z7J_URTRP   := ""//Self:aItens[nX][12]
           Z7J->(MsUnLock())       
       Next nX++
       If  Self:Find(Self:Filial,Self:Codigo)
           lReturn:= .T.
       Else
           DisarmTransaction()
       EndIf
              
      End Transaction
  EndIf  
Return lReturn

Method Alter(cEmpFil,cCodigo) Class Rating 
  Local lReturn
Return lReturn

Method Delete(cEmpFil,cCodigo) Class Rating 
  Local lReturn
Return lReturn

Method Find(cEmpFil,cCodigo) Class Rating 
  Local lReturn     := .F.
  Local cIndex      := cEmpFil + cCodigo

  DbSelectArea('Z7I')
  Z7I->(DbSetOrder(1))//Z7I_FILIAL+Z7I_CODIGO
  Z7I->(DbSeek(cIndex))
  If  Found()
      Self:Codigo     := Z7I->Z7I_CODIGO  
      Self:Emissao    := Z7I->Z7I_EMISSA 
      Self:Validade   := Z7I->Z7I_VENCIM 
      Self:Contrato   := Z7I->Z7I_CONTRA 
      Self:Pedido     := Z7I->Z7I_PEDIDO
      Self:Volume     := Z7I->Z7I_VOLUME 
      Self:UserImp    := Z7I->Z7I_USR 
      Self:Status     := Z7I->Z7I_STATUS
      DbSelectArea('Z7J')
      Z7J->(DbSetOrder(1))//
      Z7J->(DbSeek(cIndex))//Z7J_FILIAL+Z7J_CODIGO                                                                                                                                           
      If  Found()
          Do While !Z7J->(Eof()) .And. (Z7J->Z7J_FILIAL+Z7J->Z7J_CODIGO) == cIndex
             Aadd(Self:aItens,{Z7J->Z7J_FILIAL,Z7J->Z7J_ITEM,Z7J->Z7J_CODIGO,Z7J->Z7J_CONTRA,Z7J->Z7J_TRANS,Z7J->Z7J_LOJATR,Z7J->Z7J_VLRF,Z7J->Z7J_APROV})
             Z7J->(DbSkip())
          EndDo    
      Else
          lReturn := .T.
      EndIf      
  Else
      lReturn   := .F.
  EndIf
Return lReturn


Method FindAll(aPar,nTipo) Class Rating
 Local cSQL     := ""
 Local cWhere   := " WHERE "
 Local cMsgErro := ""
 Local aCond    := {1,2,3}
 Local cLog			:= ""
 Local nX				:= 0

 
 nTipo  := Iif(ValType(nTipo)!= 'N',Val(nTipo),nTipo)
 //oLog:SetPath() //SetPath() 
 If nTipo == aCond[1]
    If  nTipo == 1
        If  Empty(aPar)
            
            cWhere+=" Z7I_CODIGO>='"+''+"'         AND "
            cWhere+=" Z7I_CODIGO<='"+'ZZZZZZ'+"'   AND "
            cWhere+=" Z7I.D_E_L_E_T_=''"
        Else
            cWhere+=" Z7I_CODIGO>='"+aPar[1]+"'    AND "
            //cWhere+= "Z7I_CODIGO<='"+aPar[2]+" "
            cWhere+=" Z7I.D_E_L_E_T_=''"    
        EndIf    
    Else
        If  Empty(aPar)
            cWhere+=" Z7I_EMISSA>='"+''+"'         AND	"
            cWhere+=" Z7I_EMISSA<='"+'ZZZZZZ'+"'   AND	"
            cWhere+=" Z7I.D_E_L_E_T_=''"
        Else
            cWhere+=" Z7I_EMISSA>='"+aPar[2]+"' 	 AND	"
            cWhere+=" Z7I_EMISSA<='"+aPar[3]+"' 	 AND	"
            cWhere+=" Z7I.D_E_L_E_T_=''"   
        EndIf 
    EndIf
    cSQL+=" SELECT                                  "+PulaLinha+""
    cSQL+=" Z7I_FILIAL 'FILIAL',                    "+PulaLinha+""
    cSQL+=" Z7I_CODIGO 'CODIGO',                    "+PulaLinha+""
    cSQL+=" Z7I_EMISSA 'EMISSAO',                   "+PulaLinha+""
    cSQL+=" Z7I_VENCIM 'VENCIMENTO',                "+PulaLinha+""
    cSQL+=" Z7I_CONTRA 'CONTRATO',                  "+PulaLinha+""
    cSQL+=" Z7I_PEDIDO 'PEDIDO',                    "+PulaLinha+""
    cSQL+=" Z7I_VOLUME 'VOLUME',                    "+PulaLinha+""
    cSQL+=" Z7I_USR    'USUARIO',                   "+PulaLinha+""
    cSQL+=" Z7I_STATUS 'STATUS',                    "+PulaLinha+""
    cSQL+=" Z7J_ITEM  'ITEM'           ,            "+PulaLinha+""
    cSQL+=" Z7J_TRANS+'-'+A4_NOME 'TRANSPORTADORA' ,"+PulaLinha+""
    cSQL+=" Z7J_VLRF  'VALOR_FRETE'    ,            "+PulaLinha+""
    cSQL+=" Z7J_APROV 'STATUS_ITEM'    ,            "+PulaLinha+""
    cSQL+=" Z7J_USRI  'USER_INCLUSAO'  ,            "+PulaLinha+""
    cSQL+=" Z7J_USRAL 'USER_ALTERACAO' ,            "+PulaLinha+""
    cSQL+=" Z7J_URSAP 'USER_APROVACAO' ,            "+PulaLinha+""
    cSQL+=" Z7J_URTRP 'USERTRANSP_APROVACAO'        "+PulaLinha+""
    cSQL+=" FROM "+RetSqlName('Z7I')+" Z7I          "+PulaLinha+""      
    cSQL+="                                         "+PulaLinha+""
    cSQL+=" INNER JOIN "+RetSqlName('Z7J')+"        "+PulaLinha+""
    cSQL+=" Z7J ON                                  "+PulaLinha+""
    cSQL+=" Z7I_CONTRA = Z7J_CONTRA     AND         "+PulaLinha+""
    cSQL+=" Z7I_CODIGO = Z7J_CODIGO     AND         "+PulaLinha+""
    cSQL+=" Z7I_FILIAL = Z7J_FILIAL     AND         "+PulaLinha+""
    cSQL+=" Z7J.D_E_L_E_T_='' 				        "+PulaLinha+""	
    cSQL+="                                         "+PulaLinha+""
    cSQL+=" INNER JOIN "+RetSqlName('SA4')+"        "+PulaLinha+""
    cSQL+=" SA4   ON                                "+PulaLinha+""
    cSQL+=" Z7J_TRANS = A4_COD      AND             "+PulaLinha+""
    cSQL+=" SA4.D_E_L_E_T_=''                       "+PulaLinha+""
    cSQL+=" "+cWhere+" "

    MemoWrite('\temp\rating_get_all.txt',cSQL) 
    cSQL+= ChangeQuery(cSQL)    

	For nX:= 1 to Len(aPar)
	    cLog	+= 'Parametro na posicao:'+AllTrim(Str(nX))+' valor e: '+AllTrim(CvalToChar(aPar[nX]))+' .'+PulaLinha+''
	Next nX++
	MemoWrite('\temp\rating_get_all_par_log.txt',cLog)

    If  Select('cAlias')> 0
        DbSelectArea('cAlias')
        cAlias->(DbCloseArea())
    EndIf
    TcQuery cSQL New Alias 'cAlias'
    DbSelectArea('cAlias')
    cAlias->(DbGotop())
    If  Empty(cAlias->CODIGO)
        MemoWrite('\temp\rating_get_all_result_empty.txt','cAlias')
        ConOut("---------------"+PulaLinha+"")
        
    Else
        MemoWrite('\temp\rating_get_all_result.txt','cAlias')
    EndIf   
 Else
      cMsgErro:="O parametro Tipo para executar a consulta e invalido!!"
      ConOut(cMsgErro)
      //MemoWrite(oLog:PathLog+'raitng_FindAll_erro.txt',cMsgErro)     
      cMsgErro  := "" 
      TcQuery cMsgErro New Alias 'cAlias'
      
 EndIf

Return 'cAlias'

//-------------------------------------------------------------------
/*/{Protheus.doc} function
description
@author  Joao Manoel
@since   16/12/2018
@version 12.1.17
@param Object, Objeto, um objeto Rating com os 
@return return, return_type, return_description
/*/
//-------------------------------------------------------------------
Method SetItem(Object,aTransp) Class Rating
 Local nX     := 0
  For nX:= 1 to Len(aTransp)
      Aadd(Self:aItens,{Self:Filial,AllTrim(Str(nX)),Self:Codigo,Self:Contrato,aTransp[nX][1],,0,'C'})
  Next nX++
  
Return Self