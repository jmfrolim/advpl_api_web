#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Define  PulaLinha   Chr(13)+Chr(10)

Class Transporter
  Data Filial
  Data Codigo
  Data CGC
  Data Nome
  Data InsEstadual
  Data UF  

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter(cEmpFil,cCodigo)
  Method Delete(cEmpFil,cCodigo)
  Method Find(cFili,cCodigo)
  Method FindAll()

EndClass
//-------------------------------------------------------------------
/*/{Protheus.doc} Transporter:New
description Method New
@author  Joao Manoel Freitas Rolim
@since   2/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method New() Class Transporter 
Return Self
//-------------------------------------------------------------------
/*/{Protheus.doc} Transporter:GetCod
description Method GetCod
@author  Joao Manoel Freitas Rolim
@since   2/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method GetCod() Class Transporter
 Local cSQL                       := ""

  cSQL += " SELECT MAX(A4_COD) CODIGO FROM "+RetSqlName('SA4')+ " SA4 "+PulaLinha+""
  cSQL += " WHERE SA4.A4_FILIAL='"+FwxFilial('SA4')+"' "
  cSQL += ChangeQuery(cSQL)
  
  MemoWrite("\temp\Transporter_getcod"+AllTrim(Time())+".txt ",cSQL)
  
  If  Select('cAlias') <> 0
      DbSelectArea('cAlias')
      cAlias->(DbCloseArea())
  EndIf

  TcQuery cSQL New Alias cAlias
  DbSelectArea('cAlias')
  cAlias->(DbGoTop())
  If  Empty(cAlias->CODIGO)
      Self:Codigo := StrZero(1,6)    
  Else
      Self:Codigo := StrZero(Val(cAlias->CODIGO)+1,6) 
  EndIf 
  cAlias->(DbCloseArea())
Return Self:Codigo
//-------------------------------------------------------------------
/*/{Protheus.doc} Transporter:Insert
description Method Insert
@author  Joao Manoel Freitas Rolim
@since   2/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method Insert() Class Transporter 
 Local aMata050                   := {}
 Local lReturn                    := .F.
 Private lMsErroAuto              := .F.
  
  Aadd(aMata050, {"A4_FILIAL", FwxFilial("SA4")		          , Nil})
  Aadd(aMata050, {"A4_COD"   , Self:Codigo	                , Nil})
  Aadd(aMata050, {"A4_NOME"  , Self:Nome                    , Nil})
  Aadd(aMata050, {"A4_CGC"   , Self:CGC                     , Nil})
  Aadd(aMata050, {"A4_NREDUZ", SubStr(Self:Nome,1,20)       , Nil})
  Aadd(aMata050, {"A4_EST"   , Self:UF                      , Nil})
  
  MsExecAuto({|x,y|Mata050(x,y)},aMata050,3) 	
  If  lMsErroAuto		
      ConOut("Erro na grava��o da transportadora")
      MostraErro() 
  Else
      ConOut('Transportadora incluida com sucesso.')
      lReturn := .T.
  EndIf

Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Transporter:Alter
description Method Alter
@author  Joao Manoel Freitas Rolim
@since   2/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method Alter(cEmpFil,cCodigo) Class Transporter 
 Local aMata050                   := {}
 Local lReturn                    := .F.
 Private lMsErroAuto              := .F.
  
  Aadd(aMata050, {"A4_FILIAL", FwxFilial("SA4")		        , Nil})
  Aadd(aMata050, {"A4_COD"   , Self:Codigo	                , Nil})
  Aadd(aMata050, {"A4_NOME"  , Self:Nome                    , Nil})
  Aadd(aMata050, {"A4_CGC"   , Self:CGC                     , Nil})
  Aadd(aMata050, {"A4_NREDUZ", SubStr(Self:Nome,1,20)       , Nil})
  Aadd(aMata050, {"A4_EST"   , Self:UF                      , Nil})
  
  MsExecAuto({|x,y|Mata050(x,y)},aMata050,4) 	
  If  lMsErroAuto		
      ConOut("Erro na Altera�ao da transportadora")
      MostraErro() 
  Else
      ConOut('Transportadora Alterado com sucesso.')
      lReturn := .T.
  EndIf

Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Transporter:Delete
description Method Delete
@author  Joao Manoel Freitas Rolim
@since   2/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method Delete(cEmpFil,cCodigo) Class Transporter 
 Local aMata050                   := {}
 Local lReturn                    := .F.
 Private lMsErroAuto              := .F.
  
  Aadd(aMata050, {"A4_FILIAL", FwxFilial("SA4")		          , Nil})
  Aadd(aMata050, {"A4_COD"   , cCodigo	                    , Nil})
  
  MsExecAuto({|x,y|Mata050(x,y)},aMata050,5) 	
  If  lMsErroAuto		
      ConOut("Erro na Exclusao da transportadora")
      MostraErro() 
  Else
      ConOut('Transportadora Excluida com sucesso.')
      lReturn := .T.
  EndIf

Return lReturn

//-------------------------------------------------------------------
/*/{Protheus.doc} Transporter:Find
description Method Find
@author  Joao Manoel Freitas Rolim
@since   2/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method Find(cEmpFil,cCodigo) Class Transporter 
 Local lReturn    := .F.
 Local cIndex     := cEmpFil+cCodigo
 Local nIndex     := 0
 
  If  10 >=  Len(cCodigo) 
      nIndex := 1
  Else
      nIndex := 3
    
  EndIf
 DbSelectArea('SA4')
 SA4->(DbSetOrder(nIndex))// INDEX 1 := A4_FILIAL+A4_COD || OU INDEX 3 := A4_FILIAL+A4_CGC
 SA4->(DbSeek(cIndex))
 If  Found()
     Self:Filial        := SA4->A4_FILIAL
     Self:Codigo        := SA4->A4_COD
     Self:CGC           := SA4->A4_CGC
     Self:Nome          := SA4->A4_NOME
     Self:InsEstadual   := SA4->A4_INSEST
     Self:UF            := SA4->A4_EST
     lReturn  := .T.
 Else
   
 EndIf
 SA4->(DbCloseArea())
Return lReturn

Method FindAll() Class Transporter
 Local cSQL     := ""
 Local cWhere   := ""
  
  cSQL +=" SELECT "                                   +PulaLinha+""
  cSQL +=" A4_FILIAL AS FILIAL,SA4.A4_COD AS CODIGO," +PulaLinha+""
  cSQL +=" A4_NOME AS NOME ,A4_EST AS UF,"            +PulaLinha+""
  cSQL +=" A4_CGC 'CPF_CNPJ', A4_INSEST AS 'INSEST'"  +PulaLinha+""
  cSQL +=" FROM "+RetSqlName('SA4')+ " SA4 "          +PulaLinha+""
  cSQL +=" WHERE SA4.D_E_L_E_T_=''"

  MemoWrite('\temp\tranposter_FindAll.txt',cSQL)

  If  Select('cAlias') > 0
      DbSelectArea('cAlias')
      cAlias->(DbCloseArea())      
  EndIf

  TcQuery cSQL New Alias 'cAlias'
  DbSelectArea('cAlias')
  cAlias->(DbGoTop())
  If  Empty(cAlias->CODIGO)
      ConOut('------------------------------------------------------------------'+PulaLinha+'')
      ConOut('Consulta Class Transporter Metodo FindAll() Vazia'+PulaLinha+'')
      ConOut('------------------------------------------------------------------'+PulaLinha+'')
  Else
      ConOut('------------------------------------------------------------------'+PulaLinha+'')
      ConOut('Consulta Class Transporter Metodo FindAll() realizada com sucesso.'+PulaLinha+'')
      ConOut('------------------------------------------------------------------'+PulaLinha+'') 
  EndIf
    
Return 'cAlias'
