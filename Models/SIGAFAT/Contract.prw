
#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Include 'RestFul.ch'
#Define  PulaLinha   Chr(13)+Chr(10)

Class Contract 
 Data Filial
 Data Pedido
 Data Status
 Data Contrato
 Data TipCliente
 Data Cliente
 Data Emissao
 Data Vendedores
 Data TipoVenda
 Data TipoFrete
 Data Vencimento
 Data Moeda
 Data PTAX
 Data Itens
 Data ValUnit

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter(cEmFil,Codigo)
  Method Delete(cEmpFil,cCodigo)
  Method Find(cFili,Codigo)
  Method SetItem(oObjet,cItem,cCultura,nQuant,nSaldo,nEntreg)
	Method GVolume()
  Method FindAll()
	Method GetSaldo(cItem)
	

EndClass

Method New() Class Contract
	Self:Itens							 :={}
Return Self

Method GetCod() Class Contract
	Local cNumPV := ""
	Local cSQL := ""
	Local cNumMin:="200000"//numero inicial
			
		cSQL := " SELECT MAX(Z59_NUM) AS NUMERO FROM "+RetSQLName('Z59')+" Z59 "
		cSQL += " "
			
		If  Select("_Z59") <> 0
				DbSelectArea("_Z59")
				_Z59->(DbCloseArea())
		EndIf
		
		TcQuery cSQL New Alias "_Z59"
		
		DbSelectArea("_Z59")
		_Z59->(DbGoTop())
		
		If  _Z59->(Eof()) .Or. Empty(Alltrim(_Z59->NUMERO))
				cNumPV := cNumMin
		Else
				cNumPV := StrZero(Val(_Z59->NUMERO)+1,6)
		EndIf
		
		_Z59->(DbCloseArea())

Return Self:Pedido	:= cNumPV

Method Insert() Class Contract
  Local lReturn
  Local cEmpty	:= ""
  Local nZero		:= 0
  Local	nX

	If  !Empty(Self:Itens)
			For nX:= 1 To Len(Self:Itens)

					DbSelectArea('Z59')
					RecLock("Z59", .T.)						
						//Cabe�alho
						Z59->Z59_FILIAL	 := FwxFilial('Z59')
						Z59->Z59_NUM	 := Self:Pedido
						Z59->Z59_CONTRA	 := Self:Pedido
						Z59->Z59_CLIENT	 := Self:Cliente:Codigo
						Z59->Z59_LOJA	 := Self:Cliente:Loja
						Z59->Z59_VEND1	 := cEmpty
						Z59->Z59_VEND2	 := cEmpty
						Z59->Z59_COMIS1	 := nZero
						Z59->Z59_COMIS2	 := nZero
						Z59->Z59_PTAX	 := Self:PTAX
						Z59->Z59_MOEDA	 := Self:Moeda
						Z59->Z59_TPCLI	 := Self:Cliente:TipoVenda
						Z59->Z59_TIPOVE	 := cEmpty
						Z59->Z59_TPFRET	 := 'F'
						Z59->Z59_STATUS	 := "S"
						//Z59->Z59_DATA		 := Date() Criar esse campo na tabela
						Z59->Z59_VCTO		 := CtoD('29/11/2018')
						//Z59->Z59_LOCAL	 := cGetTpLoc
						//Z59->Z59_FRETE	 := cEmpty
						Z59->Z59_CONDPA	 := cEmpty
						Z59->Z59_OBS		 := cEmpty
						Z59->Z59_OBSOC	 := cEmpty
						Z59->Z59_ITEM		 := Self:Itens[nX][1]
						Z59->Z59_PRODUT	 := Self:Itens[nX][2]
						Z59->Z59_QUANT	 := Self:Itens[nX][4]
						Z59->Z59_PRCVEN	 := Self:Itens[nX][5]
						Z59->Z59_TOTAL	 := Self:Itens[nX][6]
						Z59->Z59_DESCRI	 := Self:Itens[nX][3]
						Z59->Z59_CULTUR	 := Self:Itens[nX][7]
					MsUnlock("Z59")
				
			Next nX++	
			lReturn	:= .T.
	Else
			lReturn := .F.
		
	EndIf 
  Z59->(DbCloseArea())
Return lReturn

Method Alter(cEmFil,Codigo) Class Contract
Return

Method Delete(cEmpFil,cCodigo) Class Contract
Return

Method Find(cEmpFil,cCodigo) Class Contract
	Local lReturn		:= .F.
	Local nSaldo		:= 0
	Local nEntregu	:= 0
	Local aSaldo		:= {}
	Local cIndex		:= cEmpFil+cCodigo

	DbSelectArea('Z59')
	Z59->(DbSetOrder(1))//Z59_FILIAL + Z59_NUM
	Z59->(DbSeek(cIndex))
	If  Found()
			Self:Filial				:= Z59->Z59_FILIAL
 			Self:Pedido				:= Z59->Z59_PED
 			Self:Status				:= Z59->Z59_STATUS
 			Self:Contrato			:= Z59->Z59_NUM
 			Self:TipCliente		:= Z59->Z59_TPCLI
 			Self:Cliente			:= Z59->Z59_CLIENT
 			Self:Vendedores		:= Z59->Z59_VEND1
			Self:Emissao			:= Z59->Z59_DATA
 			Self:Vencimento		:= Z59->Z59_VCTO
 			Self:Moeda				:= Z59->Z59_MOEDA
 			Self:Pedido				:= Z59->Z59_PED// um arry ou um Objeto
 			Self:PTAX					:= Z59->Z59_PTAX
			Do While !Z59->(Eof()) .And. Z59_NUM == Self:Contrato
				 Self:ValUnit:= Z59->Z59_PRCVEN
				 aSaldo := Self:GetSaldo(Z59->Z59_ITEM)
				 If  aSaldo[1] == .T.
					   nSaldo		:= aSaldo[2]
						 nEntregu	:= aSaldo[3]
						 Self:SetItem(Self,Z59->Z59_ITEM,Z59->Z59_CULTUR,Z59->Z59_QUANT,nSaldo,nEntregu)
				 Else
				 		 Self:SetItem(Self,Z59->Z59_ITEM,Z59->Z59_CULTUR,Z59->Z59_QUANT,nSaldo,nEntregu)					 
				 EndIf			 
				 Z59->(DbSkip())
			EndDo
			lReturn						:= .T.
	Else

	EndIf
	Z59->(DbCloseArea())
Return lReturn

//ITEM  
//PRODUT
//DESCRI
//QUANT 
//PRCVEN
//TOTAL 
//CULTUR		
//-------------------------------------------------------------------
/*/{Protheus.doc} Method 
 description Metodo SetItem
 @author  Joao Manoel 
 @since   04/08/2018
 @version 12.1.17
/*/
//-------------------------------------------------------------------	
Method SetItem(oObjet,cItem,cProd,cCultura,nQuant,nSaldo,nEntreg) Class Contract
 Local cLog		:= ""
 Local cHora  := AllTrim(SubStr(Time(),1,6))
 Local cDate	:= AllTrim(CvalToChar(Date()))
	
	If  Empty(oObjet)
			cLog	:='Objeto recebido no parametro do Metodo SetItem na Class Contract  esta vazio'+PulaLinha+''
			MemoWrite('\temp\class_contract_setitem_'+cHora+'_'+cDate+'.txt',cLog)
			ConOut('----------------------------------------------------------'+PulaLinha+'')
			ConOut(cLog)
			ConOut('----------------------------------------------------------'+PulaLinha+'')

	Else
			/*Aadd(Self:Itens,{{"ITEM",cItem},{"PRODUTO",cProd},{"CONTRATO",oObjet:Contrato},{"QUANT",nQuant},{"TOTAL",(oObjet:ValUnit*nQuant)},;
												{"SALDO",nSaldo},{"ENTREGUE",nEntreg},{"CULTURA",AllTrim(cCultura)}})*/
      Aadd(Self:Itens,{{"ITEM",cItem,"PRODUTO",cProd,"CONTRATO",oObjet:Contrato,"QUANT",nQuant,"TOTAL",(oObjet:ValUnit*nQuant),;
												"SALDO",nSaldo,"ENTREGUE",nEntreg,"CULTURA",AllTrim(cCultura)}})												
		  
			cLog	:='Objeto recebido no parametro do Metodo SetItem na Class Contract preenchido, esse e o item '+cItem+''+PulaLinha+''
			ConOut('----------------------------------------------------------'+PulaLinha+'')
			ConOut(cLog)
			ConOut('----------------------------------------------------------'+PulaLinha+'')			
			MemoWrite('\temp\class_contract_setitem_'+cHora+'_'+cDate+'_'+cItem+'.txt',cLog)
	EndIf	
Return Self:Itens

Method FindAll() Class Contract
 Local cSQL				:= ""
 Local cWhAll			:= " " //Where all todos o registros
 Local dDtFim			:= ""
 Local dDtIni			:= ""
 Local cWhEmissao	:= " Z59_DATA>='"+dDtIni+"' AND Z59_DATA'"+dDtFim+"' "//Where por data de emissao 
 Local cOrderBy		:= " ORDER BY Z59.R_E_C_N_O_ "
 
 
  cSQL +=" SELECT TOP 100 "+PulaLinha+""
  cSQL +=" Z59_FILIAL 'FILIAL',	"													+PulaLinha+""
	cSQL +=" Z59_NUM 'CONTRATO', 	"													+PulaLinha+""
	cSQL +=" Z59_STATUS 'STATUS',	"													+PulaLinha+""
	cSQL +=" Z59_TPCLI 'TIPOCLI'," 													+PulaLinha+""
  cSQL +=" Z59_CLIENT+'-'+Z59_LOJA+'-'+A1_NOME 'CLIENTE',"+PulaLinha+""
	cSQL +=" Z59_VEND1+'-'+A3_NOME 'VENDEDOR',"  						+PulaLinha+""
  cSQL +=" Z59_COMIS1 'COMISSAO',"												+PulaLinha+""
	cSQL +=" Z59_DATA 'EMISSAO',"														+PulaLinha+""
	cSQL +=" Z59_VCTO 'VENCIMENTO',"												+PulaLinha+""
	cSQL +=" Z59_MOEDA 'MOEDA',"														+PulaLinha+""
  cSQL +=" Z59_PTAX 'TAXA',"															+PulaLinha+""
	cSQL +=" Z59_ITEM 'ITEM',"															+PulaLinha+""
	cSQL +=" Z59_PRODUT 'PRODUTO',"													+PulaLinha+""
	cSQL +=" Z59_DESCRI 'DESCRICAO',     "									+PulaLinha+""
  cSQL +=" Z59_CULTUR+'-'+Z24_DESC 'CULTURA',"						+PulaLinha+""
	cSQL +=" Z59_EMBAL 'EMBALAGEM',"												+PulaLinha+""
	cSQL +=" Z59_QUANT 'QUANT',"														+PulaLinha+""
  cSQL +=" Z59_PRCVEN 'PRCVEN',"													+PulaLinha+""
	cSQL +=" Z59_TOTAL 'TOTAL',"														+PulaLinha+""
	cSQL +=" Z59_OBS 'OBS',"																+PulaLinha+""
	cSQL +=" Z59_FRETE 'FRETE',"														+PulaLinha+""
  cSQL +=" Z59_LOCAL 'LOCAL',"														+PulaLinha+""
	cSQL +=" Z59_PED 'PED',"																+PulaLinha+""
	cSQL +=" Z59_DESC 'DESC',"															+PulaLinha+""
	cSQL +=" Z59_CONDPA 'CONDPA',"													+PulaLinha+""
  cSQL +=" SC6.C6_QTDENT 'ENTREGUE',"											+PulaLinha+""
	cSQL +=" (SC6.C6_QTDVEN - SC6.C6_QTDENT) 'SALDO',"			+PulaLinha+""
  cSQL +=" Z59_OBSOC 'OBSOC',"														+PulaLinha+""
	cSQL +=" Z59_SAFRA 'SAFRA'"															+PulaLinha+""
  cSQL +=" FROM "+RetSqlName('Z59')+" Z59 " 							+PulaLinha+""
 
  cSQL +=" INNER JOIN "+RetSqlName('SA1')+" SA1 ON Z59_CLIENT = A1_COD " +PulaLinha+""
  cSQL +=" AND Z59_LOJA = A1_LOJA "+PulaLinha+""
  cSQL +=" AND SA1.D_E_L_E_T_='' "+PulaLinha+""

  cSQL +=" LEFT JOIN "+RetSqlName('SA3')+" SA3 ON Z59_VEND1 = A3_COD "+PulaLinha+""
  cSQL +=" AND SA3.D_E_L_E_T_='' "+PulaLinha+""

  cSQL +=" LEFT JOIN "+RetSqlName('Z24')+" Z24 ON Z59_CULTUR = Z24_CODIGO "+PulaLinha+""
  cSQL +=" AND Z24_ATIVO=1 "+PulaLinha+""
  cSQL +=" AND Z24.D_E_L_E_T_=''"+PulaLinha+""

  cSQL +=" LEFT JOIN "+RetSqlName('SC6')+" SC6 ON Z59_PED = C6_NUM " +PulaLinha+""
  cSQL +=" AND Z59_CLIENT = C6_CLI" +PulaLinha+""
  cSQL +=" AND Z59_LOJA	= C6_LOJA" +PulaLinha+""
  cSQL +=" AND Z59_PRODUT = C6_PRODUTO"+PulaLinha+""
  cSQL +=" AND SC6.D_E_L_E_T_=''" +PulaLinha+""

  cSQL +=" WHERE Z59.D_E_L_E_T_='' "+PulaLinha+""

	MemoWrite('\temp\contract_FindAll.txt',cSQL)

	cSQL+= ChangeQuery(cSQL) 
	If   Select('cAlias') > 0
		   DbSelectArea('cAlias')
			 cAlias->(DbCloseArea())
	EndIf
	
	TcQuery cSQL New Alias 'cAlias'
	DbSelectArea('cAlias')
	cAlias->(DbGoTop())
	If  Empty(cAlias->CONTRATO)
		  ConOut('---------------------------------------------'+PulaLinha+'')
      ConOut('Consulta Class Contract Metodo FindAll() Vazia'+PulaLinha+'')
      ConOut('---------------------------------------------'+PulaLinha+'')			
	Else
		  ConOut('--------------------------------------------------------------'+PulaLinha+'')
      ConOut('Consulta Class Contract Metodo FindAll() realizada com sucesso!'+PulaLinha+'')
      ConOut('--------------------------------------------------------------'+PulaLinha+'')		
	EndIf	
		
Return 'cAlias'

Method GVolume() Class Contract
 Local nTotal			:= 0
 Local cSQL				:= ""

 cSQL:=" SELECT SUM(Z59_QUANT) TOTAL FROM "+RetSqlName('Z59') +" "				+PulaLinha+""
 cSQL+=" Z59 WHERE Z59_CONTRA='"+Self:Contrato+"' AND Z59.D_E_L_E_T_=''"	+PulaLinha+""

 MemoWrite('\temp\contract_GVolume_total'+AllTrim(Self:Contrato)+'.txt',cSQL)
 cSQL+= ChangeQuery(cSQL)

 If  Select('cAlias') > 0
	   DbSelectArea('cAlias') > 0
		 cAlias->(DbCloseArea())
 EndIf

 TcQuery cSQL New Alias 'cAlias'
 DbSelectArea('cAlias')
 nTotal	:= Iif(ValType(cAlias->TOTAL)== 'C',Val(cAlias->TOTAL),cAlias->TOTAL)
 cAlias->(DbCloseArea()) 

Return nTotal

Method GetSaldo(cItem) Class Contract
 Local cSQL			:= ""
 Local aReturn	:= {}
 
 cSQL +=" SELECT "+PulaLinha+""
 cSQL +=" Z59_FILIAL 'FILIAL',Z59_NUM 'CONTRATO',Z59_PED 'PED',"						+PulaLinha+""
 cSQL +=" Z59_ITEM 'ITEM',Z59_PRODUT 'PRODUTO',Z59_DESCRI 'DESCRICAO',"			+PulaLinha+""
 cSQL +=" SC6.C6_QTDENT 'ENTREGUE',(SC6.C6_QTDVEN - SC6.C6_QTDENT) 'SALDO' "+PulaLinha+""
 cSQL +=" FROM "+RetSqlName('Z59')+" Z59 " 																	+PulaLinha+""
 
 cSQL +=" INNER JOIN "+RetSqlName('SA1')+" SA1 ON Z59_CLIENT = A1_COD " 		+PulaLinha+""
 cSQL +=" AND Z59_LOJA = A1_LOJA "																					+PulaLinha+""
 cSQL +=" AND SA1.D_E_L_E_T_='' "																						+PulaLinha+""

 cSQL +=" LEFT JOIN "+RetSqlName('SA3')+" SA3 ON Z59_VEND1 = A3_COD "				+PulaLinha+""
 cSQL +=" AND SA3.D_E_L_E_T_='' "																						+PulaLinha+""

 cSQL +=" LEFT JOIN "+RetSqlName('Z24')+" Z24 ON Z59_CULTUR = Z24_CODIGO "	+PulaLinha+""
 cSQL +=" AND Z24_ATIVO=1 "																									+PulaLinha+""
 cSQL +=" AND Z24.D_E_L_E_T_=''"																						+PulaLinha+""

 cSQL +=" LEFT JOIN "+RetSqlName('SC6')+" SC6 ON Z59_PED = C6_NUM " 				+PulaLinha+""
 cSQL +=" AND Z59_CLIENT = C6_CLI" 																					+PulaLinha+""
 cSQL +=" AND Z59_LOJA	= C6_LOJA" 																					+PulaLinha+""
 cSQL +=" AND Z59_PRODUT = C6_PRODUTO"																			+PulaLinha+""
 cSQL +=" AND SC6.D_E_L_E_T_=''" 																						+PulaLinha+""

 cSQL +=" WHERE Z59.D_E_L_E_T_='' "																					+PulaLinha+""
 cSQL +=" AND Z59_NUM ='"+AllTrim(Self:Contrato)+"' "												+PulaLinha+""
 cSQL +=" AND Z59_ITEM='"+AllTrim(cItem)+"' "																+PulaLinha+""

 MemoWrite('\temp\contract_getSaldo.txt',cSQL)
 cSQL+= ChangeQuery(cSQL) 

 If	 Select('cAlias') > 0
		 DbSelectArea('cAlias')
		 cAlias->(DbCloseArea())
 EndIf

 TcQuery cSQL New Alias 'cAlias'
 DbSelectArea('cAlias')
 If  Empty(cAlias->PED)
	 	 aReturn	:={.F.,(cAlias->SALDO),(cAlias->ENTREGUE)}
 Else
	 	 aReturn	:={.T.,(cAlias->SALDO),(cAlias->ENTREGUE)}
 EndIf
 cAlias->(DbCloseArea())
Return aReturn
