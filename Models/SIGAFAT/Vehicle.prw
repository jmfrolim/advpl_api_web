#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Define  PulaLinha   Chr(13)+Chr(10)

Class Vehicle
  Data Filial
  Data Id
  Data erp_id
  Data Codigo
  Data Placa
  Data Placa2
  Data Placa3
  Data Cidade
  Data RNTC
  Data CAP_MINIMA
  Data CAP_MAXIMA
  Data UF
  Data VOL_MAXIMO
  Data Motorista
  Data Status
  Data Modelo
  Data Tipo

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter(aDados)
  Method Delete(cIndexFil,cCodigo)
  Method Find(cIndexFil,cCodigo)
  Method FindAll(aPars)
  Method Clone(Object)

EndClass

Method New() Class Vehicle 
Return Self

Method GetCod() Class Vehicle 
Return Self:Placa

//-------------------------------------------------------------------
/*/{Protheus.doc} function
  description
  @author  Joao Manoel 
  @since   23/09/18
  @version 1.0
  @build   12.1.17
  @param aDados -> Array   
    [1] Placa
    [2] UF
    [3] Status -> 1 Sim(Nao Bloqueado); 2 Nao (Bloqueado)
    [4] Modelo
    [5] Filial
    [6] Tipo -> 1 Proprio; 2 Terceiro; 3 Agregado
/*/
//-------------------------------------------------------------------
Method Insert() Class Vehicle
  Local lReturn   := .F.
  Local lExist    := Self:Find(FwxFilial('DA3'),Self:Placa)

  If  lExist
      lReturn   := .F.

  Else
      Begin Transaction
        RecLock('DA3',.T.)
         DA3->DA3_FILIAL   := FwxFilial('DA3')
         DA3->DA3_COD      := Self:Placa
         DA3->DA3_DESC     := Self:Modelo
         DA3->DA3_PLACA    := Self:Placa
         DA3->DA3_ESTPLA   := Self:UF
         DA3->DA3_ATIVO    := Iif(Len(Self:Status)>1,SubStr(Self:Status,1,1),Self:Status)
         DA3->DA3_FROVEI   := Iif(Len(Self:Tipo)>1,SubStr(Self:Tipo,1,1),Self:Tipo)
        DA3->(MsUnLock())        
   
        If Self:Find(FwxFilial('DA3'),Self:Placa)
           lReturn := .T.
        Else
           lReturn := .F.
        EndIf
      End Transaction      
  EndIf
  
Return lReturn

Method Alter() Class Vehicle 
  Local lReturn
  If  Self:Find()
      Begin Transaction
        RecLock('DA3',.F.)
          DA3->DA3_FILIAL   := FwxFilial('DA3')
          DA3->DA3_COD      := Self:Placa
          DA3->DA3_DESC     := Self:Modelo
          DA3->DA3_PLACA    := Self:Placa
          DA3->DA3_ESTPLA   := Self:UF
          DA3->DA3_ATIVO    := Iif(Len(Self:Status)>1,SubStr(Self:Status,1,1),Self:Status)
          DA3->DA3_FROVEI   := Iif(Len(Self:Tipo)>1,SubStr(Self:Tipo,1,1),Self:Tipo)
        DA3->(MsUnLock())           
        If  Self:Find(FwxFilial('DA3'),Self:Placa)
            lReturn := .T.
        Else
            lReturn := .F.
        EndIf
      End Transaction 
  Else
      lReturn  := .T.
  EndIf   

Return lReturn

Method Delete(cEmpFil,cCodigo) Class Vehicle 
  Local lReturn

Return lReturn

Method Find(cIndexFil,cCodigo) Class Vehicle
 Local  lReturn
 Local  cIndex   := cIndexFil+cCodigo //1 ->DA3_FILIAL+DA3_COD
 Local  cLog     := ''
 Local  dDate    := Date()
 Local  cHora    := Time()

 DbSelectArea('DA3')
 DA3->(DbSetOrder(1))
 DA3->(DbSeek(cIndex))
 If  Found()     
     Self:Filial    := DA3->DA3_FILIAL
     Self:Codigo    := DA3->DA3_COD
     Self:Placa     := DA3->DA3_PLACA
     Self:UF        := DA3->DA3_ESTPLA
     Self:Status    := Iif(DA3->DA3_ATIVO == '1','1-ATIVO','2-BLOQUEADO')
     Self:Modelo    := DA3->DA3_DESC
     Self:RNTC      := DA3->DA3_RNTC
     lReturn    := .T.
     cLog :='A consulta do metodo Find Class Vehicle ok '+AllTrim(Str(Day(dDate)))+' mes '+AllTrim(Str(Month(dDate)))+' hora '+ cHora+'.'
     ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
     ConOut('')
     ConOut(cLog)
     ConOut('')
     ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
     MemoWrite('\temp\vehicle_find_dia_'+AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))+'.txt',cLog)

 Else
     lReturn    := .F.   
     cLog :='A consulta do metodo Find Class Vehicle falha '+AllTrim(Str(Day(dDate)))+' mes '+AllTrim(Str(Month(dDate)))+' hora '+ cHora+'.'
     ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
     ConOut('')
     ConOut(cLog)
     ConOut('')
     ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
     MemoWrite('\temp\vehicle_find_dia_'+AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))+'.txt',cLog)
 EndIf
 DA3->(DbCloseArea())
Return lReturn

Method FindAll(aPars) Class Vehicle
 Local cSQL   := ""
 Local cLog   := ""
 Local dDate  := Date()
 Local cHora  := Time()
 Local cTop   := ""
 Local cWhere := ""

 If  !Empty(aPars)
     Do Case
        Case (aPars[1] == 'all' .Or. aPars[1]=='ALL')  .And. !Empty(aPars[4])
             cTop    := " TOP  "+aPars[4]+""
             cWhere  := " WHERE DA3.D_E_L_E_T_='' ORDER BY DA3.R_E_C_N_O_ DESC  "+PulaLinha+""
        
        Case Empty(aPars[1]) .And. !Empty(aPars[2]) .And. Empty(aPars[3]) .And. Empty(aPars[4])             
             cWhere := " WHERE DA3.D_E_L_E_T_='' AND DA3_DESC='"+aPars[2]+"'  "
             cWhere += " ORDER BY DA3.R_E_C_N_O_ DESC "+PulaLinha+""
        
        Case Empty(aPars[1]) .And. Empty(aPars[2]) .And. !Empty(aPars[3]) 
             If  (ValType(aPars[3])=='C' .And. Len(aPars) > 1)
                 cWhere := "WHERE DA3.D_E_L_E_T_='' AND DA3_ATIVO='"+SubStr(aPars,1,1)+"'"
             Else
                 cWhere := "WHERE DA3.D_E_L_E_T_='' AND DA3_ATIVO='"+aPars[3]+"'"
             EndIf
             
             cWhere += " ORDER BY DA3.R_E_C_N_O_ DESC "+PulaLinha+""
        
        Case !Empty(aPars[5]) .And. aPars[1]='all'
             cWhere := " WHERE DA3.D_E_L_E_T_='' AND DA3.R_E_C_N_O_='"+aPars[5]+"'  "+PulaLinha+""
     EndCase
 Else
     cTop   := " TOP 100 " 
     cWhere := " WHERE DA3.D_E_L_E_T_=''  ORDER BY DA3.R_E_C_N_O_ DESC "+PulaLinha+""
 EndIf
 

 cSQL:=" SELECT     "+cTop+"                       "+PulaLinha+"" 
 cSQL+=" DA3.R_E_C_N_O_ AS ID,                     "+PulaLinha+""
 cSQL+=" DA3_FILIAL FILIAL,DA3_COD CODIGO,         "+PulaLinha+""
 cSQL+=" DA3_DESC MODELO,DA3_PLACA PLACA,          "+PulaLinha+""
 cSQL+=" DA3_PLACA2 PLACA2,DA3_PLACA3 PALCA3,      "+PulaLinha+""
 cSQL+=" DA3_MUNPLA CIDADE,DA3_ESTPLA UF,          "+PulaLinha+""
 cSQL+=" DA3_RNTC RNTC,DA3_CAPACN CAP_MINIMA,      "+PulaLinha+""
 cSQL+=" DA3_CAPACM CAP_MAXIMA,                    "+PulaLinha+""
 cSQL+=" CASE DA3_ATIVO                            "+PulaLinha+""
 cSQL+="  WHEN '1' THEN '1-ATIVO'                  "+PulaLinha+""
 cSQL+="  ELSE '2-BLOQUEADO'                       "+PulaLinha+""
 cSQL+=" END STATUS,                               "+PulaLinha+""
 cSQL+=" DA3_VOLMAX VOL_MAXIMO,                    "+PulaLinha+""
 cSQL+=" DA3_MOTORI MOTORISTA,                     "+PulaLinha+""
 cSQL+=" DA3_FILATU FILIAL_ATUAL,                  "+PulaLinha+""
 cSQL+=" CASE DA3_FROVEI                           "+PulaLinha+""
 cSQL+="  WHEN 1 THEN '1-PROPRIO'                  "+PulaLinha+""
 cSQL+="  WHEN 2 THEN '2-TERCEIRO'                 "+PulaLinha+""
 cSQL+="  WHEN 3 THEN '3-AGREGADO'                 "+PulaLinha+""
 cSQL+=" END TIPO                                  "+PulaLinha+""
 cSQL+=" FROM "+ RetSqlName('DA3')+  "  DA3        "+PulaLinha+""
 cSQL+=""+ cWhere+"                                "+PulaLinha+""

  MemoWrite('\temp\Vehicle_FindAll.txt',cSQL)

  cSQL+= ChangeQuery(cSQL)
  
  If  Select('cAlias') > 0
      DbSelectArea('cAlias')
      cAlias->(DbCloseArea())
  EndIf
  
  TcQuery cSQL New Alias cAlias
  DbSelectArea('cAlias')
  If  Empty(cAlias->PLACA)
      cLog :='A consulta do metodo FindAll da Class Vehicle restornou vazia dia '+AllTrim(Str(Day(dDate)))+' mes '+AllTrim(Str(Month(dDate)))+' hora '+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))+' .'
      ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
      ConOut('')
      ConOut(cLog)
      ConOut('')
      ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
      MemoWrite('\temp\result_vehicle_finall dia_'+AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))+'.txt',cLog)

  Else
      cLog :='A consulta do metodo FindAll da Class Vehicle foi realizada com sucesso '+AllTrim(Str(Day(dDate)))+' mes '+AllTrim(Str(Month(dDate)))+' hora '+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))+' .'
      ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
      ConOut('')
      ConOut(cLog)
      ConOut('')
      ConOut('------------------------------------------------------------------------------------------------------------------------------------------------------------------')
      MemoWrite('\temp\result_vehicle_finall dia_'+AllTrim(Str(Day(dDate)))+'_mes_'+AllTrim(Str(Month(dDate)))+'_hora_'+AllTrim(SubStr(cHora,1,2))+'_'+AllTrim(SubStr(cHora,4,2))+'_'+AllTrim(SubStr(cHora,4,2))+'.txt',cLog)
  EndIf 

Return 'cAlias'

/*/{Protheus.doc} Clone
    (long_description)
    @type  Method
    @author user
    @since date
    @version version
    @param param, param_type, param_descr
    @return return, return_type, return_description
    @example
    (examples)
    @see (links_or_references)
/*/
Method Clone(Object) Class Vehicle
 Local cLog   := ""  
 Local nX:= 1
 For nX:= 1 To 2
     ConOut(" Filial: "+AllTrim(Object:Filial) +""+PulaLinha  )
     ConOut(" ID    : "+CvalToChar(Object:erp_id)+""+PulaLinha)
     ConOut(" Placa : "+AllTrim(Object:Placa)  +""+PulaLinha  )
     ConOut(" UF    : "+AllTrim(Object:UF)     +""+PulaLinha  )
     ConOut(" Status: "+AllTrim(Object:Status) +""+PulaLinha  )
     ConOut(" Modelo: "+AllTrim(Object:Modelo) +""+PulaLinha  )
     ConOut(" Tipo  : "+AllTrim(Object:Tipo)   +""+PulaLinha  )
     nX++
 Next
  MemoWrite('\temp\vehicle_method_clone_log.txt',cLog)
  Do While !Empty(Object)
      Self:Filial    := AllTrim(Object:Filial)
      Self:Id        := Object:erp_id
      Self:Placa     := AllTrim(Object:Placa)
      Self:UF        := AllTrim(Object:UF)
      Self:Status    := AllTrim(Object:Status)
      Self:Modelo    := AllTrim(Object:Modelo)
      Self:Tipo      := AllTrim(Object:Tipo)
      Object         := ""
  EndDo
Return Self