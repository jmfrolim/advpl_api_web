#Include 'Protheus.ch'
#Include 'TbiConn.ch'


Class GeraLog
 Data Filial
 Data DataIni
 Data HoraIni
 Data DataFim
 Data HoraFim
 Data Rotina
 Data Query
 Data Usuario
 Data SO
 Data UserSO  //usuario do SO
 Data Maquina //nome do computador
 Data Status
 Data PathLog
 Data LogGeral // estrutura em json com a varias descricoes

 Method New() Constructor
 Method GetSistema()
 Method GetCPU()
 Method GetUsers()
 Method SetRotina(cRotina)
 Method SetQuery(cSql)
 Method SetPath()
EndClass

Method New() Class GeraLog
 Self:DataIni    := Date()
 Self:GetUsers()
 Self:GetCPU()
Return Self

Method GetSistema() Class GeraLog
  If  (GetRemoteType() == 2)                     
      Self:SO:="Linux" 
  Else
      Self:SO:="Windows"
  EndIf  
Return Self:SO

Method GetCPU() Class GeraLog  
Return Self:Maquina  := ComputerName()

Method GetUsers() Class GeraLog
 Self:UserSO  := LogUserName()
 Self:Usuario := UsrRetName(RetCodUsr())
Return Self

Method SetPath() Class GeraLog
 Local cSO  := GetSistema()
 If  cSO $ 'Linux'
     Self:PathLog :='\temp\'
 Else
     Self:PathLog := 'c:\temp\'
 EndIf
 
Return Self