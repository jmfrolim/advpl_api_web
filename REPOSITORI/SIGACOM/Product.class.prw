#Include 'Totvs.ch'
#Include 'TopConn.ch'
#Define  PulaLinha   Chr(13)+Chr(10)

Class Product
     
 Data Filial        
 Data Codigo        
 Data Descricao     
 Data Tipo          
 Data Ncm           
 Data Unidade       
 Data Armazem
 Data PercICM       
 Data PercIPI       
 Data CodBar
 Data CusStand  
 Data ContaCon      
 Data PrecoVenda    
 Data MRP           
 Data ContParceria  
 Data ContSegSocia 
 Data TIPOCQ        
 Data IRRF          
 Data CtoEndereco   
 Data DescCientifico
 Data Collection
 Data Grupo
 Data Origem
 Data Garant
 Data Naturez 
 Data Agricul

 Method New() Constructor
 Method GetCod(cFili,cTipo)
 Method SetCod()
 Method SetDesc()
 Method GetDesc(cCodigo)
 Method Insert()
 Method Alter()
 Method Delete(cEmpFil,cCodigo)
 Method IsExist(cFili,cCodigo,cTipo)
 Method GetClone(oObjetc)
 Method GetAll()
 Method GetRange(oObjetc)

EndClass

//-------------------------------------------------------------------
/*/{Protheus.doc} Product:New
description Method New
@author  Joao Manoel Freitas Rolim
@since   21/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method New() Class Product
 Self:Collection    := {}
Return Self
//-------------------------------------------------------------------
/*/{Protheus.doc} Product:GetCod
description Method GetCod
@author  Joao Manoel Freitas Rolim
@since   21/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method GetCod(cFili,cTipo) Class Product

Return Self:Codigo := U_ZUF02112(cTipo)
//-------------------------------------------------------------------
/*/{Protheus.doc} Product:IsExist
description Method IsExist
@author  Joao Manoel Freitas Rolim
@since   21/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method IsExist(cFili,cCodigo,cTipo) Class Product
 Local lReturn
 Local nIndex  := Iif(Empty(cTipo),1,2)  
 Local cIndex  := Iif(nIndex == 2,cFili+cTipo+cCodigo,cFili+cCodigo)

    DbSelectArea('SB1')
    SB1->(DbSetOrder(nIndex))//B1_FILIAL+B1_TIPO+B1_COD
    SB1->(MsSeek(cIndex))
    If  Found()    
        Self:Codigo     := SB1->B1_COD
        Self:Descricao  := SB1->B1_DESC
        Self:Tipo       := SB1->B1_TIPO
        Self:PrecoVenda := SB1->B1_PRV1
        Self:CusStand   := SB1->B1_CUSTD
        Self:Armazem    := SB1->B1_LOCPAD
        lReturn         := .T.
    Else            
        lReturn:= .F.
    EndIf
    SB1->(DbCloseArea())

Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Product:Insert
description Method Insert
@author  Joao Manoel Freitas Rolim
@since   21/09/2018
@version 1.1
/*/
//--------------------------------------------------------------------
Method Insert() Class Product
 Local lExist   := Self:IsExist(cFilAnt,Self:Codigo,)
 Local lReturn  := .F.
 Private lMsErroAuto := .F.

 If  lExist
     lReturn := .F.
 Else

    aVetor:= {{"B1_COD"         ,Self:Codigo                                                                  ,Nil},;
    		      {"B1_DESC"        ,Self:Descricao                                                               ,Nil},;
			        {"B1_TIPO"        ,Self:Tipo                                                                    ,Nil},; 
              {"B1_POSIPI"      ,Self:Ncm                                                                     ,Nil},;
			        {"B1_UM"          ,Self:Unidade                                                                 ,Nil},; 
			        {"B1_LOCPAD"      ,Self:Armazem                                                                 ,Nil},; 
			        {"B1_PICM"        ,Iif( ValType(Self:PercICM) ==   'C', Val(Self:PercICM),  Self:PercICM )      ,Nil},; 
			        {"B1_IPI"         ,Iif( ValType(Self:PercIPI) ==   'C', Val(Self:PercIPI),  Self:PercIPI )      ,Nil},; 
			        {"B1_PRV1"        ,Iif( ValType(Self:PrecoVenda)== 'C', Val(Self:PrecoVenda), Self:PrecoVenda)  ,Nil},; 
			        {"B1_TIPOCQ"      ,Self:TIPOCQ                                                                  ,Nil},; 
			        {"B1_CONTRAT"     ,Self:ContParceria                                                            ,Nil},; 
			        {"B1_LOCALIZ"     ,Self:CtoEndereco                                                             ,Nil},; 
			        {"B1_CODBAR"      ,Self:CodBar                                                                  ,Nil},; 
			        {"B1_IRRF"        ,Self:IRRF                                                                    ,Nil},; 
			        {"B1_CONTSOC"     ,Self:ContSegSocia                                                            ,Nil},; 
			        {"B1_CONTA"       ,Self:ContaCon                                                                ,Nil},;
			        {"B1_GRUPO"       ,Self:Grupo                                                                   ,Nil},;
			        {"B1_ORIGEM"      ,Self:Origem                                                                  ,Nil},;
			        {"B1_GARANT"      ,Self:Garant                                                                  ,Nil},;
			        {"B1_NATUREZ"     ,Self:Naturez                                                                 ,Nil},;
			        {"B1_REGAGRI"     ,Self:Agricul                                                                 ,Nil},;
			        {"B1_MRP"         ,Self:MRP                                                                     ,Nil}}

    MsExecAuto({|x,y| Mata010(x,y)},aVetor,3)

    If  lMsErroAuto
        MostraErro()
    Else
        ConOut("Ok") 
        lReturn := .T.
    EndIf      
 EndIf

Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Product:Alter
description Method Alter
@author  Joao Manoel Freitas Rolim
@since   21/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method Alter() Class Product
 Local      lReturn     := .F.
 Private    lMsErroAuto := .F.

 If !Empty(Self:Codigo)
    aVetor:= {{"B1_COD"     ,Self:Codigo          ,Nil},;
              {"B1_DESC"    ,Self:Descricao           ,Nil},;
              {"B1_UM"      ,Self:Unidade           ,Nil},; 
              {"B1_LOCPAD"  ,Self:Armazem           ,Nil},; 
              {"B1_LOCALIZ" ,Self:CtoEndereco          ,Nil},; 
              {"B1_CODBAR"  ,Self:CodBar          ,Nil},; 
              {"B1_MRP"     ,Self:MRP          ,Nil}}
    
    MsExecAuto({|x,y| Mata010(x,y)},aVetor,3)

    If lMsErroAuto       
       MostraErro()    
    Else
        ConOut('Aviso Registro Alterado')        
        lReturn := .T.
    EndIf
 Else
     ConOut("")
     ConOut('Erro o produto passado para altera�?o nao existe, o atributo Codigo esta vazio!')
     ConOut("")
 EndIf

Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Product:Delete
description Method Alter
@author  Joao Manoel Freitas Rolim
@since   21/09/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method Delete(cEmpFil,cCodigo) Class Product
 Local     aVetor      := {}
 Local     cTipo       := SubStr(cCodigo,1,2)
 Local     lExist      := Self:IsExist(cEmpFil,cCodigo,cTipo)
 Local     lReturn     
 Private   lMsErroAuto := .F.  

  If lExist
     aVetor := {{"B1_COD"  ,Self:Codigo     ,Nil},;
                {"B1_DESC" ,Self:Descricao  ,Nil}}
     
     MsExecAuto({|x,y| Mata010(x,y)},aVetor,5)
     
     If lMsErroAuto
        MostraErro()
        lReturn     := .F.     
     Else     
        lReturn     := .T.           
     EndIf           
  Else
     lReturn        := .F.
  EndIf 

Return lReturn

//-------------------------------------------------------------------
/*/{Protheus.doc} Product:GetClone
description Method GetClone
@author  Joao Manoel Freitas Rolim
@since   06/10/2018
@version 1.0
/*/
//--------------------------------------------------------------------
Method GetClone(oObjetc) Class Product
 While !Empty(oObjetc)
        Self:Filial        := oObjetc:Filial        
        Self:Codigo        := oObjetc:Codigo        
        Self:Descricao     := oObjetc:Descricao     
        Self:Tipo          := oObjetc:Tipo          
        Self:Ncm           := oObjetc:Ncm           
        Self:Unidade       := oObjetc:Unidade       
        Self:Armazem       := oObjetc:Armazem
        Self:PercICM       := oObjetc:PercICM       
        Self:PercIPI       := oObjetc:PercIPI       
        Self:CodBar        := oObjetc:CodBar
        Self:PrecoVenda    := oObjetc:PrecoVenda    
        Self:MRP           := oObjetc:MRP           
        Self:ContParceria  := oObjetc:ContParceria  
        Self:ContSegSocia  := oObjetc:ContSegSocia 
        Self:TIPOCQ        := oObjetc:TIPOCQ        
        Self:IRRF          := oObjetc:IRRF          
        Self:CtoEndereco   := oObjetc:CtoEndereco
        Self:ContaCon      := oObjetc:ContaCon
        Self:Grupo         := oObjetc:Grupo   
        Self:Origem        := oObjetc:Origem  
        Self:Garant        := oObjetc:Garant  
        Self:Naturez       := oObjetc:Naturez 
        Self:Agricul       := oObjetc:Agricul 
        oObjetc := ""        
 EndDo   
Return Self


Method GetAll() Class Product
 Local cSQL    := ""
    
    cSQL+=" SELECT B1_COD AS CODIGO,B1_DESC AS DESCRICAO,B1_TIPO AS TIPO,"+PulaLinha+""
    cSQL+=" B1_PRV1 AS PRECOVENDA,B1_LOCPAD ARMAZEM,B1_CONTA AS CONTA "+PulaLinha+""
    cSQL+=" FROM "+RetSqlName('SB1')+" SB1 "+PulaLinha+""
    cSQL+=" WHERE SB1.D_E_L_E_T_=''"    
    
    MemoWrite('\temp\produto_getall'+AllTrim(SubStr(Time(),1,6))+'.txt',cSQL)
    
    cSQL+= ChangeQuery(cSQL)    
    
    If  Select('cAlias') > 0
        DbSelectArea('cAlias')
        cAlias->(DbCloseArea())
    EndIf

    TcQuery cSQL New Alias cAlias
    DbSelectArea('cAlias')
    If  Empty(cAlias->CODIGO)
        ConOut()
        ConOut('Consulta Vazia')
        ConOut()
    Else
        ConOut()
        ConOut('Consulta realizada com sucesso1')
        ConOut()        
    EndIf   
    
Return 'cAlias'

Method GetRange(oObjetc) Class Product
 
 Aadd(Self:Collection, oObjetc) 

Return Self:Collection