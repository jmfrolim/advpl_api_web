
#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'
#Include 'RestFul.ch'


Class Contract 
 Data Filial
 Data Pedido
 Data Status
 Data Contrato
 Data TipoCliente
 Data oCliente
 Data Emissao
 Data aVendedores
 Data TipoVenda
 Data TipoFrete
 Data Vencimento
 Data Moeda
 Data aPedido// um arry ou um Objeto
 Data PTAX
 Data aItens
 Data ValUnit

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter(cEmFil,Codigo)
  Method Delete(cEmpFil,cCodigo)
  Method IsExist(cFili,Codigo)
  Method SetItem(oObjet,cItem,cCultura,nQuant,nSaldo,nEntreg)
	Method GVolume()
  Method GetAll()
	Method GetSaldo(cItem)
	

EndClass

Method New() Class Contract
	Self:aItens							 :={}
Return Self

Method GetCod() Class Contract
	Local cNumPV := ""
	Local cSQL := ""
	Local cNumMin:="200000"//numero inicial
			
		cSQL := " SELECT MAX(Z59_NUM) AS NUMERO FROM "+RetSQLName('Z59')+" Z59 "
		cSQL += " "
			
		If  Select("_Z59") <> 0
				DbSelectArea("_Z59")
				_Z59->(DbCloseArea())
		EndIf
		
		TcQuery cSQL New Alias "_Z59"
		
		DbSelectArea("_Z59")
		_Z59->(DbGoTop())
		
		If  _Z59->(Eof()) .Or. Empty(Alltrim(_Z59->NUMERO))
				cNumPV := cNumMin
		Else
				cNumPV := StrZero(Val(_Z59->NUMERO)+1,6)
		EndIf
		
		_Z59->(DbCloseArea())

Return Self:Pedido	:= cNumPV

Method Insert() Class Contract
  Local lReturn
  Local cEmpty	:= ""
  Local nZero		:= 0
  Local	nX

	If  !Empty(Self:aItens)
			For nX:= 1 To Len(Self:aItens)

					DbSelectArea('Z59')
					RecLock("Z59", .T.)						
						//Cabe�alho
						Z59->Z59_FILIAL	 := FwxFilial('Z59')
						Z59->Z59_NUM	 := Self:Pedido
						Z59->Z59_CONTRA	 := Self:Pedido
						Z59->Z59_CLIENT	 := Self:oCliente:Codigo
						Z59->Z59_LOJA	 := Self:oCliente:Loja
						Z59->Z59_VEND1	 := cEmpty
						Z59->Z59_VEND2	 := cEmpty
						Z59->Z59_COMIS1	 := nZero
						Z59->Z59_COMIS2	 := nZero
						Z59->Z59_PTAX	 := Self:PTAX
						Z59->Z59_MOEDA	 := Self:Moeda
						Z59->Z59_TPCLI	 := Self:oCliente:TipoVenda
						Z59->Z59_TIPOVE	 := cEmpty
						Z59->Z59_TPFRET	 := 'F'
						Z59->Z59_STATUS	 := "S"
						//Z59->Z59_DATA		 := Date() Criar esse campo na tabela
						Z59->Z59_VCTO		 := CtoD('29/11/2018')
						//Z59->Z59_LOCAL	 := cGetTpLoc
						//Z59->Z59_FRETE	 := cEmpty
						Z59->Z59_CONDPA	 := cEmpty
						Z59->Z59_OBS		 := cEmpty
						Z59->Z59_OBSOC	 := cEmpty
						Z59->Z59_ITEM		 := Self:aItens[nX][1]
						Z59->Z59_PRODUT	 := Self:aItens[nX][2]
						Z59->Z59_QUANT	 := Self:aItens[nX][4]
						Z59->Z59_PRCVEN	 := Self:aItens[nX][5]
						Z59->Z59_TOTAL	 := Self:aItens[nX][6]
						Z59->Z59_DESCRI	 := Self:aItens[nX][3]
						Z59->Z59_CULTUR	 := Self:aItens[nX][7]
					MsUnlock("Z59")
				
			Next nX++	
			lReturn	:= .T.
	Else
			lReturn := .F.
		
	EndIf 
  Z59->(DbCloseArea())
Return lReturn

Method Alter(cEmFil,Codigo) Class Contract
Return

Method Delete(cEmpFil,cCodigo) Class Contract
Return

Method IsExist(cEmpFil,cCodigo) Class Contract
	Local lReturn		:= .F.
	Local nSaldo		:= 0
	Local nEntregu	:= 0
	Local aSaldo		:= {}
	Local cIndex		:= cEmpFil+cCodigo

	DbSelectArea('Z59')
	Z59->(DbSetOrder(1))//Z59_FILIAL + Z59_NUM
	Z59->(DbSeek(cIndex))
	If  Found()
			Self:Filial				:= Z59->Z59_FILIAL
 			Self:Pedido				:= Z59->Z59_PED
 			Self:Status				:= Z59->Z59_STATUS
 			Self:Contrato			:= Z59->Z59_NUM
 			Self:TipoCliente	:= Z59->Z59_TPCLI
 			Self:oCliente			:= Z59->Z59_CLIENT
 			Self:aVendedores	:= Z59->Z59_VEND1
			Self:Emissao			:= Z59->Z59_DATA
 			Self:Vencimento		:= Z59->Z59_VCTO
 			Self:Moeda				:= Z59->Z59_MOEDA
 			Self:aPedido			:= Z59->Z59_PED// um arry ou um Objeto
 			Self:PTAX					:= Z59->Z59_PTAX
			Do While !Z59->(Eof()) .And. Z59_NUM == Self:Contrato
				 Self:ValUnit:= Z59->Z59_PRCVEN
				 aSaldo := Self:GetSaldo(Z59->Z59_ITEM)
				 If  aSaldo[1] == .T.
					   nSaldo		:= aSaldo[2]
						 nEntregu	:= aSaldo[3]
						 Self:SetItem(Self,Z59->Z59_ITEM,Z59->Z59_CULTUR,Z59->Z59_QUANT,nSaldo,nEntregu)
				 Else
				 		 Self:SetItem(Self,Z59->Z59_ITEM,Z59->Z59_CULTUR,Z59->Z59_QUANT,nSaldo,nEntregu)					 
				 EndIf			 
				 Z59->(DbSkip())
			EndDo
			lReturn						:= .T.
	Else

	EndIf
	Z59->(DbCloseArea())
Return lReturn

//ITEM  
//PRODUT
//DESCRI
//QUANT 
//PRCVEN
//TOTAL 
//CULTUR			
Method SetItem(oObjet,cItem,cCultura,nQuant,nSaldo,nEntreg) Class Contract
	Local lReturn
	//Local nValUnit	:= Iif(ValType(oObjet:ValUnit)!='N',Val(oObjet:ValUnit),oObjet:ValUnit)
	
	If  Empty(oObjet)
			ConOut('Objeto Produto esta vazio')
			lReturn	:= .F.
	Else
			Aadd(Self:aItens,{cItem,oObjet:Contrato,nQuant,(oObjet:ValUnit*nQuant),nSaldo,nEntreg,AllTrim(cCultura)})
			lReturn	:= .T.
	EndIf	
Return Self:aItens

Method GetAll() Class Contract
 Local cSQL				:= ""
 Local cWhAll			:= " " //Where all todos o registros
 Local dDtFim			:= ""
 Local dDtIni			:= ""
 Local cWhEmissao	:= " Z59_DATA>='"+dDtIni+"' AND Z59_DATA'"+dDtFim+"' "//Where por data de emissao 
 Local cOrderBy		:= " ORDER BY Z59.R_E_C_N_O_ "
 
 
  cSQL +=" SELECT TOP 100 "+Chr(13)+Chr(10)+""
  cSQL +=" Z59_FILIAL 'FILIAL',	"													+Chr(13)+Chr(10)+""
	cSQL +=" Z59_NUM 'CONTRATO', 	"													+Chr(13)+Chr(10)+""
	cSQL +=" Z59_STATUS 'STATUS',	"													+Chr(13)+Chr(10)+""
	cSQL +=" Z59_TPCLI 'TIPOCLI'," 													+Chr(13)+Chr(10)+""
  cSQL +=" Z59_CLIENT+'-'+Z59_LOJA+'-'+A1_NOME 'CLIENTE',"+Chr(13)+Chr(10)+""
	cSQL +=" Z59_VEND1+'-'+A3_NOME 'VENDEDOR',"  						+Chr(13)+Chr(10)+""
  cSQL +=" Z59_COMIS1 'COMISSAO',"												+Chr(13)+Chr(10)+""
	cSQL +=" Z59_DATA 'EMISSAO',"														+Chr(13)+Chr(10)+""
	cSQL +=" Z59_VCTO 'VENCIMENTO',"												+Chr(13)+Chr(10)+""
	cSQL +=" Z59_MOEDA 'MOEDA',"														+Chr(13)+Chr(10)+""
  cSQL +=" Z59_PTAX 'TAXA',"															+Chr(13)+Chr(10)+""
	cSQL +=" Z59_ITEM 'ITEM',"															+Chr(13)+Chr(10)+""
	cSQL +=" Z59_PRODUT 'PRODUTO',"													+Chr(13)+Chr(10)+""
	cSQL +=" Z59_DESCRI 'DESCRICAO',     "									+Chr(13)+Chr(10)+""
  cSQL +=" Z59_CULTUR+'-'+Z24_DESC 'CULTURA',"						+Chr(13)+Chr(10)+""
	cSQL +=" Z59_EMBAL 'EMBALAGEM',"												+Chr(13)+Chr(10)+""
	cSQL +=" Z59_QUANT 'QUANT',"														+Chr(13)+Chr(10)+""
  cSQL +=" Z59_PRCVEN 'PRCVEN',"													+Chr(13)+Chr(10)+""
	cSQL +=" Z59_TOTAL 'TOTAL',"														+Chr(13)+Chr(10)+""
	cSQL +=" Z59_OBS 'OBS',"																+Chr(13)+Chr(10)+""
	cSQL +=" Z59_FRETE 'FRETE',"														+Chr(13)+Chr(10)+""
  cSQL +=" Z59_LOCAL 'LOCAL',"														+Chr(13)+Chr(10)+""
	cSQL +=" Z59_PED 'PED',"																+Chr(13)+Chr(10)+""
	cSQL +=" Z59_DESC 'DESC',"															+Chr(13)+Chr(10)+""
	cSQL +=" Z59_CONDPA 'CONDPA',"													+Chr(13)+Chr(10)+""
  cSQL +=" SC6.C6_QTDENT 'ENTREGUE',"											+Chr(13)+Chr(10)+""
	cSQL +=" (SC6.C6_QTDVEN - SC6.C6_QTDENT) 'SALDO',"			+Chr(13)+Chr(10)+""
  cSQL +=" Z59_OBSOC 'OBSOC',"														+Chr(13)+Chr(10)+""
	cSQL +=" Z59_SAFRA 'SAFRA'"															+Chr(13)+Chr(10)+""
  cSQL +=" FROM "+RetSqlName('Z59')+" Z59 " 							+Chr(13)+Chr(10)+""
 
  cSQL +=" INNER JOIN "+RetSqlName('SA1')+" SA1 ON Z59_CLIENT = A1_COD " +Chr(13)+Chr(10)+""
  cSQL +=" AND Z59_LOJA = A1_LOJA "+Chr(13)+Chr(10)+""
  cSQL +=" AND SA1.D_E_L_E_T_='' "+Chr(13)+Chr(10)+""

  cSQL +=" LEFT JOIN "+RetSqlName('SA3')+" SA3 ON Z59_VEND1 = A3_COD "+Chr(13)+Chr(10)+""
  cSQL +=" AND SA3.D_E_L_E_T_='' "+Chr(13)+Chr(10)+""

  cSQL +=" LEFT JOIN "+RetSqlName('Z24')+" Z24 ON Z59_CULTUR = Z24_CODIGO "+Chr(13)+Chr(10)+""
  cSQL +=" AND Z24_ATIVO=1 "+Chr(13)+Chr(10)+""
  cSQL +=" AND Z24.D_E_L_E_T_=''"+Chr(13)+Chr(10)+""

  cSQL +=" LEFT JOIN "+RetSqlName('SC6')+" SC6 ON Z59_PED = C6_NUM " +Chr(13)+Chr(10)+""
  cSQL +=" AND Z59_CLIENT = C6_CLI" +Chr(13)+Chr(10)+""
  cSQL +=" AND Z59_LOJA	= C6_LOJA" +Chr(13)+Chr(10)+""
  cSQL +=" AND Z59_PRODUT = C6_PRODUTO"+Chr(13)+Chr(10)+""
  cSQL +=" AND SC6.D_E_L_E_T_=''" +Chr(13)+Chr(10)+""

  cSQL +=" WHERE Z59.D_E_L_E_T_='' "+Chr(13)+Chr(10)+""

	MemoWrite('\temp\contract_getall.txt',cSQL)

	cSQL+= ChangeQuery(cSQL) 
	If   Select('cAlias') > 0
		   DbSelectArea('cAlias')
			 cAlias->(DbCloseArea())
	EndIf
	
	TcQuery cSQL New Alias 'cAlias'
	DbSelectArea('cAlias')
	cAlias->(DbGoTop())
	If  Empty(cAlias->CONTRATO)
		  ConOut()
      ConOut('Consulta Vazia')
      ConOut()			
	Else
		  ConOut()
      ConOut('Consulta realizada com sucesso!')
      ConOut()		
	EndIf	
		
Return 'cAlias'

Method GVolume() Class Contract
 Local nTotal			:= 0
 Local cSQL				:= ""

 cSQL:=" SELECT SUM(Z59_QUANT) TOTAL FROM "+RetSqlName('Z59') +" "				+Chr(13)+Chr(10)+""
 cSQL+=" Z59 WHERE Z59_CONTRA='"+Self:Contrato+"' AND Z59.D_E_L_E_T_=''"	+Chr(13)+Chr(10)+""

 MemoWrite('\temp\total'+AllTrim(Self:Contrato)+'.txt',cSQL)

 If  Select('cAlias') > 0
	   DbSelectArea('cAlias') > 0
		 cAlias->(DbCloseArea())
 EndIf

 TcQuery cSQL New Alias 'cAlias'
 DbSelectArea('cAlias')
 nTotal	:= Iif(ValType(cAlias->TOTAL)== 'C',Val(cAlias->TOTAL),cAlias->TOTAL)
 cAlias->(DbCloseArea()) 

Return nTotal

Method GetSaldo(cItem) Class Contract
 Local cSQL			:= ""
 Local aReturn	:= {}
 
 cSQL +=" SELECT "+Chr(13)+Chr(10)+""
 cSQL +=" Z59_FILIAL 'FILIAL',Z59_NUM 'CONTRATO',Z59_PED 'PED',"						+Chr(13)+Chr(10)+""
 cSQL +=" Z59_ITEM 'ITEM',Z59_PRODUT 'PRODUTO',Z59_DESCRI 'DESCRICAO',"			+Chr(13)+Chr(10)+""
 cSQL +=" SC6.C6_QTDENT 'ENTREGUE',(SC6.C6_QTDVEN - SC6.C6_QTDENT) 'SALDO' "+Chr(13)+Chr(10)+""
 cSQL +=" FROM "+RetSqlName('Z59')+" Z59 " 																	+Chr(13)+Chr(10)+""
 
 cSQL +=" INNER JOIN "+RetSqlName('SA1')+" SA1 ON Z59_CLIENT = A1_COD " 		+Chr(13)+Chr(10)+""
 cSQL +=" AND Z59_LOJA = A1_LOJA "																					+Chr(13)+Chr(10)+""
 cSQL +=" AND SA1.D_E_L_E_T_='' "																						+Chr(13)+Chr(10)+""

 cSQL +=" LEFT JOIN "+RetSqlName('SA3')+" SA3 ON Z59_VEND1 = A3_COD "				+Chr(13)+Chr(10)+""
 cSQL +=" AND SA3.D_E_L_E_T_='' "																						+Chr(13)+Chr(10)+""

 cSQL +=" LEFT JOIN "+RetSqlName('Z24')+" Z24 ON Z59_CULTUR = Z24_CODIGO "	+Chr(13)+Chr(10)+""
 cSQL +=" AND Z24_ATIVO=1 "																									+Chr(13)+Chr(10)+""
 cSQL +=" AND Z24.D_E_L_E_T_=''"																						+Chr(13)+Chr(10)+""

 cSQL +=" LEFT JOIN "+RetSqlName('SC6')+" SC6 ON Z59_PED = C6_NUM " 				+Chr(13)+Chr(10)+""
 cSQL +=" AND Z59_CLIENT = C6_CLI" 																					+Chr(13)+Chr(10)+""
 cSQL +=" AND Z59_LOJA	= C6_LOJA" 																					+Chr(13)+Chr(10)+""
 cSQL +=" AND Z59_PRODUT = C6_PRODUTO"																			+Chr(13)+Chr(10)+""
 cSQL +=" AND SC6.D_E_L_E_T_=''" 																						+Chr(13)+Chr(10)+""

 cSQL +=" WHERE Z59.D_E_L_E_T_='' "																					+Chr(13)+Chr(10)+""
 cSQL +=" AND Z59_NUM ='"+AllTrim(Self:Contrato)+"' "												+Chr(13)+Chr(10)+""
 cSQL +=" AND Z59_ITEM='"+AllTrim(cItem)+"' "																+Chr(13)+Chr(10)+""

 MemoWrite('\temp\contract_getSaldo.txt',cSQL)
 cSQL+= ChangeQuery(cSQL) 

 If	 Select('cAlias') > 0
		 DbSelectArea('cAlias')
		 cAlias->(DbCloseArea())
 EndIf

 TcQuery cSQL New Alias 'cAlias'
 DbSelectArea('cAlias')
 If  Empty(cAlias->PED)
	 	 aReturn	:={.F.,(cAlias->SALDO),(cAlias->ENTREGUE)}
 Else
	 	 aReturn	:={.T.,(cAlias->SALDO),(cAlias->ENTREGUE)}
 EndIf
 cAlias->(DbCloseArea())
Return aReturn
