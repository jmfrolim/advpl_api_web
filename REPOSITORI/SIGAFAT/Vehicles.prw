#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'


Class Vehicles
  Data EmpFil
  Data Placa
  Data UF
  Data Status
  Data Modelo
  Data TipoVei

  Method New() Constructor
  Method GetCod()
  Method Insert()
  Method Alter(aDados)
  Method Delete(cEmpFil,cCodigo)
  Method IsExist(cFili,cCodigo)
  Method GetAll(aPar)

EndClass

Method New() Class Vehicles 
Return Self

Method GetCod() Class Vehicles 
Return Self:Placa

//-------------------------------------------------------------------
/*/{Protheus.doc} function
  description
  @author  Joao Manoel 
  @since   23/09/18
  @version 1.0
  @build   12.1.17
  @param aDados -> Array   
    [1] Placa
    [2] UF
    [3] Status -> 1 Sim(Nao Bloqueado); 2 Nao (Bloqueado)
    [4] Modelo
    [5] Filial
    [6] Tipo -> 1 Proprio; 2 Terceiro; 3 Agregado
/*/
//-------------------------------------------------------------------
Method Insert() Class Vehicles
  Local lReturn   := .F.
  Local lExist    := Self:IsExist(FwxFilial('DA3'),Self:Placa)

  If  lExist

      lReturn   := .F.

  Else

      Begin Transaction

        RecLock('DA3',.T.)
         DA3->DA3_FILIAL   := FwxFilial('DA3')
         DA3->DA3_COD      := Self:Placa
         DA3->DA3_DESC     := Self:Modelo
         DA3->DA3_PLACA    := Self:Placa
         DA3->DA3_ESTPLA   := Self:UF
         DA3->DA3_ATIVO    := Self:Status
         DA3->DA3_FROVEI   := Self:TipoVei
        DA3->(MsUnLock())        
   
        If Self:IsExist(FwxFilial('DA3'),Self:Placa)
           lReturn := .T.
        Else
           lReturn := .F.
        EndIf

      End Transaction      
  EndIf
  
Return lReturn

Method Alter(aDados) Class Vehicles 
  Local lReturn

Return lReturn

Method Delete(cEmpFil,cCodigo) Class Vehicles 
  Local lReturn

Return lReturn

Method IsExist(cEmpFil,cCodigo) Class Vehicles
 Local  lReturn
 Local  cIndex   := cEmpFil+cCodigo //1 ->DA3_FILIAL+DA3_COD

 DbSelectArea('DA3')
 DA3->(DbSetOrder(1))
 DA3->(DbSeek(cIndex))
 If  Found()     
     Self:EmpFil
     Self:Placa
     Self:UF
     Self:Status
     Self:Modelo
     lReturn    := .T.

 Else

     lReturn    := .F.
   
 EndIf
 DA3->(DbCloseArea())

Return lReturn

Method GetAll(aPar) Class Vehicles
 Local cSQL   := ""
 Local cWhere := ""

  cSQL+=" SELECT " + Chr(13) + Chr(10)+" "
  cSQL+=" DA3_FILIAL,DA3_COD,DA3_DESC,DA3_PLACA 'PLACA',DA3_MUNPLA,DA3_ESTPLA," + Chr(13) + Chr(10)+" "
  cSQL+=" DA3_CAPACN,DA3_CAPACM,DA3_VOLMAX 'VOLMAX',DA3_MOTORI,DA3_SITUAC,DA3_PLACA2," + Chr(13) + Chr(10)+" "
  cSQL+=" DA3_PLACA3,DA3_RNTC,DA3_PLACA2,DA3_PLACA3,DA3_RNTC,DA3_FILATU " + Chr(13) + Chr(10)+" "
  cSQL+=" FROM "+RetSqlName('DA3')+ " DA3 " + Chr(13) + Chr(10)+" "
  cSQL+=" WHERE D3.D_E_L_E_T_='' "+ Chr(13) + Chr(10)+" "

  MemoWrite('c:\temp\vehicles_getall.txt',cSQL)
  
  If  Select('cAlias') > 0
      DbSelectArea('cAlias')
      cAlias->(DbCloseArea())
  EndIf
  
  TcQuery cSQL new Alias cAlias
  DbSelectArea('cAlias')
  If  Empty(cAlias->PLACA)
      ConOut()
      ConOut('Consulta Vazia')
      ConOut()

  Else
      ConOut()
      ConOut('Consulta realizada com sucesso1')
      ConOut()        
  EndIf 

Return 'cAlias'
