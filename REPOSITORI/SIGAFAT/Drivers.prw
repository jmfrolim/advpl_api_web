#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'


Class Drivers
  Data Codigo
  Data CPF
  Data CNH
  Data Nome
  Data Status
  Data EmpFil
  Data Tipo
  Data UF
  Data Fone

  Method New() Constructor
  Method GetCod()
  Method Insert(aDados)
  Method Alter(aDados)
  Method Delete(cEmpFil,cCodigo)
  Method IsExist(cFili,cCodigo)
  Method SetBlock(cEmpFil,cCodigo)
  Method SetFree(cEmpFil,cCodigo)
  Method GetStatus(cEmpFil,cCodigo)

EndClass

Method New() Class Drivers
  
Return Self

Method GetCod() Class Drivers 
Return Self:Codigo  := U_ZUF05116()
//-------------------------------------------------------------------
/*/{Protheus.doc} function
description Method Insert
@author  Joao Manoel Freitas Rolim
@since   23/03/2018
@version 1.0
@build 12.1.17
@param aDados
  [1] Codigo
  [2] CPF
  [3] Nome
  [4] Status -> 1 Sim(Bloqueado); 2 N?o (N?o Bloqueado)
  [5] Filial
  [6] Tipo -> 1 Proprio; 2 Terceiro; 3 Agregado
  [7] Estado -> ex SP; RJ; MA; PI
  [8] Fone
  [9] CNH
@return lReturn -> Logico podendo ser .T.ou .F.
/*/
//-------------------------------------------------------------------
Method Insert(aDados) Class Drivers
  Local lReturn
  Local lExist    := Self:IsExist(FwxFilial('DA4'),Self:Codigo)

  If lExist
     lReturn    := .F.
  Else
     RecLock('DA4',.T.)
      DA4->DA4_FILIAL      := ""
      DA4->DA4_COD         := Self:Codigo
      DA4->DA4_NOME        := aDados[3]
      DA4->DA4_TIPMOT      := aDados[6]
      DA4->DA4_NREDUZ      := SubStr(aDados[3],1,15)
      DA4->DA4_EST         := aDados[7]
      DA4->DA4_CGC         := aDados[2]
      DA4->DA4_TEL         := aDados[8]
      DA4->DA4_NUMCNH      := aDados[9] 
     DA4->(MsUnLock())

     If Self:IsExist(FwxFilial('DA4'),aDados[1])
        lReturn := .T.
     Else
        lReturn := .F.
     EndIf
  
  EndIf
  
Return lReturn
//-------------------------------------------------------------------
/*/{Protheus.doc} Method
description Method Alter
@author  Joao Manoel Freitas Rolim
@since   23/03/2018
@version 1.0
@build 12.1.17
@param aDados
  [1] Codigo
  [2] CPF
  [3] Nome
  [4] Status -> 1 Sim(Bloqueado); 2 N?o (N?o Bloqueado)
  [5] Filial
  [6] Tipo -> 1 Proprio; 2 Terceiro; 3 Agregado
  [7] Estado -> ex SP; RJ; MA; PI
  [8] Fone
@return lReturn -> Logico podendo ser .T.ou .F.
/*/
//-------------------------------------------------------------------
Method Alter(aDados) Class Drivers
  Local lReturn 
  Local cIndex    := aDados[5]+aDados[1]
  Local lExist    := Self:IsExist(aDados[5],aDados[1])

  If lExist

     Begin Transaction
      RecLock('DA4',.F.)
       DA4->DA4_NOME        := aDados[3]
       DA4->DA4_TIPMOT      := aDados[6]
       DA4->DA4_NREDUZ      := SubStr(aDados[1],1,20)
       DA4->DA4_EST         := aDados[7]
       DA4->DA4_CGC         := aDados[2]
       DA4->DA4_TEL         := aDados[8] 
      DA4->(MsUnLock())
      End   Transaction
      lReturn   := .T.     

  Else
     lReturn    := .F.    
  EndIf

Return lReturn

Method Delete(cEmpFil,cCodigo) Class Drivers
  Local lReturn
  Local cIndex    := cEmpFil+cCodigo

  DbSelectArea('DA4')
  DA4->(DbSetOrder(1))
  DA4->(DbSeek(cIndex))
  If Found()
     Begin Transaction
      RecLock('DA4',.F.)
       DA4->(DbDelete())
      DA4->(MsUnLock())

      If Self:IsExist(cEmpFil,cCodigo)
         DisarmTransaction()
      Else
         lReturn  := .T.        
      EndIf
     End   Transaction
     
  Else
    
    lReturn := .F.
    
  EndIf
  DA4->(DbCloseArea())
  
Return lReturn

Method IsExist(cEmpFil,cCodigo) Class Drivers
  Local lReturn
  Local cIndex  := cEmpFil+cCodigo 

  DbSelectArea('DA4')
  DA4->(DbSetOrder(1))//DA4_FILIAL+DA4_COD
  DA4->(DbSeek(cIndex))
  If Found()
     Self:Codigo
     Self:CPF
     Self:Nome
     Self:Status
     Self:EmpFil
     Self:Tipo
     Self:UF
     Self:Fone
     Self:CNH
     lReturn  := .T.
  Else
     lReturn  := .F.
    
  EndIf
  DA4->(DbCloseArea())
  
Return lReturn
