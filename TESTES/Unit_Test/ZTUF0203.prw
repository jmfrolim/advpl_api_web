#Include 'Totvs.ch'
#Include 'TbiConn.ch'

/*/{Protheus.doc} ZTUF0203
  (long_description)
  @type  User Function
  @author user
  @since 24/09/18
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
/*/

User Function ZTUF0203()
  Local oVehicles   := Nil
  Local aVehicles   := {}
  Local nOpc        := 0
  Local nX          := 1

  	PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
      oVehicles   := Vehicle():New()
      nOpc        := 1
      STARRY(aVehicles)     

      If  nOpc == 1
          For nX:=1  To Len(aVehicles)
            If  oVehicles:Find(FwxFilial('DA3'),aVehicles[nX][1])
                ConOut("________________________________________")
                ConOut("")
                ConOut("   Esse Veiculo ja esta cadastrado      ")
                ConOut("")          
                ConOut("")
                ConOut("________________________________________")

            Else
                oVehicles:EmpFil  := FwxFilial('DA3')
                oVehicles:Placa   := aVehicles[nX][1]
                oVehicles:UF      := aVehicles[nX][3]
                oVehicles:Status  := '1'
                oVehicles:Modelo  := aVehicles[nX][2]
                oVehicles:TipoVei := '2'

                If  oVehicles:Insert()
                    ConOut(" ________________________________________")
                    ConOut("|                                        |")
                    ConOut("|     Veiculo: "+AllTrim(aVehicles[nX][1])+ "cadastrado com Sucesso! |")
                    ConOut("|                                        |")
                    ConOut("|________________________________________|")

                Else
                    ConOut(" ________________________________________")
                    ConOut("|                                        |")
                    ConOut("|Erro ao Cadastrar o Veiculo "+AllTrim(aVehicles[nX][1])+"! |")
                    ConOut("|                                        |")
                    ConOut("|________________________________________|")

                EndIf        

            EndIf
          Next nX++

      Else
          ConOut( "Implementar Alterar ou Excluir!")        
      EndIf  
    RESET ENVIRONMENT
Return 

/*/{Protheus.doc} STARRY
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/

Static Function STARRY(aVehicles)
  
  aVehicles := {{"MWT5492",	"SCANIA	  " , "MA"},;
                {"NXH1003",	"LS	      " , "MA"},;
                {"HPQ9051",	"LS	      " , "MA"},;
                {"GXH0905",	"TRUCK	  " , "MA"},;
                {"JKW3058",	"TRUCK	  " , "MA"},;
                {"ECT5058",  "SCANIA   "	, "MA"},;
                {"MWG3285",	"CAMCABA  " , "MA"},;
                {"HCU8787",	"    LS	  " , "PI"},;
                {"NIP0817",  "  IVECO	" , "PI"},;
                {"NID4967",  "  LS	    " , "PI"},;
                {"HBG2214",	"BITREM	  " , "CE"},;
                {"ORR2744",	"BITREM	  " , "CE"},;
                {"AVC1234",	"SACNIA   " , "PR"},;
                {"III1111",	"BI       " , "PR"},;
                {"CCC2222",	"TRUCK    " ,	"PR"}}
Return 