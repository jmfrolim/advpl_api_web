#Include 'Totvs.ch'
#Include 'TbiConn.ch'

/*/{Protheus.doc} ZTUF0201
  (long_description)
  @type  User Function
  @author Joao
  @since  23/09/18
  @version 1.0
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/

User Function ZTUF0201()
  Local oMotorista  := Nil
  Local aMotoristas := {}
  Local aDados      := {}
  Local nX

   PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
    oMotorista:= Drivers():New()
    aDados    := Array(9)

    aMotoristas := {{"", "84817528001" , "Obediah Bendley"      , "2" , FwxFilial() , "2" , "PI" , "7132830362" , "01804918409"},;
                    {"", "86973104070" , "Barnabe Maurice"      , "2" , FwxFilial() , "2" , "TO" , "2747923059" , "87398337800"},;
                    {"", "76137201015" , "Cecile Bayldon"       , "2" , FwxFilial() , "2" , "CE" , "3331114052" , "60478720669"},;
                    {"", "42059296099" , "Georgette Penhearow"  , "2" , FwxFilial() , "2" , "MA" , "9839572018" , "56029475005"},;
                    {"", "59683049010" , "Anna-diane McClounan" , "2" , FwxFilial() , "2" , "GO" , "7491866632" , "62642675141"},;
                    {"", "30280877072" , "Maegan Bangley"       , "2" , FwxFilial() , "2" , "SP" , "6196569922" , "11195165258"},;
                    {"", "76726988001" , "Forest Dever"         , "2" , FwxFilial() , "2" , "CE" , "8183991042" , "12159527813"},;
                    {"", "44729883055" , "Cosetta Clack"        , "2" , FwxFilial() , "2" , "PI" , "2902655573" , "66792064320"},;
                    {"", "39866506010" , "Harlen Ferron"        , "2" , FwxFilial() , "2" , "PA" , "4353026415" , "17440270035"},;
                    {"", "31547837098" , "Diane Bangley"        , "2" , FwxFilial() , "2" , "MT" , "3139866812" , "61040092307"};
                   }
    
    For nX:= 1 To Len(aMotoristas)    
        aDados[1]  := ""
        aDados[2]  := aMotoristas[nX][2]
        aDados[3]  := aMotoristas[nX][3]
        aDados[4]  := aMotoristas[nX][4]
        aDados[5]  := aMotoristas[nX][5]
        aDados[6]  := aMotoristas[nX][6]
        aDados[7]  := aMotoristas[nX][7]
        aDados[8]  := aMotoristas[nX][8]
        aDados[9]  := aMotoristas[nX][9]

        oMotorista:GetCod()

        If  oMotorista:Insert(aDados)

            ConOut("__________________")
            ConOut("__________________")
            ConOut("Motorista de Nome: " +aMotoristas[nX][3]+ " ")
            ConOut("__________________")

        Else

            ConOut("__________________")
            ConOut("__________________")
            ConOut("Motorista de Nome: " +aMotoristas[nX][3]+ " nao pode ser persistido! ")
            ConOut("__________________")

        EndIf
        Afill(aDados,"")
    Next nX++
    

   RESET ENVIRONMENT
Return 