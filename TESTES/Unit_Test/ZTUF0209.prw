#Include 'Totvs.ch'
#Include 'TbiConn.ch'

/*/{Protheus.doc} ZTUF0209
  (long_description)
  @type  Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/

User Function ZTUF0209()
 Local oRating      := Nil
 Local aTrans       := {}
 Local aDados       := {'000295','000294','000296'}
 Local oContrat     := Nil
 Local cLog         := ""
 Local nX           := 0
  PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
    oContrat    := Contract():New()
    For nX:= 1 To 3
        Aadd(aTrans,{aDados[nX]})
    Next nX++
    If  oContrat:IsExist(cFilAnt,'202652')
        oRating  := Rating():New()
        oRating:GetCod()
        oRating:Contrato  := oContrat:Contrato
        oRating:Emissao   := Date()
        oRating:Validade  := CtoD('05/01/2019')
        oRating:Volume    := oContrat:GVolume()
        oRating:SetItem(oRating,aTrans)
        If  Len(oRating:aItens) > 0
            If  oRating:Insert()
                cLog:="Teste Unitario de Insercao Class Rating realizado com sucesso."
                If  (GetRemoteType() == 2)                     
                     cLog +="  |SmartClient Linux|"
                     ConOut(cLog)
                     MemoWrite("\temp\ZTUF0209.txt",cLog)
                Else
                     cLog+="  |SmartClient Windows|"
                     ConOut(cLog)
                     MemoWrite("c:\temp\ZTUF0209.txt",cLog)
                EndIf                
            Else
                cLog:="Teste Unitario de Insercao Class Rating com falha."
                If  (GetRemoteType() == 2)                     
                     cLog +="  |SmartClient Linux|"
                     ConOut(cLog)
                     MemoWrite("\temp\ZTUF0209.txt",cLog)
                Else
                     cLog+="  |SmartClient Windows|"
                     ConOut(cLog)
                     MemoWrite("c:\temp\ZTUF0209.txt",cLog)
                EndIf
            EndIf
        Else
            cLog:="Teste Unitario de Metdo SetItem Class Rating falha."
            If  (GetRemoteType() == 2)                     
                 cLog +="  |SmartClient Linux|"
                 ConOut(cLog)
                 MemoWrite("\temp\ZTUF0209.txt",cLog)
            Else
                cLog+="  |SmartClient Windows|"
                ConOut(cLog)
                MemoWrite("c:\temp\ZTUF0209.txt",cLog)
            EndIf          
        EndIf        
    Else
        cLog:="Teste Unitario de Metodo IsExist Class Contract com falha."
        If  (GetRemoteType() == 2)                     
             cLog +="  |SmartClient Linux|"
             ConOut(cLog)
             MemoWrite("\temp\ZTUF0209.txt",cLog)
        Else
            cLog+="  |SmartClient Windows|"
            ConOut(cLog)
            MemoWrite("c:\temp\ZTUF0209.txt",cLog)
        EndIf      
    EndIf    
  RESET ENVIRONMENT
Return 