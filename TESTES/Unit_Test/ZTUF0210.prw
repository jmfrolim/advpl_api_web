#Include 'Totvs.ch'
#Include 'TbiConn.ch'

/*/{Protheus.doc} ZTUF0210
  (long_description)
  @type  Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
/*/
User Function ZTUF0210()
 Local oPreOrder  := Nil
 
 PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
  oPreOrder   := PreOrder():New()
  oPreOrder:Codigo := '000001'
  ConOut('PreOrder Codigo:  '+ oPreOrder:Codigo)
 RESET ENVIRONMENT
Return