#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'

/*/{Protheus.doc} ZTUF0205
  (long_description)
  @type  Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
User Function ZTUF0205()
  Local oContract       := Nil
 // Local oObj            := Nil
 // Local aContracts      := {}
  Local nOpc            := 0
  Local oProduto        := Nil
  Local cCultura        := "0001"
  Local nQuant
  

  	PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
     oProduto   := Product():New()
     nOpc       := 1
     
     oProduto:IsExist("01",	"SV0002         	","SV")
     If  nOpc == 1
         ConOut("")
         ConOut("Vamos Iniciar a Inclusao")
         Alert("Vamos Iniciar a Inclusao")
         ConOut("")
         If  Empty(oProduto)
             Conout("")
             Conout("Objeto Produto Vazio")
             Alert("Objeto Produto Vazio")
             ConOut("")
         Else
             nQuant      := 500
             Iif(Empty(oProduto:PrecoVenda),oProduto:PrecoVenda := 250, oProduto:PrecoVenda)
             oContract  := Contract():New()
             oContract:oCliente := Customer():New('','','19854889000100')
             oContract:GetCod()

             ConOut("")
             ConOut("Vamos Iniciar o Preenchimento dos Itens de Contrato")
             Alert("Vamos Iniciar o Preenchimento dos Itens de Contrato")
             ConOut("")
             ConOut("")
             ConOut("")

             If  oContract:SetItem(oProduto,cCultura,nQuant)
                 ConOut("")
                 ConOut("")
                 ConOut("Vamos Iniciar a Inclusao")
                 Alert("Vamos Iniciar a Inclusao")
                 ConOut("")
                 ConOut("")

                 Iif(oContract:Insert(),ConOut("Cadastro de Contrato realizado"),ConOut("Erro ao cadastrar contrato"))

             Else
                 Conout("Erro ao preencher os Itens")
                 Alert("Erro ao preencher os Itens")
                 ConOut("")
                 ConOut("")
             EndIf
         
         EndIf         
     Else 
         ConOut("")
         ConOut("")        
         ConOut("Falta Impementar o Alterar e Excluir")
         Alert("Falta Impementar o Alterar e Excluir")
         ConOut("")
         ConOut("")

     EndIf      
    RESET ENVIRONMENT
Return 