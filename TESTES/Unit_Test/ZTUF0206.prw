#Include 'Protheus.ch'
#Include 'TbiConn.ch'
/*/{Protheus.doc} ZTUF0206
  (long_description)
  @type  Function
  @author user
  @since 05/10/18
  @version 1.0
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @description Teste Unitario da Model Transportes
  @see (links_or_references)
  /*/
User Function ZTUF0206()
 Local oTransport       := Nil
 Local aTransporters    := {}
 Local nX               := 0
 Local cEmpty           := ""
 Local nOpc             := 0

 	PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
     DbSelectArea('SA4')
     nOpc   := 5
    aTransporters  :={{'02931303000153','Thiel-Langworth','MT'   },;
                     {'22091765000117','Price Group','MA'       },;
                     {'46402160000168','Howe Inc','MT'          },;
                     {'88067723000159','Hand-Leannon','MA'      }}
    
    oTransport    := Transporter():New()

    If  nOpc == 3
        For nX:= 1 To Len(aTransporters)
            If  oTransport:IsExist(FwxFilial('SA4'),aTransporters[nX][1])
                ConOut( "")
                ConOut( "")
                ConOut( "")
                ConOut("Transportadora ja Existe")
                ConOut( "")
                ConOut( "")
                ConOut( "")            
            Else
                oTransport:Filial       := FwxFilial('SA4')
                oTransport:Codigo       := oTransport:GetCod()
                oTransport:CGC          := aTransporters[nX][1]
                oTransport:Nome         := aTransporters[nX][2]
                oTransport:InsEstadual  := cEmpty
                oTransport:UF           := aTransporters[nX][3]

                If  oTransport:Insert()
                    ConOut( "")
                    ConOut( "")
                    ConOut( "")
                    ConOut("Transportadora Cadastrada com sucesso")
                    ConOut( "")
                    ConOut( "")
                    ConOut( "")              
                Else
                    ConOut( "")
                    ConOut( "")
                    ConOut( "")
                    ConOut("Nao foi possivel cadastrar transportadora")
                    ConOut( "")
                    ConOut( "")
                    ConOut( "")                 
                EndIf          
            EndIf     
        Next nX++        
    ElseIf (nOpc == 4)
        If  (oTransport:IsExist(FwxFilial('SA4'),aTransporters[4][1]))
             oTransport:Nome := 'Hand Leannon Alterado2' 
            If  oTransport:Alter(FwxFilial(''),oTransport:Codigo)
                ConOut( "")
                ConOut( "")
                ConOut( "")
                ConOut("Transportadora Alterada com sucesso")
                ConOut( "")
                ConOut( "")
                ConOut( "")              
            Else
                ConOut( "")
                ConOut( "")
                ConOut( "")
                ConOut("Nao foi possivel cadastrar transportadora")
                ConOut( "")
                ConOut( "")
                ConOut( "")                 
            EndIf    
        Else
            ConOut( "")
            ConOut( "")
            ConOut( "")
            ConOut("Transportadora nao Existe")
            ConOut( "")
            ConOut( "")
            ConOut( "") 
        EndIf
    Else
         If  (oTransport:IsExist(FwxFilial('SA4'),aTransporters[2][1]))
             
            If  oTransport:Delete(FwxFilial(''),oTransport:Codigo)
                ConOut( "")
                ConOut( "")
                ConOut( "")
                ConOut("Transportadora Excluida com sucesso")
                ConOut( "")
                ConOut( "")
                ConOut( "")              
            Else
                ConOut( "")
                ConOut( "")
                ConOut( "")
                ConOut("Nao foi possivel Excluir a transportadora")
                ConOut( "")
                ConOut( "")
                ConOut( "")                 
            EndIf    
        Else
            ConOut( "")
            ConOut( "")
            ConOut( "")
            ConOut("Transportadora nao Existe")
            ConOut( "")
            ConOut( "")
            ConOut( "") 
        EndIf        
    EndIf
   SA4->(DbCloseArea())            
  RESET ENVIRONMENT
Return 