
#Include 'Totvs.ch'
#Include 'TbiConn.ch'

/*/{Protheus.doc} ZTUF0203
  (long_description)
  @type  User Function
  @author user
  @since 24/09/18
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
/*/

User Function ZTUF0204()
  Local oCustomer     := Nil
  Local oObj          := Nil
  Local aCustomers    := {}
  Local nOpc          := 0
  

  	PREPARE ENVIRONMENT EMPRESA '99' FILIAL "01" MODULO "FAT" TABLES "SA1"
      nOpc        := 3
      aCustomers  := {'19854889000100','Price LLC','F','J','Hermina Center','2','MT', '03403',  'CUIABA'}
      
      oCustomer   := Customer():New("" ,"" ,aCustomers[1])

      If  nOpc == 1
         // For nX:=1  To Len(aCustomers)

            If  oCustomer:IsExist(FwxFilial('SA1'),aCustomers[1])

                ConOut("________________________________________")
                ConOut("")
                ConOut("   Esse Cliente ja esta cadastrado     !")
                ConOut("")          
                ConOut("")
                ConOut("________________________________________")

            Else             
                
                oCustomer:Codigo  	    
                oCustomer:Loja  	      
                oCustomer:Nome  	      := aCustomers[2]
                oCustomer:NomeRed       := SubStr(aCustomers[2],1,20)
                oCustomer:TipoVenda     := aCustomers[3]
                oCustomer:Endereco	    := aCustomers[5]
                oCustomer:Cidade	      := aCustomers[8] + aCustomers[9]
                oCustomer:UF            := aCustomers[7]
                oCustomer:Status        := aCustomers[6]
                oCustomer:TipoCivil     := aCustomers[4]
                oCustomer:Natureza      := '101'
                oCustomer:Pais          := '105'
                oCustomer:CGC           := aCustomers[1]
                oCustomer:ContaContabil :='1120010010001

                If  oCustomer:Insert()

                    ConOut("  __________________________________________________________________________________________________________________________________")
                    ConOut("|                                                                                                                                   |")
                    ConOut("|     Cliente CNPJ/CPF: "+AllTrim(CvalToChar(aCustomers[1]))+" Nome: "+AllTrim(CvalToChar(aCustomers[2]))+" cadastrado com Sucesso! |")
                    ConOut("|                                                                                                                                   |")
                    ConOut("| __________________________________________________________________________________________________________________________________|")

                Else

                    ConOut(" ____________________________________________________________________________________________________________________________")
                    ConOut("|                                                                                                                            |")
                    ConOut("|Erro ao Cadastrar o Cliente CNPJ/CPF: "+AllTrim(CvalToChar(aCustomers[1]))+" Nome: "+AllTrim(CvalToChar(aCustomers[2]))+" ! |")
                    ConOut("|                                                                                                                            |")
                    ConOut("|____________________________________________________________________________________________________________________________|")

                EndIf        

            EndIf

         // Next nX++

      
      ElseIf nOpc == 3
          
          Iif (oCustomer:IsExist(FwxFilial('SA1'),aCustomers[1]),ConOut('Ok'),ConOut('Falha'))
          oObj  := oCustomer

      Else 

          ConOut( "Implementar Alterar ou Excluir!")
        
      EndIf  
    RESET ENVIRONMENT
Return 
/*
/{Protheus.doc} STARRY
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /

Static Function STARRY(aCustomers)
  
  aCustomers := {;
                 {'68732510000197','Thompson Inc  ', 'L' , 'J' , '313 Aberg Center', '2', 'MA', '11300','SAO LUIS'},;
                 {'17418467000159','Hessel-Braun'  , 'F' , 'J' , '90664 6th Alley ', '2', 'MT', '03403','CUIABA'},;
                 {'32361840000176','O'Keefe Inc'   ,'L','J'   ,'377 Westend Parkway'  ,'2', 'MA', '11300',  'SAO LUIS'},;
                 {'19854889000100','Price LLC','F','J','Hermina Center','2','MT', '03403',  'CUIABA'},
                }
Return 

*/