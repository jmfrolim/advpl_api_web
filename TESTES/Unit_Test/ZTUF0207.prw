#Include 'Protheus.ch'
#Include 'TbiConn.ch'
#Include 'RestFul.ch'
/*/{Protheus.doc} ZTUF0206
  (long_description)
  @type  Function
  @author user
  @since 06/10/18
  @version 1.0
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @description Teste Unitario da API_PRODUCT
  @see (links_or_references)
/*/
User Function ZTUF0207()
 Local cUrl         :="http://localhost:9090/rest/APIPRODUTO/produto/"
 Local cJson        := ""
 Local cTestJson    := ""
 Local oProduct     := Nil
 Local oObj         := Nil
 Local nOpc         := 0
 Local cAlias       := ""
 /*Local xPostUrl     := ""
 Local nTimeOut     := 60
 Local cHeaderGet   := ""
 Local cProduto     := "" */

  PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
   InsertJson(@cJson)
   oProduct := Product():New()
   oObj     := Product():New()
   nOpc     := 1
   
   If nOpc == 3
        cTestJson:= FwJsonDeserialize(cJson , @oProduct)
        oObj:GetCod(FwxFilial('SB1'),"SV")
        oProduct:Codigo := oObj:Codigo 
        oObj:GetClone(oProduct)  
        
        oObj:GetCod(FwxFilial('SB1'),"SV")
            If  oObj:Insert()
                //lReturn   := .T.
                cJson  := '{"Codigo":"'+oObj:Codigo+'","Tipo":"'+oObj:Tipo+'","msg":"Sucesso"}'
                ConOut( "")
                ConOut(cJson)      
                ConOut( "")
            Else
                ConOut("Nao Foi possivel cadastrar o produto")
            EndIf
        /*   
        xPostUrl := HttpPost( cUrl, , cJson,  nTimeOut,  aHeadStr,  @cHeaderGet )
        If !Empty(xPostUrl)
            ConOut("HttpPost Ok")
            VarInfo("WebPage", xPostUrl)
        Else
            ConOut("HttpPost Failed.")
            VarInfo("Header", cHeadRet)
        Endif */
   Else
        cAlias:= oProduct:GetAll()
        If  Empty(cAlias)
         ConOut('Retorno vazio nao existem produtos cadastrados')
        Else
         While (!Eof(cAlias))
                oProduct:Codigo     := cAlias->CODIGO
                oProduct:Descricao  := cAlias->DESCRICAO
                oProduct:Tipo       := cAlias->TIPO
                oProduct:PrecoVenda := Iif(ValType(cAlias->PRECOVENDA) == 'C',CvalToChar(cAlias->PRECOVENDA),cAlias->PRECOVENDA)
                oProduct:Armazem    := cAlias->ARMAZEM
                
            oObj:GetRange(oProduct)
            cAlias->(DbSkip())
         EndDo
        
        cJson:= FwJsonSerialize(oObj:Collection)
        EndIf
        ConOut( "")
        ConOut( "")
        ConOut( "")
        ConOut(cJson)
        ConOut( "")
        ConOut( "")
   EndIf
  RESET ENVIRONMENT
Return

/*/{Protheus.doc} InsertJson
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function InsertJson(cJson)
 cJson :='{"Filial":"","Codigo":"","Descricao":"Desenvolvimento de Software","Tipo":"SV","Ncm":"","Unidade":"UN","Armazem":"0001","PercICM":"0","PercIPI":"0","CodBar":"","PrecoVenda":"0","MRP":"N","ContParceria":"N","ContSegSocia":"N","TIPOCQ":"M","IRRF":"N","CtoEndereco":"N"}'
 //cJson :='{"PRODUCT":{  "Filial":"","Codigo":"","Descricao":"Desenvolvimento de Software","Tipo":"SV","Ncm":"","Unidade":"UN","Armazem":"0001","PercICM":"0","PercIPI":"0","CodBar":"","PrecoVenda":"0","MRP":"N","ContParceria":"N","ContSegSocia":"N","TIPOCQ":"N","IRRF":"N","CtoEndereco":"N"}}'

Return 