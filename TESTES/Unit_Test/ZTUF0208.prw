#Include 'Totvs.ch'
#Include 'TopConn.ch'
#Include 'TbiConn.ch'
#Include 'RestFul.ch'
/*/{Protheus.doc} ZTUF0208
  (long_description)
  @type  Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/

User Function ZTUF0208()
 Local oContract  := Contract():New()
 Local oObj       := Contract():New()
 Local oCollec    := Collect():New()
 Local cJson      := ""
 Local cAlias     := ""
 Local nWhile     := 0

  PREPARE ENVIRONMENT EMPRESA '02' FILIAL '01' MODULO 'COM'
    cAlias:= oContract:GetAll()
    If !Empty(cAlias)
        DbSelectArea('cAlias')
        While cAlias->(!Eof())
            oObj       := Contract():New()
            oObj:Filial        := cAlias->FILIAL
            oObj:Pedido        := cAlias->PED
            oObj:Status        := cAlias->STATUS
            oObj:Contrato      := cAlias->CONTRATO
            oObj:TipoCliente   := cAlias->TIPOCLI
            oObj:oCliente      := AllTrim(cAlias->CLIENTE)
            oObj:aVendedores   := cAlias->VENDEDOR
            //oObj:TipoFrete     := cAlias->TIPOFRETE
            oObj:Vencimento    := cAlias->VENCIMENTO
            oObj:Emissao       := cAlias->EMISSAO
            oObj:Moeda         := cAlias->MOEDA
            oObj:PTAX          := cAlias->TAXA
            oObj:ValUnit       := cAlias->PRCVEN
            Do While (cAlias->CONTRATO == oObj:Contrato)
               oObj:aItens        := oObj:SetItem(oObj,cAlias->ITEM,cAlias->CULTURA,cAlias->QUANT,cAlias->SALDO,cAlias->ENTREGUE) 
               cAlias->(DbSkip())               
            EndDo           
            oCollec:SetAdd(oObj)
            oObj:=Nil
            nWhile++
            cAlias->(DbSkip())
        EndDo
        cJson:= FwJsonSerialize(oCollec)
        ConOut(cJson)
        MemoWrite('c:\temp\teste_contract.json',cJson)
        MemoWrite('c:\temp\numero_itens.txt',Str(nWhile))
    Else 
        cJson:= "Erro na seleção do Alias de Dados."
        MemoWrite('c:\temp\ZTUF0208.txt',cJson)     
    EndIf
    
  RESET ENVIRONMENT
Return 