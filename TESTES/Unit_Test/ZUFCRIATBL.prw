#Include 'Protheus.ch'
//--------------------------------------------------------------
/*/{Protheus.doc} ZUFCRIATBL
Description

@param xParam Parameter Description                             
@return xRet Return Description                                 
@author Joao Manoel Freitas Rolim - contato@joaomanoel.net.br                                              
@since 7/12/2018                                                   
/*/
//--------------------------------------------------------------
User Function ZUFCRIATBL()                        
  Local obntfim
  Local obntsc2
  Local obntsc5
  Local obntsc6
  Local obntsc7
  Local obntsc9
  Local obntsd4
  Local obntse4
  Local obntsed
  Local obntsf4
  Local obntza0
  Local obntza4
  Local obntza7
  Local obntza8
  Local obntzc3
  Local obntzc4
  Local oFont1 := TFont():New("Lucida MAC",,019,,.F.,,,,,.F.,.F.)
  Local osayname
  Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Criar Estrutra de Tabelas" FROM 000, 000  TO 300, 400 COLORS 0, 16777215 PIXEL

    @ 009, 040 SAY osayname PROMPT "Escolha qual  estrutura de tabela  deseja criar." SIZE 127, 027 OF oDlg FONT oFont1 COLORS 0, 16777215 PIXEL
    @ 045, 047 BUTTON obntza0 PROMPT "ZA0" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZA0())
    @ 065, 048 BUTTON obntza7 PROMPT "ZA7" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZA7())
    @ 085, 047 BUTTON obntza8 PROMPT "ZA8" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZA8())
    @ 105, 047 BUTTON obntsed PROMPT "SED" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSED())
    @ 045, 100 BUTTON obntse4 PROMPT "SE4" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSE4())
    @ 066, 099 BUTTON obntsf4 PROMPT "SF4" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSF4())
    @ 087, 099 BUTTON obntzc3 PROMPT "ZC3" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZC3())
    @ 105, 099 BUTTON obntzc4 PROMPT "ZC4" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZC4())
    @ 122, 046 BUTTON obntsc7 PROMPT "SC7" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSC7())
    @ 123, 098 BUTTON obntza4 PROMPT "ZA4" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZA4())
    @ 045, 149 BUTTON obntsc5 PROMPT "SC5" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSC5())
    @ 066, 148 BUTTON obntsc6 PROMPT "SC6" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSC6())
    @ 086, 148 BUTTON obntsc2 PROMPT "SC2" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSC2())
    @ 106, 147 BUTTON obntsd4 PROMPT "SD4" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSD4())
    @ 124, 148 BUTTON obntsc9 PROMPT "SC9" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSC9())
    @ 065, 003 BUTTON obntsa3 PROMPT "SA3" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSA3())
    @ 085, 002 BUTTON obntsa4 PROMPT "SA4" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetSA4())
    @ 104, 003 BUTTON obntda3 PROMPT "DA3" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetDA3())
    @ 123, 004 BUTTON obntda4 PROMPT "ZAI" SIZE 037, 012 OF oDlg  PIXEL   ACTION(SetZAI())
    @ 045, 002 BUTTON obntfim PROMPT "FIM" SIZE 037, 012 OF oDlg  PIXEL   ACTION(oDlg:End())

  ACTIVATE MSDIALOG oDlg CENTERED

Return

/*/{Protheus.doc} SetZA0
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetZA0()
 Local cTitulo      := "Tabela ZA0"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("ZA0",cTitulo,cDel,cOk)
Return 


/*/{Protheus.doc} SetZA7
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetZA7()
  Local cTitulo      := "Tabela ZA7"
  Local cDel         := ".T."
  Local cOk          := ".T."
  AxCadastro("ZA7",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetZA8
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetZA8()
 Local cTitulo      := "Tabela ZA8"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("ZA8",cTitulo,cDel,cOk)
Return 

/*/{Protheus.doc} SetZA4
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetZA4()
 Local cTitulo      := "Tabela ZA4"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("ZA4",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetZC4
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetZC4()
 Local cTitulo      := "Tabela ZC4"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("ZC4",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetZC3
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetZC3()
 Local cTitulo      := "Tabela ZC3"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("ZC3",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSC2
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSC2()
 Local cTitulo      := "Tabela SC2"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SC2",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSC5
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSC5()
 Local cTitulo      := "Tabela SC5"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SC5",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSC6
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSC6()
 Local cTitulo      := "Tabela SC6"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SC6",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSC7
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSC7()
 Local cTitulo      := "Tabela SC7"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SC7",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSC9
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSC9()
 Local cTitulo      := "Tabela SC9"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SC9",cTitulo,cDel,cOk)
Return  
/*/{Protheus.doc} SetSD4
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSD4()
 Local cTitulo      := "Tabela SD4"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SD4",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSE4
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSE4()
 Local cTitulo      := "Tabela SE4"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SE4",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSED
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSED()
 Local cTitulo      := "Tabela SED"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SED",cTitulo,cDel,cOk)
Return 
/*/{Protheus.doc} SetSF4
  (long_description)
  @type  Static Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
  /*/
Static Function SetSF4()
 Local cTitulo      := "Tabela SF4"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SF4",cTitulo,cDel,cOk)
Return 

Static Function SetZ59()
 Local cTitulo      := "Tabela Z59"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("Z59",cTitulo,cDel,cOk)
Return 

Static Function SetSA3()
 Local cTitulo      := "Tabela SA3"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SA3",cTitulo,cDel,cOk)
Return 

Static Function SetSA2()
 Local cTitulo      := "Tabela SA2"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SA2",cTitulo,cDel,cOk)
Return 

Static Function SetSA4()
 Local cTitulo      := "Tabela SA4"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("SA4",cTitulo,cDel,cOk)
Return 

Static Function SetDA3()
 Local cTitulo      := "Tabela DA3"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("DA3",cTitulo,cDel,cOk)
Return 

Static Function SetZAI()
 Local cTitulo      := "Tabela ZAI"
 Local cDel         := ".T."
 Local cOk          := ".T."
 AxCadastro("ZAI",cTitulo,cDel,cOk)
Return 