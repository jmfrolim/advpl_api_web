#Include 'Totvs.ch'
#Include 'TbiConn.ch'
#Include 'TopConn.ch'

/*/{Protheus.doc} ZTUF0200
  (long_description)
  @type  Function
  @author user
  @since date
  @version version
  @param param, param_type, param_descr
  @return return, return_type, return_description
  @example
  (examples)
  @see (links_or_references)
/*/
User Function ZTUF0200()
 Local oProduto := Nil
 Local cTipo    := 'SV'
 Local nOperacao:= 0
 Local oObject  := Nil  


 PREPARE ENVIRONMENT EMPRESA '99' FILIAL '01' MODULO 'COM'
    oProduto:= Product():New()
    oObject := Product():New()
    ConOut("")
    ConOut("-----------------------------------------------------------------------------------------------------------")
    ConOut("")
    ConOut("---Iniciando Advpl---")
    Alert("---Iniciando Advpl---")
    ConOut("")
    ConOut("")

    nOperacao:=1

    If  nOperacao = 1
        oProduto:GetCod(cFilAnt,cTipo)
        Alert('Preenchedo Dados do Objeto Produto')
        oProduto:Filial        :=cFilAnt
        oProduto:Codigo        
        oProduto:Descricao     := 'Pintura de Casa'
        oProduto:Tipo          := cTipo
        oProduto:Ncm           := '99999999'
        oProduto:Unidade       := 'UN'
        oProduto:Armazem       := '0001'
        oProduto:PercICM       := 0
        oProduto:PercIPI       := 0
        oProduto:CodBar        := ''
        oProduto:PrecoVenda    := 0
        oProduto:MRP           := 'N'
        oProduto:ContParceria  := 'N'
        oProduto:ContSegSocia  := 'N'
        oProduto:TIPOCQ        := 'M'
        oProduto:IRRF          := 'N'
        oProduto:CtoEndereco   := 'N'
        oProduto:ContaCon      := '31107001002' 
        oProduto:Grupo         := '0006'
        oProduto:Origem        := '0'
        oProduto:Garant        := '2'
        oProduto:Naturez       :='002'
        oProduto:Agricul       :='*'
        
        ConOut("")

        If  oProduto:Insert()
            ConOut("Produto Salvo")
            Alert("Produto Salvo")
        Else
            ConOut("Erro ao Salvar Produto")
            Alert("Erro ao Salvar Produto")
        EndIf

    Else
        oProduto:IsExist(cFilAnt,"SV0003         ",cTipo)
       If  oProduto:Delete(cFilAnt,"SV0003")
           ConOut("---------")         
           ConOut("----ExecAuto com Sucesso----")
           Alert("----ExecAuto com Sucesso----")
           ConOut("---------")   
       Else
           ConOut("----------")
           ConOut("----Produto nao Existe----")
           Alert("----Produto nao Existe----")
           ConOut("----------")
       EndIf

    EndIf
 
    ConOut("-----------------------------------------------------------------------------------------------------------")
    ConOut("")
    ConOut("---Finalizando---")
    Alert("---Finalizando---")
    ConOut("")

 RESET ENVIRONMENT
Return 