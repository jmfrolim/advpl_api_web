#Include "Protheus.ch"
#Include "Apwebsrv.ch"

WsStruct Produto
  
  WsData Codigo
  WsData Filial
  WsData Descricao
  WsData Tipo
  WsData UN
  WsData Armazem

EndWsStruct

WsService Produtos Description "WEB SERVICES para manutencao de Produtos"
  WsData cCodigo
  WsData cTipo
  WsData cUnidade
  WsData ListaProduto As Array Of Produto

  
EndWsService