#include 'Protheus.ch'
#include 'TbiConn.ch'
#include 'Topconn.ch'

/*/{Protheus.doc} COMNPED0
(long_description)
@type function
@author joao.rolim
@since 25/05/2016
@version 1.0
@param cTipo, character, (Descri��o do par�metro)
@return cNumPro, Numero do Produto
@example  U_codProA(M->B1_TIPO) ANTIGA U_COMNPED0(M->B1_TIPO)
(examples)
@see ao ser implementado o modo de edicao do campo tipo deve ficar dessa forma iif(inclui,.T.,.F.)          
/*/
User Function COMNPED0(cTipo)
		 U_ZUF02112(cTipo)
Return

User Function ZUF02112(cTipo)
	Local aArea := GetArea()
	Local cNumRecn
	Local cNumproF

	If cTipo == 'MP'
		cNumRecn:= SFUNCOMP()
		cNumproF:= cNumRecn
	Else
		cNumRecn:= SFUNRECNO(cTipo)
		cNumproF:= cTipo + cNumRecn
	Endif

	RestArea(aArea)
Return cNumproF
//-----------------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SFUNRECNO
(long_description)
@type function
@author joao.rolim
@since 27/05/2016
@version 1.0
@param ctipo, character, (Descri��o do par�metro)
@return cNumpro
@example
(examples)
@see (links_or_references)
/*/
//-----------------------------------------------------------------------------------------------------------------------------------
Static Function SFUNRECNO(ctipo)
	Local aArea  := GetArea()
	Local cNumRecn
	Local cNumpro
	Local cQuery:= ""
	Local cWhere:= ctipo

	cQuery:= " 	SELECT MAX(R_E_C_N_O_) AS COD FROM " +  RetSqlName('SB1') + " SB1 "
	cQuery+=" WHERE SB1.B1_TIPO = '" + cWhere + "' AND SB1.D_E_L_E_T_ = ''  "

	If Select("cAlias1") <> 0
		dbSelectArea("cAlias1")
		cAlias1->(dbcloseArea())
	EndIf

	TcQuery cQuery New Alias "cAlias1"
	dbSelectArea("cAlias1")

	cNumRecn:= AllTrim(Str(cAlias1->COD))
	ValType(cNumRecn)

	cNumpro:= SFUNCODP(cNumRecn)

	cAlias1->(dbCloseArea())
	RestArea(aArea)
Return cNumpro
//
//-----------------------------------------------------------------------------------------------------------------------------------
/*/{Protheus.doc} SFUNCODP
(long_description)
@type function
@author joao.rolim
@since 27/05/2016
@version 1.0
@param cRecno, character, (numero do R_E_C_N_O_ da Tabela )
@return cNumpro, Caracter com �ltimo n�mero do produto por Tipo
@example MC1032 anterior o proximo sera MC1033
@see (links_or_references)
/*/
//-----------------------------------------------------------------------------------------------------------------------------------
Static Function SFUNCODP(cRecno)
	Local aArea1 := GetArea()
	Local cNumpro
	Local nNumqry
	Local cQuery				 := ""
	Local cWhere			 := cRecno     
	Local cNumMin		 := '000001'

	cQuery:= " 	SELECT B1_COD AS COD FROM " +  RetSqlName('SB1') + " SB1 "
	cQuery+= " WHERE SB1.R_E_C_N_O_ = '" + cWhere + "' AND SB1.D_E_L_E_T_ = ''  "

	If Select("cAlias2") <> 0
		dbSelectArea("cAlias2")
		cAlias2->(dbcloseArea())
	EndIf

	TcQuery cQuery New Alias "cAlias2"
	dbSelectArea("cAlias2")
		If  	cAlias2->(Eof()) .Or. Empty(Alltrim(cAlias2->COD))
					cNumpro := SubStr(cNumMin,3,6)
		Else	
					nNumqry:= SubStr(cAlias2->COD,3,6)
					nNumqry:= strZero(val(nNumqry)+ 1,4 )
					cNumpro:= nNumqry
		EndIf
					cAlias2->(dbCloseArea())
	RestArea(aArea1)
Return cNumpro

/*/{Protheus.doc} SFUNCOMP
(long_description)
@type function
@author joao.rolim
@since 31/05/2016
@version 1.0
@return cText
@example
(examples)
@see (links_or_references)
/*/
Static Function SFUNCOMP()
	Local oGet1
	Local cGet1 := Space(10)
	Local cMsgVa:= "Esse campo nao pode ser vazio"
	Local oSay1
	Local oSButton1
	Local oSButtonCan
	Local cText
	Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Codigo MP" FROM 000, 000  TO 130, 180 COLORS 0, 16777215 PIXEL
		@ 022, 013 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
		@ 008, 017 SAY oSay1 PROMPT "Codigo Materia Prima" SIZE 062, 007 OF oDlg COLORS 0, 16777215 PIXEL
		DEFINE SBUTTON oSButton1   FROM 042, 011 TYPE 01 OF oDlg ENABLE ACTION Iif(Empty(cGet1),MsgStop(cMsgVa,"Codigo MP"),oDlg: End())
		DEFINE SBUTTON oSButtonCan FROM 042, 044 TYPE 02 OF oDlg ENABLE ACTION  oDlg: End()
	ACTIVATE MSDIALOG oDlg CENTERED
	cText:= cGet1
Return cText