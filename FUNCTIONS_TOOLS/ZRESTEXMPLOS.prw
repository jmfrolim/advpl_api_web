


WSMETHOD GET WSSERVICE CLIENTES
 Local aArea := GetArea()
 Local cNextAlias := GetNextAlias()
 Local oCliente := CLIENTES():New() // �> Objeto da classe cliente
 Local oResponse := FULL_CLIENTES():New() // �> Objeto que ser? serializado
 Local cJSON := ""
 Local lRet := .T. 

  BeginSQL Alias cNextAlias
  SELECT A1_COD, A1_LOJA, A1_NOME, A1_END, A1_CGC, A1_CEP, A1_DDD, A1_TEL
  FROM %table:SA1% SA1
  WHERE SA1.%notdel%
  EndSQL

  (cNextAlias)->( DbGoTop() )

  If (cNextAlias)->( !Eof() )    
      While (cNextAlias)->( !Eof() )
            oCliente:SetCodigo( AllTrim((cNextAlias)->A1_COD ))
            oCliente:SetLoja( AllTrim((cNextAlias)->A1_LOJA))
            oCliente:SetNome( AllTrim((cNextAlias)->A1_NOME))
            oCliente:SetCGC( AllTrim((cNextAlias)->A1_CGC ))
            oCliente:SetCEP( AllTrim((cNextAlias)->A1_CEP ))
            oCliente:SetEnd( AllTrim((cNextAlias)->A1_END ))
            oCliente:SetDDD( AllTrim((cNextAlias)->A1_DDD ))
            oCliente:SetTel( AllTrim((cNextAlias)->A1_TEL ))
            oResponse:Add(oCliente)
            (cNextAlias)->( DbSkip() )
      EndDo
    
      cJSON := FWJsonSerialize(oResponse, .T., .T.,,.F.)
      ::SetResponse(cJSON)
  Else
      SetRestFault(400, "SA1 Empty")
      lRet := .F.

  EndIf
  RestArea(aArea)

Return(lRet)

WSMETHOD POST WSRECEIVE RECEIVE WSSERVICE CLIENTES
 Local cJSON := Self:GetContent() // Pega a string do JSON
 Local oParseJSON := Nil
 Local aDadosCli := {} //�> Array para ExecAuto do MATA030
 Local cFileLog := ""
 Local cJsonRet := ""
 Local cArqLog := ""
 Local cErro := ""
 Local cCodSA1 := ""
 Local lRet := .T.
 Private lMsErroAuto := .F.
 Private lMsHelpAuto := .F.

  // �> Cria o diret?rio para salvar os arquivos de log

  If !ExistDir("\log_cli")
      MakeDir("\log_cli")
  EndIf

  // �> Deserializa a string JSON

  FWJsonDeserialize(cJson, @oParseJSON)
  SA1->(DbSetOrder(3))
  If !SA1->(DbSeek(xFilial("SA1") + oParseJSON:CLIENTE:CGC ))

      cCodSA1 := GetNewCod()
      Aadd(aDadosCli, {"A1_FILIAL"    , xFilial("SA1")                                  , Nil} )
      Aadd(aDadosCli, {"A1_COD"       , cCodSA1                                         , Nil} )
      Aadd(aDadosCli, {"A1_LOJA"      , "01"                                            , Nil} )
      Aadd(aDadosCli, {"A1_CGC"       , oParseJSON:CLIENTE:CGC                          , Nil} )
      Aadd(aDadosCli, {"A1_NOME"      , oParseJSON:CLIENTE:NOME                         , Nil} )
      Aadd(aDadosCli, {"A1_NREDUZ"    , oParseJSON:CLIENTE:NOME                         , Nil} )
      Aadd(aDadosCli, {"A1_END"       , oParseJSON:CLIENTE:ENDERECO                     , Nil}  )
      Aadd(aDadosCli, {"A1_PESSOA"    , Iif(Len(oParseJSON:CLIENTE:CGC)== 11, "F", "J") , Nil} )
      Aadd(aDadosCli, {"A1_CEP"       , oParseJSON:CLIENTE:CEP                          , Nil} )
      Aadd(aDadosCli, {"A1_TIPO"      , "F"                                             , Nil} )
      Aadd(aDadosCli, {"A1_EST"       , oParseJSON:CLIENTE:ESTADO                       , Nil} )
      Aadd(aDadosCli, {"A1_MUN"       , oParseJSON:CLIENTE:MUNICIPIO                    , Nil} )
      Aadd(aDadosCli, {"A1_TEL"       , oParseJSON:CLIENTE:TELEFONE                     , Nil} )

      MsExecAuto({|x,y| MATA030(x,y)}, (aDadosCli, 3))

      If (lMsErroAuto)

          cArqLog := oParseJSON:CLIENTE:CGC + " � " +SubStr(Time(),1,5 ) + ".log"
          RollBackSX8()
          cErro := MostraErro("\log_cli", cArqLog)
          cErro := TrataErro(cErro) // �> Trata o erro para devolver para o client.
          SetRestFault(400, cErro)
          lRet := .F.

      Else

          ConfirmSX8()
          cJSONRet := '{"cod_cli":"'+SA1->A1_COD+'"'+',"loja":"'+SA1->A1_LOJA+'"'+',"msg":"'+"Sucesso"+'"'+'}'
          ::SetResponse( cJSONRet )

      EndIf
  Else

      SetRestFault(400, "Cliente j? cadastrado: " + SA1->A1_COD + " � " + SA1->A1_LOJA)
      lRet := .F.
  EndIf

Return(lRet)

WSMETHOD PUT WSRECEIVE RECEIVE WSSERVICE CLIENTES

 Local cJSON := Self:GetContent() // �> Pega a string do JSON
 Local cCGC := Self:CGC // �> Pega o par�metro recebido pela UR?
 Local lRet := .T.
 Local oParseJSON := Nil
 Local aDadosCli := {} //�> Array para ExecAuto do MATA030
 Local cJsonRet := ""
 Local cArqLog := ""
 Local cErro := ""
 Private lMsErroAuto := .F.

  If  !ExistDir("\log_cli")
      MakeDir("\log_cli")
  EndIf

  ::SetContentType("application/json")

  // �> Deserializa a string JSON

  FWJsonDeserialize(cJson, @oParseJSON)
  SA1->(DbSetOrder(3))
  If (SA1->( DbSeek( xFilial("SA1") + cCGC ) ))

      Aadd( aDadosCli, {"A1_NOME", oParseJSON:CLIENTE:NOME , Nil } )
      Aadd( aDadosCli, {"A1_END" , oParseJSON:CLIENTE:ENDERECO , Nil } )
      
      MsExecAuto({|x,y| MATA030(x,y)}, aDadosCli, 4)

      If lMsErroAuto
        cArqLog := oParseJSON:CLIENTE:CGC + " � " + SubStr( Time(),1,5 ) + ".log"
        cErro := MostraErro("\log_cli", cArqLog)
        cErro := TrataErro(cErro) // �> Trata o erro para devolver para o client.
        SetRestFault(400, cErro)
        lRet := .F.
      Else
          cJSONRet := '{"cod_cli":"' + SA1->A1_COD + '"';
                        + ',"loja":"' + SA1->A1_LOJA + '"';
                        + ',"msg":"' + "Alterado" + '"'+;
                      '}'
          ::SetResponse( cJSONRet )
      EndIf
  Else
      SetRestFault(400, "Cliente n?o encontrado.")
      lRet := .F.
  EndIf

Return(lRet)

#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
 
WSRESTFUL sample DESCRIPTION "Exemplo de servi�o REST"
  
  WSDATA count      AS INTEGER
  WSDATA startIndex AS INTEGER
    
  WSMETHOD GET DESCRIPTION "Exemplo de retorno de entidade(s)" WSSYNTAX "/sample || /sample/{id}"
  WSMETHOD POST DESCRIPTION "Exemplo de inclusao de entidade" WSSYNTAX "/sample/{id}"
  WSMETHOD PUT DESCRIPTION "Exemplo de altera�?o de entidade" WSSYNTAX "/sample/{id}"
  WSMETHOD DELETE DESCRIPTION "Exemplo de exclus?o de entidade" WSSYNTAX "/sample/{id}"
  
END WSRESTFUL
 
// O metodo GET nao precisa necessariamente receber parametros de querystring, por exemplo:
// WSMETHOD GET WSSERVICE sample 
WSMETHOD GET WSRECEIVE startIndex, count WSSERVICE sample
Local i
  
// define o tipo de retorno do m�todo
::SetContentType("application/json")
  
// verifica se recebeu parametro pela URL
// exemplo: http://localhost:8080/sample/1
If Len(::aURLParms) > 0
    
   // insira aqui o c?digo para pesquisa do parametro recebido
  
   // exemplo de retorno de um objeto JSON
   ::SetResponse('{"id":' + ::aURLParms[1] + ', "name":"sample"}')
  
Else
   // as propriedades da classe receber?o os valores enviados por querystring
   // exemplo: http://localhost:8080/sample?startIndex=1&count=10
   DEFAULT ::startIndex := 1, ::count := 5
    
   // exemplo de retorno de uma lista de objetos JSON
   ::SetResponse('[')
   For i := ::startIndex To ::count + 1
      If i > ::startIndex
         ::SetResponse(',')
      EndIf
      ::SetResponse('{"id":' + Str(i) + ', "name":"sample"}')
   Next
   ::SetResponse(']')
EndIf
Return .T.
 
// O metodo POST pode receber parametros por querystring, por exemplo:
// WSMETHOD POST WSRECEIVE startIndex, count WSSERVICE sample
WSMETHOD POST WSSERVICE sample
Local lPost := .T.
Local cBody
// Exemplo de retorno de erro
If Len(::aURLParms) == 0
 SetRestFault(400, "id parameter is mandatory")
 lPost := .F.
Else
 // recupera o body da requisi�?o
 cBody := ::GetContent()
 // insira aqui o c?digo para opera�?o inser�?o
 // exemplo de retorno de um objeto JSON
 ::SetResponse('{"id":' + ::aURLParms[1] + ', "name":"sample"}')
EndIf
Return lPost
  
// O metodo PUT pode receber parametros por querystring, por exemplo:
// WSMETHOD PUT WSRECEIVE startIndex, count WSSERVICE sample
WSMETHOD PUT WSSERVICE sample
Local lPut := .T.
  
// Exemplo de retorno de erro
If Len(::aURLParms) == 0
   SetRestFault(400, "id parameter is mandatory")
   lPut := .F.
Else
   // recupera o body da requisi�?o
   cBody := ::GetContent()
   // insira aqui o c?digo para opera�?o de atualiza�?o
   // exemplo de retorno de um objeto JSON
   ::SetResponse('{"id":' + ::aURLParms[1] + ', "name":"sample"}')
EndIf
Return lPut
  
// O metodo DELETE pode receber parametros por querystring, por exemplo:
// WSMETHOD DELETE WSRECEIVE startIndex, count WSSERVICE sample
WSMETHOD DELETE WSSERVICE sample
Local lDelete := .T.
  
// Exemplo de retorno de erro
If Len(::aURLParms) == 0
   SetRestFault(400, "id parameter is mandatory")
   lDelete := .F.
  
Else
   // insira aqui o c?digo para opera�?o exclus?o
   // exemplo de retorno de um objeto JSON
   :SetResponse('{"id":' + ::aURLParms[1] + ', "name":"sample"}')
EndIf
Return lDelete