#Include 'Totvs.ch'
#Include 'TopConn.ch'
#Include 'TbiConn.ch'

User Function ZUF05116()
 Local aArea      := GetArea()
 Local cSQL       :=  ""
 Local cCodigo

 cSQL+= " SELECT MAX(DA4_COD) AS CODIGO  FROM  " + RetSqlName('DA4') + "  "    
 MemoWrite("c:\temp\ZUF05116"+Time()+".txt",cSQL) 
 
 If Select("cAlias") <> 0
    DbSelectArea("cAlias")
    cAlias->(DbCloseArea())
 EndIf
 
 TcQuery cSQL New Alias "cAlias"
 
 DbSelectArea("cAlias")
 cCodigo:= StrZero(Val(cAlias->CODIGO)+1,6)
 cAlias->(DbCloseArea())

 RestArea(aArea)

Return cCodigo