#include "PROTHEUS.ch"
#include "RESTFUL.ch"

#xtranslate @{Header <(cName)>} => ::GetHeader( <(cName)> )
#xtranslate @{Param <n>} => ::aURLParms\[ <n> \]
#xtranslate @{EndRoute} => EndCase
#xtranslate @{Route} => Do Case
#xtranslate @{When <path>} => Case NGIsRoute( ::aURLParms, <path> )
#xtranslate @{Default} => Otherwise

Static aUsers := {{'1','Vitor','27'},{'2','Ana','22'},{'3','Joao','44'}}

WsRestful tstwsrest Description "WebService REST para testes"

	WsMethod GET Description "Sincronizaç?o de dados via GET"    WsSyntax "/GET/{method}"
	WsMethod POST Description "Sincronizaç?o de dados via POST"    WsSyntax "/POST/{method}"
	WSMETHOD DELETE DESCRIPTION "Exemplo de exclus?o de entidade" WSSYNTAX "/users/{id}"

End WsRestful

WsMethod GET WsService tstwsrest
	Local nUser
	Local cToken :=  Self:GetHeader( 'token' )
	Local cUser := ''

	::SetContentType( 'application/json' )

	@{Route}
		@{When '/users'}
		cUser := '['
		For nUser := 1 to Len(aUsers)
			cUser += '{ "id": "'+ aUsers[nUser][1] + '","name": "'+aUsers[nUser][2] + '", "age":' + aUsers[nUser][3] + '}'
			cUSer += if(nUser <Len(aUsers),',','')
		Next nUser
		cUser += ']'
		::SetResponse(cUser)

		@{When '/user/{id}'}
			nUser := aScan(aUsers,{|x| x[1] ==  @{Param 2}})
			If nUser > 0
				cUser += '{ "id": "'+ aUsers[nUser][1] + '","name": "'+aUsers[nUser][2] + '", "age":' + aUsers[nUser][3] + '}'
			EndIf
			::SetResponse(cUser)
		@{Default}
			SetRestFault(400, "Ops")
			Return .F.
	@{EndRoute}

Return .T.

WsMethod POST WsService tstwsrest
	Local cToken  := Self:GetHeader( 'token' )
	Local cJson   := Self:GetContent()
	Local oParser
	Local cUser
	::SetContentType( 'application/json' )

	@{Route}
		@{When '/user'}
		If FWJsonDeserialize(cJson,@oParser)
			aAdd(aUsers,{oParser:id,oParser:name,oParser:age})
			cUser := '{ "id": "'+ aTail(aUsers)[1] + '","name": "'+aTail(aUsers)[2] + '", "age":' + aTail(aUsers)[3] + '}'
			::SetResponse(cUser)
		EndIf
		@{Default}
			SetRestFault(400, "Ops")
			Return .F.
	@{EndRoute}


Return .T.

WsMethod DELETE WsService tstwsrest
	Local cUser := ''
	Local nUser

	::SetContentType( 'application/json' )

	@{Route}
		
		@{When '/user/{id}'}
			nUser := aScan(aUsers,{|x| x[1] ==  @{Param 2}})
			If nUser > 0
				aDel( aUsers, nUser )
				aSize( aUsers, Len( aUsers ) - 1 )
				::SetResponse('OK')
			Else
				SetRestFault(400, "Not found")
				Return .F.
			EndIf
		@{Default}
			SetRestFault(400, "Ops")
			Return .F.
	@{EndRoute}

Return .T.