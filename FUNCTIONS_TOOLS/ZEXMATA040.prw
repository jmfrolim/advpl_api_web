#Include 'Protheus.ch'
#Include 'TbiConn.ch'

//MATA040 - Rotina autom?tica de Vendedores ( [ ] )

User Function MyMata040()
 Local aDados           := {}
 Private lMsErroAuto    := .F.

 aAdd(aDados, {"A3_FILIAL", xFilial("SA3")		, nil})
 aAdd(aDados, {"A3_COD"   , "VEN003"	            , nil})
 aAdd(aDados, {"A3_NOME"  , "VENDEDOR TESTE 002"	, nil})
 
 MsExecAuto({|x,y|mata040(x,y)},aDados,3) 	
 
 If  lMsErroAuto
     MsgStop("Erro na grava�?o do vendedor")
     MostraErro()
 Else	
     MsgAlert('Vendedor incluido com sucesso.')
 EndIf
 
Return 	
