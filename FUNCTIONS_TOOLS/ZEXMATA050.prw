#Include "Protheus.ch"
#Include "TbiConn.ch"

//MATA050 - Rotina autom?tica de Transportadora ( [ ] )

User Function MyMata050()
 Local aDados := {}
 Private lMsErroAuto
 
 aAdd(aDados, {"A4_FILIAL", xFilial("SA4")		, nil})
 aAdd(aDados, {"A4_COD"   , "TRA002"	            , nil})
 aAdd(aDados, {"A4_NOME"  , "TRANSPORTADORA 002"	, nil})
 
 MsExecAuto({|x,y|mata050(x,y)},aDados,3) 	
 If  lMsErroAuto		
     MsgStop("Erro na grava�?o da transportadora")
     MostraErro() 
 Else
  	 MsgAlert('Transportadora incluida com sucesso.')
 EndIf

Return 	
