
// A fun�?o MATA040 permite efetuar a inclus?o de vendedores.
// MATA040 - Rotina autom?tica de Vendedores ( [ ] ) 

User Function MyMata040()
  Local   aDados := {}
  Private lMsErroAuto

  aAdd(aDados, {"A3_FILIAL", xFilial("SA3")		    ,Nil})
  aAdd(aDados, {"A3_COD"   , "VEN003"	            ,Nil})
  aAdd(aDados, {"A3_NOME"  , "VENDEDOR TESTE 002"	,Nil})

  MsExecAuto({|x,y|Mata040(x,y)},aDados,3) 

  If  lMsErroAuto		  
      MsgStop("Erro na grava�?o do vendedor")	
      MostraErro()
  Else
      MsgAlert('Vendedor incluido com sucesso.')	
  EndIf

Return 	

