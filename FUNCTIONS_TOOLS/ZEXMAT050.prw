

User Function MyMata050()
  Local   aDados := {}
  Private lMsErroAuto
  
  aAdd(aDados, {"A4_FILIAL", xFilial("SA4")		     ,Nil})
  aAdd(aDados, {"A4_COD"   , "TRA002"	             ,Nil})
  aAdd(aDados, {"A4_NOME"  , "TRANSPORTADORA 002"	 ,Nil})  
  
  MsExecAuto({|x,y|Mata050(x,y)},aDados,3) 	
  
  If  lMsErroAuto		
      MsgStop("Erro na grava�?o da transportadora")
      MostraErro()
  Else
  	  MsgAlert('Transportadora incluida com sucesso.')	
  EndIf

Return 	

