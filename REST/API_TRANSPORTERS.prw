#Include 'Totvs.ch'
#Include 'RestFul.ch'
#Include 'parmtype.ch'

WsRestFul APITRANSPORTES Description "Manutencao de Transportadora"

 WsData Filial as String
 WsData Codigo as String
 WsMethod POST Description "Inseri transportadora" WSSYNTAX "transporter"
 WsMethod GET  Description "Busca as Transportadora do Banco" WSSYNTAX "/transporters/{Codigo}"

End WsRestFul

WsMethod GET WsReceive Codigo WsService APITRANSPORTES
 Local lReturn  := .F.
 Local oTrans   := Transporters():New()
 Local oCollec  := Collect():New()
 Local oObj     := Transporters():New()      
 Local cAlias   := ""
 Local cJson    := ""

 ::SetContentType('application/json')

  If  Len(::aUrlParms) > 0
      Do Case
         Case ::Codigo == 'all' 
          cAlias:= oTrans:GetAll()
          If  Empty('cAlias')
              SetRestFault(400,'Retorno vazio nao existem produtos cadastrados')
          Else
              While (cAlias)->(!Eof())
                     oObj            := Transporters():New()         
                     oObj:Filial     := cAlias->FILIAL
                     oObj:Codigo     := cAlias->CODIGO
                     oObj:CGC        := cAlias->CPF_CNPJ
                     oObj:Nome       := cAlias->NOME
                     oObj:InsEstadual:= cAlias->INSEST    
                     oObj:UF         := cAlias->UF                     
                     oCollec:SetAdd(oObj)
                     cAlias->(DbSkip())           
              EndDo
              cJson:= FwJsonSerialize(oCollec:aObject)
              ::SetResponse(cJson)     
              lReturn := .T.
          EndIf
         Case ::Codigo != 'all'
            cJson   := '{"Teste com Sucesso!"}'
            ::SetResponse(cJson)
            lReturn := .T.
      EndCase
      
  Else
      lReturn := .F.
      SetRestFault(400,'URL  Vazio')       
  EndIf
Return lReturn

WsMethod POST WsReceive transporter WsService APITRANSPORTES
  Local lReturn  := .T.
  Local cJsonRet := ""
  ::SetContentType('application/json')
  cJsonRet  := '{"POST com Sucesso!"}'
  
Return lReturn
