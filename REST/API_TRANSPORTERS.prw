#Include 'Totvs.ch'
#Include 'RestFul.ch'
#Include 'parmtype.ch'

WsRestFul APITRANSPORTES Description "Manutencao de Transportadora"

 WsData Filial      As String
 WsData Codigo      As String
 WsData CPF_CNPJ    As String
 WsMethod POST Description "Inserir transportadora" WSSYNTAX "transporter"
 WsMethod GET  Description "Buscar as Transportadora do Banco" WSSYNTAX "/Transporter/{Codigo}/{CPF_CNPJ}"

End WsRestFul

WsMethod GET WsReceive Codigo WsService APITRANSPORTES
 Local lReturn  := .F.
 Local oTrans   := Transporter():New()
 Local oCollec  := Collect():New()
 Local oObj     := Transporter():New()      
 Local cAlias   := ""
 Local cJson    := ""

 ::SetContentType('application/json')

  If  Len(::aUrlParms) > 0
      Do Case
         Case ::Codigo == 'all' 
          cAlias:= oTrans:FindAll()
          If  Empty('cAlias')
              SetRestFault(400,'Retorno vazio nao existem produtos cadastrados')
          Else
              While (cAlias)->(!Eof())
                     oObj            := Transporter():New()         
                     oObj:Filial     := cAlias->FILIAL
                     oObj:Codigo     := cAlias->CODIGO
                     oObj:CGC        := cAlias->CPF_CNPJ
                     oObj:Nome       := cAlias->NOME
                     oObj:InsEstadual:= cAlias->INSEST    
                     oObj:UF         := cAlias->UF                     
                     oCollec:SetAdd(oObj)
                     cAlias->(DbSkip())           
              EndDo
              cJson:= FwJsonSerialize(oCollec:Objects)
              ::SetResponse(cJson)     
              lReturn := .T.
          EndIf
         Case ::Codigo != 'all'
            cJson   := '{"Teste com Sucesso!"}'
            ::SetResponse(cJson)
            lReturn := .T.
      EndCase
      
  Else
      lReturn := .F.
      SetRestFault(400,'URL  Vazio')       
  EndIf
Return lReturn

WsMethod POST WsReceive transporter WsService APITRANSPORTES
  Local lReturn  := .T.
  Local cJsonRet := ""
  ::SetContentType('application/json')
  cJsonRet  := '{"POST com Sucesso!"}'
  
Return lReturn
