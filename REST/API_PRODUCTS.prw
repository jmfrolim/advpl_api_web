#Include 'Totvs.ch'
#Include 'RestFul.ch'
#Include 'parmtype.ch'

WsRestFul APIPRODUCTS Description "Servico para Manutencao de Produtos"

  WsData Filial as String
  WsData Codigo as String
  WsData Tipo   as String 
  WsData allpro as String
  
 
  WsMethod POST Description "Salva o Produto" WSSYNTAX "/produto"
  WsMethod GET  Description "Retorno o Produto" WSSYNTAX "/produto/{Codigo}"
  
 
  //WSMETHOD POST 	DESCRIPTION "Cadastra um novo cliente" 		WSSYNTAX ""
  //
  //WsMethod DELETE Description "" PATH "/produto/APIPRODUTO/{id}"
  //WsMethod PUT Description 
  
End WsRestFul

WsMethod GET  WSRECEIVE Codigo WsService APIPRODUCTS
 Local aArea    := GetArea()
 Local oProduct := Product():New()
 Local oObj     := Product():New()
 Local Collect  := Collect():New()
 Local lReturn  := .F.
 Local cTipo    := "SV"
 Local cJson    := ""
 Local cAlias   := ""
 
 ::SetContentType('application/json')  
 If  Len(::aUrlParms) > 0
     Do Case
     Case ::Codigo == 'all'
          cAlias:= oObj:FindAll()
          If  Empty(cAlias)
              SetRestFault(400,'Retorno vazio nao existem produtos cadastrados')
          Else
             While (cAlias)->(!Eof())
                   oProduct:= Product():New()
                   oProduct:Codigo     := cAlias->CODIGO
                   oProduct:Descricao  := cAlias->DESCRICAO
                   oProduct:Tipo       := cAlias->TIPO
                   oProduct:PrecoVenda := Iif(ValType(cAlias->PRECOVENDA) == 'C',CvalToChar(cAlias->PRECOVENDA),cAlias->PRECOVENDA)
                   oProduct:Armazem    := cAlias->ARMAZEM
                   Collect:SetAdd(oProduct)
                   cAlias->(DbSkip())
             EndDo       
             cJson:= FwJsonSerialize(Collect)
             ::SetResponse(cJson)     
             lReturn := .T.
          EndIf   
     Case ::Codigo != 'all'
          If  oProduct:Find(FwxFilial('SB1'),::Codigo,cTipo)    
              If  Len(::aUrlParms) > 0
                  oProduct:PrecoVenda := CvalToChar(oProduct:PrecoVenda)
                  oProduct:CusStand   := CvalToChar(oProduct:CusStand)
                  cJson := FwJsonSerialize(oProduct,.T.,.T.,,.F.)
                  ::SetResponse(cJson)
                  lReturn := .T.
              Else
                  SetRestFault(400, "E preciso colocar parametros na URL")
              EndIf
          Else
              SetRestFault(400,"Nao foi possivel encontrar o produto")    
          EndIf         
     EndCase
 EndIf 
  RestArea(aArea)
Return lReturn



WsMethod POST WSReceive produto WsService APIPRODUCTS
 Local lReturn  := .F.
 Local oProduct := Product():New()
 Local oObj     := Product():New()
 Local cJson    := ::GetContent()
 Local cJsonRet := ""
 Local cTipo    := "SV"
 Local aArea    := GetArea()
  
  If  Len(::aUrlParms) > 0
      ::SetContentType('application/json')      
      
      FwJsonDeserialize(cJson , @oProduct)
      oObj:GetClone(oProduct)
      oObj:GetCod(FwxFilial('SB1'),cTipo)
      If  oObj:Insert()
          lReturn   := .T.
          cJsonRet  := '{"Codigo":"'+oObj:Codigo+'","Tipo":"'+oObj:Tipo+'","msg":"Sucesso"}'
          ::SetResponse(cJsonRet)      
      Else
          SetRestFault(400,"Nao Foi possivel cadastrar o produto")
      EndIf
  Else
      SetRestFault(400,"Nao foi possivel executar POST o produto")
  EndIf
  FreeObj(oObj)
  RestArea(aArea)
Return lReturn