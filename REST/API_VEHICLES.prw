#Include  'totvs.ch'
#Include  'restful.ch'
#Include  'parmtype.ch'

WsRestFul APIVEHICLES Description "Manutencao de Veiculos"
  WsData Filial   As String
  WsData ID       As String
  WsData Codigo   As String
  WsData Status   As String
  WsData Modelo   As String
  WsData Limite   As String

  WsMethod POST Description "Inserir um Veiculo " WSSYNTAX "vehicle"
  WsMethod PUT Description  "Alterando um Veiculo " WSSYNTAX "vehicle"
  WsMethod GET  Description "Listar os Veiculos " WSSYNTAX "/vehicles/{Codigo}/{Modelo}/{Status},{Limite},{ID}"
End WsRestFul

WsMethod GET WsReceive Codigo,Modelo,Status,Limite,ID WsService APIVEHICLES
 Local lReturn    := .F.
 Local Object     := Vehicle():New()
 Local Vehicle    := Vehicle():New()
 Local Collect    := Collect():New()
 Local cJon       := ""
 Local cAlias     := ""
 Local cFilial    := FwxFilial('DA4')
 Local aPars      := {}
  
  ::SetContentType('application/json')
  If  Len(::aUrlParms) > 0
      aPars := {::Codigo,::Modelo,::Status,::Limite,::ID}
      Do Case
         Case ::Codigo='all'
           cAlias := Vehicle:FindAll(aPars)
           If  Empty('cAlias')
               SetRestFault(400,'Retorno vazio nao existem Veiculos cadastrados')  
           Else
               While (cAlias)->(!Eof())
                     Object              := Vehicle():New()
                     Object:Filial       := cAlias->FILIAL
                     Object:Id           := cAlias->ID
                     Object:Codigo       := cAlias->CODIGO
                     Object:Modelo       := cAlias->MODELO
                     Object:Placa        := cAlias->PLACA
                     Object:Placa2       := cAlias->PLACA2
                     Object:Placa3       := cAlias->PALCA3
                     Object:Cidade       := cAlias->CIDADE
                     Object:UF           := cAlias->UF
                     Object:RNTC         := cAlias->RNTC
                     Object:CAP_MINIMA   := cAlias->CAP_MINIMA
                     Object:CAP_MAXIMA   := cAlias->CAP_MAXIMA
                     Object:STATUS       := cAlias->STATUS
                     Object:VOL_MAXIMO   := cAlias->VOL_MAXIMO
                     Object:Motorista    := cAlias->MOTORISTA
                     Object:Tipo         := cAlias->TIPO
                     Collect:SetAdd(Object)
                     cAlias->(DbSkip())
               EndDo
               cJon := FwJsonSerialize(Collect)
               ::SetResponse(cJon)
               lReturn  := .T.                            
           EndIf
         Case !Empty(::Codigo) .And. (::Codigo!='all')
              If  Object:Find(cFilial,::Codigo)
                  cJson := FwJsonSerialize(Object)
                  ::SetResponse(cJson)
                  lReturn := .T.
              Else
                  SetRestFault(400,'Veiculo de Placa: '+::Codigo+' nao foi encontrado.')
              EndIf
      EndCase
    
  Else
      SetRestFault(400,"URL esta vazia")
      lReturn   := .F.    
  EndIf
Return lReturn

WsMethod POST WsReceive vehicle WsService APIVEHICLES
 Local lReturn      := .F.
 Local Vehicle      := Vehicle():New()
 Local Object       := Vehicle():New()
 Local cJson        := ::GetContent()
 Local cJsonRet     := ""
 Local aArea        := GetArea()

 ::SetContentType('application/json')
 If  Len(::aUrlParms)> 0
     FwJsonDeSerialize(cJson,@Object)
     Vehicle:Clone(Object)
     If  Vehicle:Insert()
         lReturn    := .T.
         cJsonRet   := '{"Msg":"Veiculo Incluido com sucesso","PLACA": "'+Vehicle:PLACA+'","UF": "'+Vehicle:UF+'","MODELO": "'+Vehicle:MODELO+'"}'
         ::SetResponse(cJsonRet)
     Else
         SetRestFault(400,'Nao foi possivel cadastrar o Veiculo!')         
     EndIf
 Else
     SetRestFault(400,'Parametro Vazio!')   
 EndIf
 FreeObj(Vehicle)
 RestArea(aArea)
Return lReturn