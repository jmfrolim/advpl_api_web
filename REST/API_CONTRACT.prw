#Include 'Totvs.ch'
#Include 'RestFul.ch'
#Include 'parmtype.ch'

WsRestFul APICONTRACT Description "Manutencao de Contratos"

 WsData Filtro as String
 WsMethod POST Description "Salva Contrato" WSSYNTAX "/contract"
 WsMethod GET  Description "Contulta todos os Contratos" WSSYNTAX "/contract/{Filtro}"

End WsRestFul

WsMethod GET WsReceive Filtro WsService APICONTRACT
 Local lReturn    := .F.
 Local oContract  := Contract():New()
 Local oObj       := Contract():New()
 Local oCollec    := Collect():New()
 Local cAlias     := ""
 Local nWhile     := 0
 
 ::SetContentType('application/json')
 
 If  Len(::aUrlParms) > 0
     Do Case
        Case ::Filtro == 'all'
             cAlias:= oContract:GetAll()
             If !Empty(cAlias)
                DbSelectArea('cAlias')
                While cAlias->(!Eof())
                      oObj       := Contract():New()
                      oObj:Filial        := cAlias->FILIAL
                      oObj:Pedido        := cAlias->PED
                      oObj:Status        := cAlias->STATUS
                      oObj:Contrato      := cAlias->CONTRATO
                      oObj:TipoCliente   := cAlias->TIPOCLI
                      oObj:oCliente      := AllTrim(cAlias->CLIENTE)
                      oObj:aVendedores   := cAlias->VENDEDOR
                      //oObj:TipoFrete     := cAlias->TIPOFRETE
                      oObj:Vencimento    := cAlias->VENCIMENTO
                      oObj:Moeda         := cAlias->MOEDA
                      oObj:PTAX          := cAlias->TAXA
                      oObj:ValUnit       := cAlias->PRCVEN
                      Do While (cAlias->CONTRATO == oObj:Contrato)
                         oObj:aItens        := oObj:SetItem(oObj,cAlias->ITEM,cAlias->CULTURA,cAlias->QUANT,cAlias->SALDO,cAlias->ENTREGUE)
                         cAlias->(DbSkip())               
                      EndDo           
                      oCollec:SetAdd(oObj)
                      oObj:=Nil
                      nWhile++
                      cAlias->(DbSkip())
                EndDo
                cJson:= FwJsonSerialize(oCollec)
                ::SetResponse(cJson)
                lReturn    := .T.
                MemoWrite('\temp\teste_contract.json',cJson)
                MemoWrite('\temp\numero_itens.txt',Str(nWhile))
             Else         
                  cJson:= "Erro na sele��o do Alias de Dados."
                  MemoWrite('c:\temp\ZTUF0208.txt',cJson)     
                  SetRestFault(400,'Inconsistencia no Alias de Dados') 
              EndIf
              SetRestFault(400,'Alias Vazio')            
              lReturn := .T.
        Case ::Filtro!='all'
             cJson   := '{"Teste com Sucesso!"}'
             ::SetResponse(cJson)
             lReturn := .T.
     EndCase     
    
 Else
      lReturn := .F.
      SetRestFault(400,'URL  Vazio')   
 EndIf
Return lReturn

WsMethod POST WsReceive contract WsService APICONTRACT
  Local lReturn  := .T.
  Local cJsonRet := ""
  ::SetContentType('application/json')
  cJsonRet  := '{"POST com Sucesso!"}'
  
Return lReturn
