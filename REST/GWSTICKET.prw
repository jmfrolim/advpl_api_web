#Include 'totvs.ch'
#Include 'restful.ch'
#Include 'topconn.ch'

WsRestful APIGWSTICKET Description "API de Manutencao de Ticket de Saida para automacao do Guardian"
    WsData placa        AS String
    WsData placa2       AS String
    WsData placa3       AS String
    WsData tag          AS String
    WsData periferico   AS String
    WsData peso         AS Float
    WsData produto      AS String
    WsData bruto        AS Float
    WsData tara         AS Float
    WsData liquido      AS Float
    WsData operacao     AS String
    WsData numero       AS String

    WsMethod POST Description "Inserir um Ticket " WSSYNTAX "ticket"
    WsMethod PUT  Description "Alterando um Ticket " WSSYNTAX "ticket"
    WsMethod GET  Description "Recuperar informacao do Ticket" WSSYNTAX "/ticket/{numero}/{placa}/{tag}/{periferico}/{peso}/{operacao}"
End WsRestful

WsMethod GET WsReceive numero,placa,tag,periferico,peso,operacao WsService APIGWSTICKET
    Local lReturn   := .F.
    Local cJson     := "" 
    Local aReturn   := {}

    ::SetContentType('application/json')
    If Len(::aUrlParms) > 0
        
    Else
        SetRestFault(400,"URL esta vazia")
        lReturn   := .F.
        
    EndIf
    
Return lReturn