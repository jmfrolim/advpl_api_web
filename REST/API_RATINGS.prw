#Include 'Totvs.ch'
#Include 'RestFul.ch'
#Include 'parmtype.ch'

WsRestful APIRATINGS Description "Manutencao de Cotacao de Frete"
 WsData Codigo As String
 WsData Inicio As Date
 WsData Fim    As Date
 WsData Status As String 
 WsData Transportadora As String
 WsData Limite As String

 WsMethod POST Description "Salva Contacao  " WSSYNTAX "/rating/"
 WsMethod GET  Description "Consulta Cotacao" WSSYNTAX "/rating/{Codigo}/{Inicio}/{Fim}/{Status}/{Transportadora}/{Limite}"
 WsMethod PUT  Description "Realiza uma alteracao na Cotacao" WSSYNTAX "/rating/{Codigo}/{Transportadora}"

End WsRestful

WsMethod GET WsReceive Codigo,Inicio,Fim,Status,Transportadora,Limite WsService APIRATINGS
 Local lReturn  := .F.
 Local oRating  := Rating():New()
 Local oObject  := Rating():New()
 Local oCollect := Collect():New()
 //Local oLog     := GeraLog():New()
 Local cLog     := ""
 Local nTipo    := 0
 Local nX       := 0
 Local aPars    := {::Codigo,::Inicio,::Fim,::Status,::Transportadora,::Limite}
 Local cAlias   := ""
 Local cJson    := ""

 ::SetContentType('application/json')
 If  Len(::aUrlParms) > 0
 		 Do	Case
 		 		Case !Empty(::aUrlParms[1])
             nTipo  := 1
             cAlias := oRating:FindAll(aPars,nTipo)
             DbSelectArea('cAlias')
             If  !Empty(cAlias)
                 While cAlias->(!Eof())
                       oObject:= Rating():New()
                       oObject:Filial    := cAlias->FILIAL
                       oObject:Contrato  := cAlias->CONTRATO
                       oObject:Pedido    := cAlias->PEDIDO
                       oObject:Status    := cAlias->STATUS
                       oObject:UserImp   := cAlias->USER_INCLUSAO
                       oObject:Codigo    := cAlias->CODIGO
                       oObject:Emissao   := cAlias->EMISSAO
                       oObject:Validade  := cAlias->VENCIMENTO
                       oObject:Volume    := CvalToChar(cAlias->VOLUME)
                       Do While (oObject:Contrato == cAlias->CONTRATO)
                          Aadd(oObject:aItens,{cAlias->FILIAL,cAlias->ITEM,cAlias->CODIGO,cAlias->CONTRATO,;
                                               cAlias->TRANSPORTADORA,,CvalToChar(cAlias->VALOR_FRETE),cAlias->STATUS_ITEM})
                          cAlias->(DbSkip())
                       EndDo
                       oCollect:SetAdd(oObject)
                       cAlias->(DbSkip())  
                 EndDo
                 cJson  := FwJsonSerialize(oCollect:Objects)
                 ::SetResponse(cJson)
                 lReturn:= .T.                 
             Else
                 cJson  := "Erro no Alias de Dados"
                 SetRestFault(400,cJson)
             EndIf
              SetRestFault(400,'Alias Vazio')            
              lReturn := .T.             
        
        Case !Empty(::aUrlParms[2]) .And. !Empty(::aUrlParms[3])
             nTipo  := 2
             cAlias := oRating:FindAll(aPars,nTipo)
             DbSelectArea('cAlias')
             If  !Empty(cAlias)
                 While cAlias->(!Eof())
                       oObject:= Rating():New()
                       oObject:Filial    := cAlias->FILIAL
                       oObject:Contrato  := cAlias->CONTRATO
                       oObject:Pedido    := cAlias->PEDIDO
                       oObject:Status    := cAlias->STATUS
                       oObject:UserImp   := cAlias->USER_INCLUSAO
                       oObject:Codigo    := cAlias->CODIGO
                       oObject:Emissao   := cAlias->EMISSAO
                       oObject:Validade  := cAlias->VENCIMENTO
                       oObject:Volume    := cAlias->VOLUME
                       Do While (oObject:Contrato == cAlias->CONTRATO)
                          Aadd(oObject:aItens,{cAlias->FILIAL,cAlias->ITEM,cAlias->CODIGO,cAlias->CONTRATO,;
                                               cAlias->TRANSPORTADORA,,cAlias->VALOR_FRETE,cAlias->STATUS_ITEM})
                          cAlias->(DbSkip())
                       EndDo
                       oCollect:SetAdd(oObject)
                       cAlias->(DbSkip())  
                 EndDo
                 cJson  := FwJsonSerialize(oCollect)
                 ::SetResponse(cJson)
                 lReturn:= .T.  
                              
             Else
                 cJson  := "Erro no Alias de Dados"
                 
                 SetRestFault(400,cJson)
             EndIf
              SetRestFault(400,'Alias Vazio')            
              lReturn := .T.        
 		 EndCase
     
 Else
     SetRestFault(400,'aUrlparms Vazio')  
 EndIf

Return lReturn

WsMethod POST WsReceive rating WsService APIRATINGS
 Local lReturn  := .F.
 Local cJsonRet := ""
  
  ::SetContentType('application/json')
  cJsonRet  :='{"Teste POST com Sucesso!"}'
  lReturn   := .T.
  ::SetResponse(cJsonRet)
  

Return lReturn


WsMethod PUT WsReceive Codigo,Transportadora WsService APIRATINGS
Return lReturn