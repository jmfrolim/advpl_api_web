#Include 'Totvs.ch'
#Include 'RestFul.ch'
#Include 'parmtype.ch'

WsRestful APIDRIVERS Description "Servico de Manutencao de Motoristas"
 WsData Filial    As String
 WsData Codigo    As String
 WsData CPF_CNPJ  As String
 WsData CNH       As String 
 WsData Status    As Boolean 
 
 WsMethod POST Description "Salva Motorista" WSSYNTAX "/motorista"
 WsMethod GET  Description "Retorno o Motoristas" WSSYNTAX "/motorista/{Codigo},{CPF_CNPJ},{CNH},{Status}"

EndWsRestFul

/*/{Protheus.doc} WsMethod GET
  (long_description)
  @type  WsMethod
  @author Joao Manoel Freitas Rolim
  @since 09/02/19
  @version version
  @param Codigo, String, Codigo do motorista
    CPF_CNPJ, String, CPF ou CNPJ do motorista
    CNH, String, habilitacao do motorista
    Status, Logico, Status do motorista
  @return lReturn, Logico, varial de retorno
  @example
/*/
WsMethod GET WsReceive Codigo,CPF_CNPJ,CNH,Status WsService APIDRIVERS
  Local lReturn     := .F.
  Local Object      := Driver():New()
  Local Driver      := Driver():New()
  Local Collect     := Collect():New()
  Local cJson       := ""
  Local cAlias      := ""
  Local aPars       := {}

  ::SetContentType('application/json')
  If  Len(::aUrlParms)> 0
      Do Case
         Case ::Codigo='all'
             cAlias:= Driver:FindAll(aPars)
             If  Empty('cAlias')
                 SetRestFault(400,'Retorno vazio n�o existem motoristas cadastrados!')
             Else
                 While (cAlias)->(!Eof())
                       Object         := Driver():New()
                       Object:Codigo  := cAlias->CODIGO
                       Object:CPF     := cAlias->CPF
                       Object:CNH     := cAlias->CNH
                       Object:Nome    := cAlias->NOME
                       Object:Status  := cAlias->STATUS
                       Object:Filial  := cAlias->FILIAL
                       Object:Tipo    := cAlias->TIPO
                       Object:UF      := cAlias->UF
                       Object:Fone    := cAlias->FONE
                       Collect:SetAdd(Object)
                       cAlias->(DbSkip())
                 EndDo                                
                 cJson  := FwJsonSerialize(Collect)
                 ::SetResponse(cJson)
                 lReturn:= .T.
             EndIf
             
      EndCase
  Else
      SetRestFault(400,'Url vazia e necessario informar os parametros!')
  EndIf  
Return lReturn
/*/{Protheus.doc} WsMethod POST
  (long_description)
  @type  WsMethod
  @author Joao Manoel Freitas Rolim
  @since 09/02/19
  @version version
  @param motorista, um objeto motorista com todos os dados
  @return lReturn, Logico, varial de retorno
  @example
/*/
WsMethod POST WsReceive motorista WsService APIDRIVERS
 Local lReturn    := .F.
 Local Object     := Driver():New()
 Local Driver     := Driver():New()
 Local cJson      := ::GetContent()
 Local cJsonRet   := ""
 Local aArea      := GetArea() 
  
  ::SetContentType('application/json')
  If  Len(::aUrlParms) > 0
      ::SetContentType('application/json')
      FwJsonDeSerialize(cJson,@Object)
      Driver:GetCod()
      Driver:Clone(Object)
      If  Driver:Insert()
          lReturn   := .T.
          cJsonRet  := '{"msg":"Motorista Incluido com sucesso","CNH": "'+Driver:CNH+'","CPF": "'+Driver:CPF+'","NOME": "'+Driver:Nome+'","UF": "'+Driver:UF+'"}'
          ::SetResponse(cJsonRet)
      Else
          SetRestFault(400,'Nao foi possivel cadastrar o motorista!')
      EndIf
  Else
      SetRestFault(400,'Nao foi possivel executar o POST do Motorista!')
  EndIf
  FreeObj(Driver)
  RestArea(aArea)
Return lReturn