#Include 'totvs.ch'
#Include 'restful.ch'
#Include 'parmtype.ch'

WsRestFul APIPREORDER Description "Manutencao de Pre Ordem de Carregamento"
  WsData ID       As String 
  WsData Filial   As String
  WsData Codigo   As String
  WsData Contrato As String 
  WsData Pedido   As String 
  WsData Emissao  As Date
  WsData Limite   As String
  
  WsMethod POST Description "Inserir uma PreOrdem " WSSYNTAX "preorder"
  WsMethod GET  Description "Listar PreOrdens     " WSSYNTAX "preorder/{ID}/{Codigo}/{Contrato}/{Filial}/{Pedido}/{Emissao}/{Limite}"
End WsRestFul